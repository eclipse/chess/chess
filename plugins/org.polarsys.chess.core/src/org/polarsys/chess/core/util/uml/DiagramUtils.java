package org.polarsys.chess.core.util.uml;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationModel;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.papyrus.infra.gmfdiag.css.notation.CSSDiagram;
import org.eclipse.papyrus.infra.gmfdiag.style.PapyrusDiagramStyle;
import org.eclipse.ui.handlers.HandlerUtil;

public class DiagramUtils {

	static public boolean isBlockDefinitionDiagram(Diagram diagram) {
		return isTypeDiagram(diagram, "PapyrusUMLClassDiagram", "org.eclipse.papyrus.sysml16.diagram.blockdefinition");
	}

	static public boolean isInternalBlockDiagram(Diagram diagram) {
		return isTypeDiagram(diagram, "CompositeStructure", "org.eclipse.papyrus.sysml16.diagram.internalblock");
	}

	static public boolean isStateMachineDiagram(Diagram diagram) {
		return isTypeDiagram(diagram, "PapyrusUMLStateMachineDiagram",
				"org.eclipse.papyrus.sysml16.diagram.stateMachine");
	}

	static private boolean isTypeDiagram(Diagram diagram, String diagramType, String diagramKind) {

		if (diagram.getType().equals(diagramType)) {
			for (Object style : diagram.getStyles()) {
				if (style instanceof PapyrusDiagramStyle) {
					return ((PapyrusDiagramStyle) style).getDiagramKindId().equals(diagramKind);
				}
			}
		}

		return false;
	}

	/**
	 * Returns the block definition diagram associated to the given element, if any.
	 * 
	 * @param diagrams the list of diagrams
	 * @param element  the owning element
	 * @return the diagram, if any
	 */
	static public Diagram getBDD(Collection<Diagram> diagrams, EObject element) {
		for (Diagram diagram : diagrams) {
			if (isBlockDefinitionDiagram(diagram) && diagram.getElement() == element) {
				return diagram;
			}
		}
		return null;
	}

	/**
	 * Returns the state machine diagram associated to the given element, if any.
	 * 
	 * @param diagrams the list of diagrams
	 * @param element  the owning element
	 * @return the diagram, if any
	 */
	static public Diagram getSMD(Collection<Diagram> diagrams, EObject element) {
		for (Diagram diagram : diagrams) {
			if (isStateMachineDiagram(diagram) && diagram.getElement() == element) {
				return diagram;
			}
		}
		return null;
	}

	static public Collection<Diagram> getDiagrams() {

		NotationModel notationModel = NotationUtils.getNotationModel();
		Set<Diagram> diagrams = getChessDiagrams(notationModel.getResources());
		return diagrams;

	}

	static public Set<Diagram> getDiagrams(Set<Resource> resources) {

		Set<Diagram> diagrams = new HashSet<Diagram>();

		for (Resource current : resources) {
			for (EObject element : current.getContents()) {
				if (element instanceof Diagram) {
					diagrams.add((Diagram) element);
				}
			}
		}

		return diagrams;
	}

	static public Set<Diagram> getChessDiagrams(Set<Resource> resources) {
		Set<Diagram> diagrams = new HashSet<Diagram>();

		for (Resource current : resources) {
			for (EObject element : current.getContents()) {
				if (element instanceof Diagram) {
					Diagram diagram = (Diagram) element;

					if (isBlockDefinitionDiagram(diagram) || isInternalBlockDiagram(diagram)
							|| isStateMachineDiagram(diagram)) {
						diagrams.add((Diagram) element);
					}
				}
			}
		}

		return diagrams;
	}

	static public boolean isBlockDefinitionDiagram(ExecutionEvent event) {

		final Diagram diagram = getDiagramFromSelection(event);
		return isBlockDefinitionDiagram(diagram);

	}

	/**
	 * Returns the selected diagram.
	 * 
	 * @param event
	 * @return
	 */
	static public Diagram getDiagramFromSelection(ExecutionEvent event) {
		final CSSDiagram selectedEditPart = getSelectedGraphicalObject(event);

		return selectedEditPart.getDiagram();
	}

	static public CSSDiagram getSelectedGraphicalObject(ExecutionEvent event) {
		ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();
		return getSelectedGraphicalObject(selection);
	}

	static private CSSDiagram getSelectedGraphicalObject(Object selection) {
		Object selectedObject = selection;

		if (selectedObject instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectedObject).getFirstElement();
			if (firstElement instanceof EObjectTreeElement) {
				if (((EObjectTreeElement) firstElement).getEObject() instanceof CSSDiagram) {
					CSSDiagram diagram = (CSSDiagram) ((EObjectTreeElement) firstElement).getEObject();
					return diagram;
				}
			}
		}

		return null;
	}

}

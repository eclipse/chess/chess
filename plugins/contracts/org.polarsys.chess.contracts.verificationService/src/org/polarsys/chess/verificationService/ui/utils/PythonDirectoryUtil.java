/*******************************************************************************
 * Copyright (C) 2017 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
package org.polarsys.chess.verificationService.ui.utils;

import java.io.File;

import org.apache.log4j.Logger;

import eu.fbk.eclipse.standardtools.utils.ui.utils.DirectoryUtil;

/**
 * 
 *
 */
public class PythonDirectoryUtil {
	
	private final String PYTHON_FILES_PATH = File.separator+"Python";
	private final String PYTHON_K2_DIR_PATH = PYTHON_FILES_PATH + File.separator+"Files";
	
	private static PythonDirectoryUtil pythonDirectoryUtil;
	private static DirectoryUtil directoryUtil = DirectoryUtil.getInstance();

	public static PythonDirectoryUtil getInstance() {
		if (pythonDirectoryUtil == null) {
			pythonDirectoryUtil = new PythonDirectoryUtil();
		}
		return pythonDirectoryUtil;
	}

	public String getPythonFileDirectory() throws Exception{
		return directoryUtil.getCurrentProjectDir() + PYTHON_FILES_PATH;
	}

	public String getPythonDirPath() throws Exception {
		return directoryUtil.getCurrentProjectDir() + PYTHON_K2_DIR_PATH;		
	}
		
}

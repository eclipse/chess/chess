/*******************************************************************************
 * Copyright (C) 2017 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package org.polarsys.chess.verificationService.ui.commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.uml2.uml.Class;

import eu.fbk.eclipse.standardtools.StateMachineTranslatorToSmv.ui.services.PyExportServiceUI;
import eu.fbk.eclipse.standardtools.utils.ui.commands.AbstractJobCommand;
import org.polarsys.chess.service.core.model.ChessSystemModel;
import org.polarsys.chess.service.core.model.UMLStateMachineModel;
import org.polarsys.chess.service.gui.utils.SelectionUtil;
import org.polarsys.chess.verificationService.ui.utils.PythonDirectoryUtil;

/**
 * 
 *
 */
public class ExportBlockToPythonFileCommand extends AbstractJobCommand {

	private SelectionUtil selectionUtil = SelectionUtil.getInstance();
	private PyExportServiceUI pythonExportService = PyExportServiceUI
			.getInstance(ChessSystemModel.getInstance(),UMLStateMachineModel.getInstance());
	private PythonDirectoryUtil pythonDirectoryUtil = PythonDirectoryUtil.getInstance();

	public ExportBlockToPythonFileCommand() {
		super("Export Model To .py File");
	}

	private String pythonFilepath;
	private boolean showPopups;
	private Class umlSelectedComponent;

	@Override
	public void execPreJobOperations(ExecutionEvent event, IProgressMonitor monitor) throws Exception {
		pythonExportService = PyExportServiceUI
				.getInstance(ChessSystemModel.getInstance(),UMLStateMachineModel.getInstance());
		umlSelectedComponent = selectionUtil.getUmlComponentFromSelectedObject(event);
		pythonFilepath = pythonDirectoryUtil.getPythonDirPath();
		showPopups = true;
	}

	@Override
	public void execJobCommand(ExecutionEvent event, IProgressMonitor monitor) throws Exception {

		pythonExportService.exportSingleBlock(umlSelectedComponent, showPopups, null, pythonFilepath, monitor);

	}

	@Override
	public void execPostJobOperations(ExecutionEvent event, NullProgressMonitor nullProgressMonitor) throws Exception {
		// TODO Auto-generated method stub
		
	}

}

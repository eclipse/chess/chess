/*******************************************************************************
 * Copyright (C) 2018 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *  
 * Contributors:
 *   Alberto Debiasi - initial API and implementation
 ******************************************************************************/
package org.polarsys.chess.contracts.verificationService.test.runtime.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;



public class TestUtil {
	
	private static TestUtil testUtil;

	public static TestUtil getInstance() {
		if (testUtil == null) {
			testUtil = new TestUtil();
		}
		return testUtil;
	}
	
	
	public Properties getConfigTestProperties() throws IOException{
		File configFile = new File("configTest.properties");
	
		    FileReader reader = new FileReader(configFile);
		    Properties props = new Properties();
		    props.load(reader);
		    reader.close();
		 return props;
		  
	}
	
}

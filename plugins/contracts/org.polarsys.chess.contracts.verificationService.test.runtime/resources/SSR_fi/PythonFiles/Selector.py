from enum import IntEnum

class Selector():

    def __init__(self):
        self.curr_init_transition = self.InitTransition.T_Undef
        self.curr_transition = self.Transition.T_Undef

    class State(IntEnum):
        S_Undef = 0
        input_1 = 1
        input_2 = 2

    # enumerative class used by the current init transition variable
    # T_Undef is the value used when the current init transition variable is initialized
    # the other value is the name of the init transition of the state machine
    class InitTransition(IntEnum):
        T_Undef = 0
        init_to_in1 = 1

    # enumerative class used by the current transition variable
    # T_Undef is the value used when the current transition variable is initialized
    # the other values are the names of the transitions of the state machine
    class Transition(IntEnum):
        T_Undef = 0
        in1_to_in1 = 1
        in1_to_in2 = 2
        in2_to_in1 = 3
        in2_to_in2 = 4




    def is_current_state(self, stateval):
        return self.get_fsmState() == stateval

    def set_fsmState(self, stateval):
        self.state = stateval

    def get_fsmState(self):
        return self.state

    def init(self):
        if(self.guard_init_to_in1()):
            self.curr_init_transition = self.InitTransition.init_to_in1
        else:
            # if the current init transition is not set, an exception will be raised
            print("the current init transition is not set")

        # check what is the current initial transition
        # to set the current state and to execute the effects of the current initial transition
        if self.curr_init_transition == self.InitTransition.T_Undef:
            # if the current init transition variable is not set, an exception will be raised
             print("the current init transition is not set")
        elif self.curr_init_transition == self.InitTransition.init_to_in1:
            self.set_fsmState(self.State.input_1)
            self.effect_init_to_in1()
            



    def execute(self):

        self.curr_transition = self.Transition.T_Undef
        if self.is_current_state(self.State.input_1):
            if(self.guard_in1_to_in1()):
                self.curr_transition = self.Transition.in1_to_in1
            elif(self.guard_in1_to_in2()):
                self.curr_transition = self.Transition.in1_to_in2
        elif self.is_current_state(self.State.input_2):
            if(self.guard_in2_to_in1()):
                self.curr_transition = self.Transition.in2_to_in1
            elif(self.guard_in2_to_in2()):
                self.curr_transition = self.Transition.in2_to_in2
        else:
            # if the current state variable is not set, an exception will be raised
            print("the current state variable is not set")

        if self.curr_transition == self.Transition.T_Undef:
            # if the current transition variable is not set, an exception will be raised
            print("the current transition variable is not set")
        elif self.curr_transition == self.Transition.in1_to_in1:
            self.set_fsmState(self.State.input_1)
            self.effect_in1_to_in1()
        elif self.curr_transition == self.Transition.in1_to_in2:
            self.set_fsmState(self.State.input_2)
            self.effect_in1_to_in2()
        elif self.curr_transition == self.Transition.in2_to_in1:
            self.set_fsmState(self.State.input_1)
            self.effect_in2_to_in1()
        elif self.curr_transition == self.Transition.in2_to_in2:
            self.set_fsmState(self.State.input_2)
            self.effect_in2_to_in2()

    def set_input(self , input1, input1_is_present, input2, input2_is_present, switch_current_use):
        self.input1 = input1
        self.input1_is_present = input1_is_present
        self.input2 = input2
        self.input2_is_present = input2_is_present
        self.switch_current_use = switch_current_use

    # setters for inputPorts
    def set_input1(self, value):
        self.input1 = value

    def set_input1_is_present(self, value):
        self.input1_is_present = value

    def set_input2(self, value):
        self.input2 = value

    def set_input2_is_present(self, value):
        self.input2_is_present = value

    def set_switch_current_use(self, value):
        self.switch_current_use = value


    # getters for inputPorts
    def get_input1(self):
        return self.input1

    def get_input1_is_present(self):
        return self.input1_is_present

    def get_input2(self):
        return self.input2

    def get_input2_is_present(self):
        return self.input2_is_present

    def get_switch_current_use(self):
        return self.switch_current_use


    # setters for outputPorts
    def set_current_use(self, value):
        self.current_use = value

    def set_output(self, value):
        self.output = value

    def set_output_is_present(self, value):
        self.output_is_present = value


    # getters for outputPorts
    def get_current_use(self):
        return self.current_use

    def get_output(self):
        return self.output

    def get_output_is_present(self):
        return self.output_is_present


    # setters for attributes

    # getters for attributes

    def guard_init_to_in1(self):
        return True
	
    def guard_in1_to_in1(self):
        return not self.get_switch_current_use()
	
    def guard_in1_to_in2(self):
        return self.get_switch_current_use() == True
	
    def guard_in2_to_in1(self):
        return self.get_switch_current_use() == True
	
    def guard_in2_to_in2(self):
        return not self.get_switch_current_use()
	

    def effect_init_to_in1(self):
        self.set_current_use(1)
        self.set_output(0.0)
        self.set_output_is_present(True)
	
    def effect_in1_to_in1(self):
        self.set_output(self.get_input1())
        self.set_output_is_present(self.get_input1_is_present())

    def effect_in1_to_in2(self):
        self.set_current_use(2)
        self.set_output(self.get_input2())
        self.set_output_is_present(self.get_input2_is_present())

    def effect_in2_to_in1(self):
        self.set_current_use(1)
        self.set_output(self.get_input1())
        self.set_output_is_present(self.get_input1_is_present())

    def effect_in2_to_in2(self):
        self.set_output(self.get_input2())
        self.set_output_is_present(self.get_input2_is_present())



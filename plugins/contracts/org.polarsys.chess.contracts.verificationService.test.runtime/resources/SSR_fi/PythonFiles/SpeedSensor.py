from enum import IntEnum

class SpeedSensor():

    def __init__(self):
        self.curr_init_transition = self.InitTransition.T_Undef
        self.curr_transition = self.Transition.T_Undef

    class State(IntEnum):
        S_Undef = 0
        primary = 1

    # enumerative class used by the current init transition variable
    # T_Undef is the value used when the current init transition variable is initialized
    # the other value is the name of the init transition of the state machine
    class InitTransition(IntEnum):
        T_Undef = 0
        init_to_primary = 1

    # enumerative class used by the current transition variable
    # T_Undef is the value used when the current transition variable is initialized
    # the other values are the names of the transitions of the state machine
    class Transition(IntEnum):
        T_Undef = 0
        pr_to_pr = 1




    def is_current_state(self, stateval):
        return self.get_fsmState() == stateval

    def set_fsmState(self, stateval):
        self.state = stateval

    def get_fsmState(self):
        return self.state

    def init(self):
        if(self.guard_init_to_primary()):
            self.curr_init_transition = self.InitTransition.init_to_primary
        else:
            # if the current init transition is not set, an exception will be raised
            print("the current init transition is not set")

        # check what is the current initial transition
        # to set the current state and to execute the effects of the current initial transition
        if self.curr_init_transition == self.InitTransition.T_Undef:
            # if the current init transition variable is not set, an exception will be raised
             print("the current init transition is not set")
        elif self.curr_init_transition == self.InitTransition.init_to_primary:
            self.set_fsmState(self.State.primary)
            self.effect_init_to_primary()
            



    def execute(self):

        self.curr_transition = self.Transition.T_Undef
        if self.is_current_state(self.State.primary):
            if(self.guard_pr_to_pr()):
                self.curr_transition = self.Transition.pr_to_pr
        else:
            # if the current state variable is not set, an exception will be raised
            print("the current state variable is not set")

        if self.curr_transition == self.Transition.T_Undef:
            # if the current transition variable is not set, an exception will be raised
            print("the current transition variable is not set")
        elif self.curr_transition == self.Transition.pr_to_pr:
            self.set_fsmState(self.State.primary)
            self.effect_pr_to_pr()

    def set_input(self , speed):
        self.speed = speed

    # setters for inputPorts
    def set_speed(self, value):
        self.speed = value


    # getters for inputPorts
    def get_speed(self):
        return self.speed


    # setters for outputPorts
    def set_sensed_speed(self, value):
        self.sensed_speed = value

    def set_sensed_speed_is_present(self, value):
        self.sensed_speed_is_present = value


    # getters for outputPorts
    def get_sensed_speed(self):
        return self.sensed_speed

    def get_sensed_speed_is_present(self):
        return self.sensed_speed_is_present


    # setters for attributes

    # getters for attributes

    def guard_init_to_primary(self):
        return True
	
    def guard_pr_to_pr(self):
        return True
	

    def effect_init_to_primary(self):
        self.set_sensed_speed(0.0)
        self.set_sensed_speed_is_present(True)
	
    def effect_pr_to_pr(self):
        self.set_sensed_speed_is_present(True)
        self.set_sensed_speed(self.get_speed())



/**
 */
package org.polarsys.chess.chessmlprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>dummy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.chessmlprofilePackage#getdummy()
 * @model
 * @generated
 */
public interface dummy extends EObject {
} // dummy

/**
 */
package org.polarsys.chess.chessmlprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.polarsys.chess.chessmlprofile.chessmlprofilePackage;
import org.polarsys.chess.chessmlprofile.dummy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>dummy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class dummyImpl extends MinimalEObjectImpl.Container implements dummy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected dummyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return chessmlprofilePackage.Literals.DUMMY;
	}

} //dummyImpl

/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.MTTF;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MTTF</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MTTFImpl extends DependabilityMeasureImpl implements MTTF {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MTTFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.MTTF;
	}

} //MTTFImpl

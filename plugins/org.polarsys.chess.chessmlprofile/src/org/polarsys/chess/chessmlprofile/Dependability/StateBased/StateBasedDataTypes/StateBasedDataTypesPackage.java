/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesFactory
 * @model kind="package"
 * @generated
 */
public interface StateBasedDataTypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "StateBasedDataTypes";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///CHESS/Dependability/StateBased/StateBasedDataTypes.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "CHESS.Dependability.StateBased.StateBasedDataTypes";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StateBasedDataTypesPackage eINSTANCE = org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind <em>Redundancy Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getRedundancyKind()
	 * @generated
	 */
	int REDUNDANCY_KIND = 9;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind <em>Confidence Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getConfidenceKind()
	 * @generated
	 */
	int CONFIDENCE_KIND = 10;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl <em>Dependability Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getDependabilityMeasure()
	 * @generated
	 */
	int DEPENDABILITY_MEASURE = 0;

	/**
	 * The feature id for the '<em><b>Eval Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDABILITY_MEASURE__EVAL_PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Required Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDABILITY_MEASURE__REQUIRED_MIN = 1;

	/**
	 * The feature id for the '<em><b>Required Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDABILITY_MEASURE__REQUIRED_MAX = 2;

	/**
	 * The number of structural features of the '<em>Dependability Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDABILITY_MEASURE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Dependability Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPENDABILITY_MEASURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl <em>Evaluation Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationMethod()
	 * @generated
	 */
	int EVALUATION_METHOD = 1;

	/**
	 * The feature id for the '<em><b>Simulation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_METHOD__SIMULATION = 0;

	/**
	 * The feature id for the '<em><b>Analytical</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_METHOD__ANALYTICAL = 1;

	/**
	 * The number of structural features of the '<em>Evaluation Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_METHOD_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Evaluation Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_METHOD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl <em>Evaluation By Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationBySimulation()
	 * @generated
	 */
	int EVALUATION_BY_SIMULATION = 2;

	/**
	 * The feature id for the '<em><b>Simulation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__SIMULATION = EVALUATION_METHOD__SIMULATION;

	/**
	 * The feature id for the '<em><b>Analytical</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__ANALYTICAL = EVALUATION_METHOD__ANALYTICAL;

	/**
	 * The feature id for the '<em><b>Confidence Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL = EVALUATION_METHOD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Confidence Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL = EVALUATION_METHOD_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__KIND = EVALUATION_METHOD_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Max Batches</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION__MAX_BATCHES = EVALUATION_METHOD_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Evaluation By Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION_FEATURE_COUNT = EVALUATION_METHOD_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Evaluation By Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_BY_SIMULATION_OPERATION_COUNT = EVALUATION_METHOD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl <em>Evaluation Analytical</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationAnalytical()
	 * @generated
	 */
	int EVALUATION_ANALYTICAL = 3;

	/**
	 * The feature id for the '<em><b>Simulation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL__SIMULATION = EVALUATION_METHOD__SIMULATION;

	/**
	 * The feature id for the '<em><b>Analytical</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL__ANALYTICAL = EVALUATION_METHOD__ANALYTICAL;

	/**
	 * The feature id for the '<em><b>Accuracy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL__ACCURACY = EVALUATION_METHOD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Iteration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL__MAX_ITERATION = EVALUATION_METHOD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Evaluation Analytical</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL_FEATURE_COUNT = EVALUATION_METHOD_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Evaluation Analytical</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_ANALYTICAL_OPERATION_COUNT = EVALUATION_METHOD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.ReliabilityImpl <em>Reliability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.ReliabilityImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getReliability()
	 * @generated
	 */
	int RELIABILITY = 4;

	/**
	 * The feature id for the '<em><b>Eval Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY__EVAL_PROPERTIES = DEPENDABILITY_MEASURE__EVAL_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Required Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY__REQUIRED_MIN = DEPENDABILITY_MEASURE__REQUIRED_MIN;

	/**
	 * The feature id for the '<em><b>Required Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY__REQUIRED_MAX = DEPENDABILITY_MEASURE__REQUIRED_MAX;

	/**
	 * The feature id for the '<em><b>Instant Of Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY__INSTANT_OF_TIME = DEPENDABILITY_MEASURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reliability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY_FEATURE_COUNT = DEPENDABILITY_MEASURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reliability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELIABILITY_OPERATION_COUNT = DEPENDABILITY_MEASURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl <em>Evaluation Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationType()
	 * @generated
	 */
	int EVALUATION_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Steady</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_TYPE__STEADY = 0;

	/**
	 * The feature id for the '<em><b>Instant Of Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_TYPE__INSTANT_OF_TIME = 1;

	/**
	 * The feature id for the '<em><b>Interval Of Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_TYPE__INTERVAL_OF_TIME = 2;

	/**
	 * The number of structural features of the '<em>Evaluation Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Evaluation Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.SteadyStateImpl <em>Steady State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.SteadyStateImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getSteadyState()
	 * @generated
	 */
	int STEADY_STATE = 6;

	/**
	 * The feature id for the '<em><b>Initial Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEADY_STATE__INITIAL_TRANSIENT = 0;

	/**
	 * The feature id for the '<em><b>Batch Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEADY_STATE__BATCH_SIZE = 1;

	/**
	 * The number of structural features of the '<em>Steady State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEADY_STATE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Steady State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEADY_STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.AvailabilityImpl <em>Availability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.AvailabilityImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getAvailability()
	 * @generated
	 */
	int AVAILABILITY = 7;

	/**
	 * The feature id for the '<em><b>Eval Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY__EVAL_PROPERTIES = DEPENDABILITY_MEASURE__EVAL_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Required Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY__REQUIRED_MIN = DEPENDABILITY_MEASURE__REQUIRED_MIN;

	/**
	 * The feature id for the '<em><b>Required Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY__REQUIRED_MAX = DEPENDABILITY_MEASURE__REQUIRED_MAX;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY__EVALUATION = DEPENDABILITY_MEASURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Availability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY_FEATURE_COUNT = DEPENDABILITY_MEASURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Availability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVAILABILITY_OPERATION_COUNT = DEPENDABILITY_MEASURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.MTTFImpl <em>MTTF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.MTTFImpl
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getMTTF()
	 * @generated
	 */
	int MTTF = 8;


	/**
	 * The feature id for the '<em><b>Eval Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTTF__EVAL_PROPERTIES = DEPENDABILITY_MEASURE__EVAL_PROPERTIES;

	/**
	 * The feature id for the '<em><b>Required Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTTF__REQUIRED_MIN = DEPENDABILITY_MEASURE__REQUIRED_MIN;

	/**
	 * The feature id for the '<em><b>Required Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTTF__REQUIRED_MAX = DEPENDABILITY_MEASURE__REQUIRED_MAX;

	/**
	 * The number of structural features of the '<em>MTTF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTTF_FEATURE_COUNT = DEPENDABILITY_MEASURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>MTTF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTTF_OPERATION_COUNT = DEPENDABILITY_MEASURE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for enum '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind <em>Redundancy Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Redundancy Kind</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind
	 * @generated
	 */
	EEnum getRedundancyKind();

	/**
	 * Returns the meta object for enum '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind <em>Confidence Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Confidence Kind</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind
	 * @generated
	 */
	EEnum getConfidenceKind();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure <em>Dependability Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dependability Measure</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure
	 * @generated
	 */
	EClass getDependabilityMeasure();

	/**
	 * Returns the meta object for the containment reference '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getEvalProperties <em>Eval Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Eval Properties</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getEvalProperties()
	 * @see #getDependabilityMeasure()
	 * @generated
	 */
	EReference getDependabilityMeasure_EvalProperties();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getRequiredMin <em>Required Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required Min</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getRequiredMin()
	 * @see #getDependabilityMeasure()
	 * @generated
	 */
	EAttribute getDependabilityMeasure_RequiredMin();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getRequiredMax <em>Required Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Required Max</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure#getRequiredMax()
	 * @see #getDependabilityMeasure()
	 * @generated
	 */
	EAttribute getDependabilityMeasure_RequiredMax();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod <em>Evaluation Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Method</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod
	 * @generated
	 */
	EClass getEvaluationMethod();

	/**
	 * Returns the meta object for the containment reference '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getSimulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Simulation</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getSimulation()
	 * @see #getEvaluationMethod()
	 * @generated
	 */
	EReference getEvaluationMethod_Simulation();

	/**
	 * Returns the meta object for the containment reference '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getAnalytical <em>Analytical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Analytical</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getAnalytical()
	 * @see #getEvaluationMethod()
	 * @generated
	 */
	EReference getEvaluationMethod_Analytical();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation <em>Evaluation By Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation By Simulation</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation
	 * @generated
	 */
	EClass getEvaluationBySimulation();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceInterval <em>Confidence Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Interval</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceInterval()
	 * @see #getEvaluationBySimulation()
	 * @generated
	 */
	EAttribute getEvaluationBySimulation_ConfidenceInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceLevel <em>Confidence Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confidence Level</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceLevel()
	 * @see #getEvaluationBySimulation()
	 * @generated
	 */
	EAttribute getEvaluationBySimulation_ConfidenceLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getKind()
	 * @see #getEvaluationBySimulation()
	 * @generated
	 */
	EAttribute getEvaluationBySimulation_Kind();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getMaxBatches <em>Max Batches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Batches</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getMaxBatches()
	 * @see #getEvaluationBySimulation()
	 * @generated
	 */
	EAttribute getEvaluationBySimulation_MaxBatches();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical <em>Evaluation Analytical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Analytical</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical
	 * @generated
	 */
	EClass getEvaluationAnalytical();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical#getAccuracy <em>Accuracy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Accuracy</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical#getAccuracy()
	 * @see #getEvaluationAnalytical()
	 * @generated
	 */
	EAttribute getEvaluationAnalytical_Accuracy();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical#getMaxIteration <em>Max Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Iteration</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical#getMaxIteration()
	 * @see #getEvaluationAnalytical()
	 * @generated
	 */
	EAttribute getEvaluationAnalytical_MaxIteration();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability <em>Reliability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reliability</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability
	 * @generated
	 */
	EClass getReliability();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability#getInstantOfTime <em>Instant Of Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instant Of Time</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability#getInstantOfTime()
	 * @see #getReliability()
	 * @generated
	 */
	EAttribute getReliability_InstantOfTime();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType <em>Evaluation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Type</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType
	 * @generated
	 */
	EClass getEvaluationType();

	/**
	 * Returns the meta object for the containment reference '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getSteady <em>Steady</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Steady</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getSteady()
	 * @see #getEvaluationType()
	 * @generated
	 */
	EReference getEvaluationType_Steady();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getInstantOfTime <em>Instant Of Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instant Of Time</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getInstantOfTime()
	 * @see #getEvaluationType()
	 * @generated
	 */
	EAttribute getEvaluationType_InstantOfTime();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getIntervalOfTime <em>Interval Of Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interval Of Time</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType#getIntervalOfTime()
	 * @see #getEvaluationType()
	 * @generated
	 */
	EAttribute getEvaluationType_IntervalOfTime();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState <em>Steady State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Steady State</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState
	 * @generated
	 */
	EClass getSteadyState();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState#getInitialTransient <em>Initial Transient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Transient</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState#getInitialTransient()
	 * @see #getSteadyState()
	 * @generated
	 */
	EAttribute getSteadyState_InitialTransient();

	/**
	 * Returns the meta object for the attribute '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState#getBatchSize <em>Batch Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Batch Size</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState#getBatchSize()
	 * @see #getSteadyState()
	 * @generated
	 */
	EAttribute getSteadyState_BatchSize();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Availability <em>Availability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Availability</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Availability
	 * @generated
	 */
	EClass getAvailability();

	/**
	 * Returns the meta object for the containment reference '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Availability#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Evaluation</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Availability#getEvaluation()
	 * @see #getAvailability()
	 * @generated
	 */
	EReference getAvailability_Evaluation();

	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.MTTF <em>MTTF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MTTF</em>'.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.MTTF
	 * @generated
	 */
	EClass getMTTF();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StateBasedDataTypesFactory getStateBasedDataTypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind <em>Redundancy Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.RedundancyKind
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getRedundancyKind()
		 * @generated
		 */
		EEnum REDUNDANCY_KIND = eINSTANCE.getRedundancyKind();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind <em>Confidence Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getConfidenceKind()
		 * @generated
		 */
		EEnum CONFIDENCE_KIND = eINSTANCE.getConfidenceKind();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl <em>Dependability Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getDependabilityMeasure()
		 * @generated
		 */
		EClass DEPENDABILITY_MEASURE = eINSTANCE.getDependabilityMeasure();

		/**
		 * The meta object literal for the '<em><b>Eval Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPENDABILITY_MEASURE__EVAL_PROPERTIES = eINSTANCE.getDependabilityMeasure_EvalProperties();

		/**
		 * The meta object literal for the '<em><b>Required Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDABILITY_MEASURE__REQUIRED_MIN = eINSTANCE.getDependabilityMeasure_RequiredMin();

		/**
		 * The meta object literal for the '<em><b>Required Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPENDABILITY_MEASURE__REQUIRED_MAX = eINSTANCE.getDependabilityMeasure_RequiredMax();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl <em>Evaluation Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationMethod()
		 * @generated
		 */
		EClass EVALUATION_METHOD = eINSTANCE.getEvaluationMethod();

		/**
		 * The meta object literal for the '<em><b>Simulation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_METHOD__SIMULATION = eINSTANCE.getEvaluationMethod_Simulation();

		/**
		 * The meta object literal for the '<em><b>Analytical</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_METHOD__ANALYTICAL = eINSTANCE.getEvaluationMethod_Analytical();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl <em>Evaluation By Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationBySimulation()
		 * @generated
		 */
		EClass EVALUATION_BY_SIMULATION = eINSTANCE.getEvaluationBySimulation();

		/**
		 * The meta object literal for the '<em><b>Confidence Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL = eINSTANCE.getEvaluationBySimulation_ConfidenceInterval();

		/**
		 * The meta object literal for the '<em><b>Confidence Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL = eINSTANCE.getEvaluationBySimulation_ConfidenceLevel();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_BY_SIMULATION__KIND = eINSTANCE.getEvaluationBySimulation_Kind();

		/**
		 * The meta object literal for the '<em><b>Max Batches</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_BY_SIMULATION__MAX_BATCHES = eINSTANCE.getEvaluationBySimulation_MaxBatches();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl <em>Evaluation Analytical</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationAnalytical()
		 * @generated
		 */
		EClass EVALUATION_ANALYTICAL = eINSTANCE.getEvaluationAnalytical();

		/**
		 * The meta object literal for the '<em><b>Accuracy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_ANALYTICAL__ACCURACY = eINSTANCE.getEvaluationAnalytical_Accuracy();

		/**
		 * The meta object literal for the '<em><b>Max Iteration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_ANALYTICAL__MAX_ITERATION = eINSTANCE.getEvaluationAnalytical_MaxIteration();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.ReliabilityImpl <em>Reliability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.ReliabilityImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getReliability()
		 * @generated
		 */
		EClass RELIABILITY = eINSTANCE.getReliability();

		/**
		 * The meta object literal for the '<em><b>Instant Of Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELIABILITY__INSTANT_OF_TIME = eINSTANCE.getReliability_InstantOfTime();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl <em>Evaluation Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getEvaluationType()
		 * @generated
		 */
		EClass EVALUATION_TYPE = eINSTANCE.getEvaluationType();

		/**
		 * The meta object literal for the '<em><b>Steady</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_TYPE__STEADY = eINSTANCE.getEvaluationType_Steady();

		/**
		 * The meta object literal for the '<em><b>Instant Of Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_TYPE__INSTANT_OF_TIME = eINSTANCE.getEvaluationType_InstantOfTime();

		/**
		 * The meta object literal for the '<em><b>Interval Of Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_TYPE__INTERVAL_OF_TIME = eINSTANCE.getEvaluationType_IntervalOfTime();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.SteadyStateImpl <em>Steady State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.SteadyStateImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getSteadyState()
		 * @generated
		 */
		EClass STEADY_STATE = eINSTANCE.getSteadyState();

		/**
		 * The meta object literal for the '<em><b>Initial Transient</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEADY_STATE__INITIAL_TRANSIENT = eINSTANCE.getSteadyState_InitialTransient();

		/**
		 * The meta object literal for the '<em><b>Batch Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEADY_STATE__BATCH_SIZE = eINSTANCE.getSteadyState_BatchSize();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.AvailabilityImpl <em>Availability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.AvailabilityImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getAvailability()
		 * @generated
		 */
		EClass AVAILABILITY = eINSTANCE.getAvailability();

		/**
		 * The meta object literal for the '<em><b>Evaluation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AVAILABILITY__EVALUATION = eINSTANCE.getAvailability_Evaluation();

		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.MTTFImpl <em>MTTF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.MTTFImpl
		 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl#getMTTF()
		 * @generated
		 */
		EClass MTTF = eINSTANCE.getMTTF();

	}

} //StateBasedDataTypesPackage

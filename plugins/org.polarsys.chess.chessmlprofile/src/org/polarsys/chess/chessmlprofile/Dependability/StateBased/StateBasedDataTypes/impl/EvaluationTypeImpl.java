/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationType;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.SteadyState;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl#getSteady <em>Steady</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl#getInstantOfTime <em>Instant Of Time</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationTypeImpl#getIntervalOfTime <em>Interval Of Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationTypeImpl extends MinimalEObjectImpl.Container implements EvaluationType {
	/**
	 * The cached value of the '{@link #getSteady() <em>Steady</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSteady()
	 * @generated
	 * @ordered
	 */
	protected SteadyState steady;
	/**
	 * The default value of the '{@link #getInstantOfTime() <em>Instant Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantOfTime()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANT_OF_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getInstantOfTime() <em>Instant Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantOfTime()
	 * @generated
	 * @ordered
	 */
	protected String instantOfTime = INSTANT_OF_TIME_EDEFAULT;
	/**
	 * The default value of the '{@link #getIntervalOfTime() <em>Interval Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntervalOfTime()
	 * @generated
	 * @ordered
	 */
	protected static final String INTERVAL_OF_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getIntervalOfTime() <em>Interval Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntervalOfTime()
	 * @generated
	 * @ordered
	 */
	protected String intervalOfTime = INTERVAL_OF_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.EVALUATION_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SteadyState getSteady() {
		return steady;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSteady(SteadyState newSteady, NotificationChain msgs) {
		SteadyState oldSteady = steady;
		steady = newSteady;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY, oldSteady, newSteady);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSteady(SteadyState newSteady) {
		if (newSteady != steady) {
			NotificationChain msgs = null;
			if (steady != null)
				msgs = ((InternalEObject)steady).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY, null, msgs);
			if (newSteady != null)
				msgs = ((InternalEObject)newSteady).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY, null, msgs);
			msgs = basicSetSteady(newSteady, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY, newSteady, newSteady));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInstantOfTime() {
		return instantOfTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstantOfTime(String newInstantOfTime) {
		String oldInstantOfTime = instantOfTime;
		instantOfTime = newInstantOfTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_TYPE__INSTANT_OF_TIME, oldInstantOfTime, instantOfTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIntervalOfTime() {
		return intervalOfTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIntervalOfTime(String newIntervalOfTime) {
		String oldIntervalOfTime = intervalOfTime;
		intervalOfTime = newIntervalOfTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_TYPE__INTERVAL_OF_TIME, oldIntervalOfTime, intervalOfTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY:
				return basicSetSteady(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY:
				return getSteady();
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INSTANT_OF_TIME:
				return getInstantOfTime();
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INTERVAL_OF_TIME:
				return getIntervalOfTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY:
				setSteady((SteadyState)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INSTANT_OF_TIME:
				setInstantOfTime((String)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INTERVAL_OF_TIME:
				setIntervalOfTime((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY:
				setSteady((SteadyState)null);
				return;
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INSTANT_OF_TIME:
				setInstantOfTime(INSTANT_OF_TIME_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INTERVAL_OF_TIME:
				setIntervalOfTime(INTERVAL_OF_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_TYPE__STEADY:
				return steady != null;
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INSTANT_OF_TIME:
				return INSTANT_OF_TIME_EDEFAULT == null ? instantOfTime != null : !INSTANT_OF_TIME_EDEFAULT.equals(instantOfTime);
			case StateBasedDataTypesPackage.EVALUATION_TYPE__INTERVAL_OF_TIME:
				return INTERVAL_OF_TIME_EDEFAULT == null ? intervalOfTime != null : !INTERVAL_OF_TIME_EDEFAULT.equals(intervalOfTime);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (instantOfTime: ");
		result.append(instantOfTime);
		result.append(", intervalOfTime: ");
		result.append(intervalOfTime);
		result.append(')');
		return result.toString();
	}

} //EvaluationTypeImpl

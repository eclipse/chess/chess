/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reliability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability#getInstantOfTime <em>Instant Of Time</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getReliability()
 * @model
 * @generated
 */
public interface Reliability extends DependabilityMeasure {

	/**
	 * Returns the value of the '<em><b>Instant Of Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instant Of Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instant Of Time</em>' attribute.
	 * @see #setInstantOfTime(String)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getReliability_InstantOfTime()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getInstantOfTime();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability#getInstantOfTime <em>Instant Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instant Of Time</em>' attribute.
	 * @see #getInstantOfTime()
	 * @generated
	 */
	void setInstantOfTime(String value);
} // Reliability

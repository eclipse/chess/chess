/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaAnalysisContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage#getFailurePropagationAnalysis()
 * @model
 * @generated
 */
public interface FailurePropagationAnalysis extends GaAnalysisContext {
} // FailurePropagationAnalysis

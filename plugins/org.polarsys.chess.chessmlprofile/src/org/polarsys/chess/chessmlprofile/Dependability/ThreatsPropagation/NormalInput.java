/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Normal Input</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.ThreatsPropagationPackage#getNormalInput()
 * @model
 * @generated
 */
public interface NormalInput extends DepEvent {
} // NormalInput

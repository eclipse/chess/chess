/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTTF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getMTTF()
 * @model
 * @generated
 */
public interface MTTF extends DependabilityMeasure {
} // MTTF

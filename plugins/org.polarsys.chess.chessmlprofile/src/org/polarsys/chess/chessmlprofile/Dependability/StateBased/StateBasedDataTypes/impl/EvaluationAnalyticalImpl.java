/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Analytical</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl#getAccuracy <em>Accuracy</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationAnalyticalImpl#getMaxIteration <em>Max Iteration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationAnalyticalImpl extends EvaluationMethodImpl implements EvaluationAnalytical {
	/**
	 * The default value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected static final String ACCURACY_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getAccuracy() <em>Accuracy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccuracy()
	 * @generated
	 * @ordered
	 */
	protected String accuracy = ACCURACY_EDEFAULT;
	/**
	 * The default value of the '{@link #getMaxIteration() <em>Max Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIteration()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_ITERATION_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getMaxIteration() <em>Max Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIteration()
	 * @generated
	 * @ordered
	 */
	protected int maxIteration = MAX_ITERATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationAnalyticalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.EVALUATION_ANALYTICAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAccuracy() {
		return accuracy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAccuracy(String newAccuracy) {
		String oldAccuracy = accuracy;
		accuracy = newAccuracy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__ACCURACY, oldAccuracy, accuracy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMaxIteration() {
		return maxIteration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMaxIteration(int newMaxIteration) {
		int oldMaxIteration = maxIteration;
		maxIteration = newMaxIteration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__MAX_ITERATION, oldMaxIteration, maxIteration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__ACCURACY:
				return getAccuracy();
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__MAX_ITERATION:
				return getMaxIteration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__ACCURACY:
				setAccuracy((String)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__MAX_ITERATION:
				setMaxIteration((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__ACCURACY:
				setAccuracy(ACCURACY_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__MAX_ITERATION:
				setMaxIteration(MAX_ITERATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__ACCURACY:
				return ACCURACY_EDEFAULT == null ? accuracy != null : !ACCURACY_EDEFAULT.equals(accuracy);
			case StateBasedDataTypesPackage.EVALUATION_ANALYTICAL__MAX_ITERATION:
				return maxIteration != MAX_ITERATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (accuracy: ");
		result.append(accuracy);
		result.append(", maxIteration: ");
		result.append(maxIteration);
		result.append(')');
		return result.toString();
	}

} //EvaluationAnalyticalImpl

/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FASpecification;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FI4FA Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FI4FASpecificationImpl extends FPTCSpecificationImpl implements FI4FASpecification {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FI4FASpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FailurePropagationPackage.Literals.FI4FA_SPECIFICATION;
	}

} //FI4FASpecificationImpl

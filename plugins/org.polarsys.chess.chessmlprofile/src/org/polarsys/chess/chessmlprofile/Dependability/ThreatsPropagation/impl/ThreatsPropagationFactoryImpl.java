/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ThreatsPropagationFactoryImpl extends EFactoryImpl implements ThreatsPropagationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ThreatsPropagationFactory init() {
		try {
			ThreatsPropagationFactory theThreatsPropagationFactory = (ThreatsPropagationFactory)EPackage.Registry.INSTANCE.getEFactory(ThreatsPropagationPackage.eNS_URI);
			if (theThreatsPropagationFactory != null) {
				return theThreatsPropagationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ThreatsPropagationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreatsPropagationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ThreatsPropagationPackage.DEGRADED_STATE: return createDegradedState();
			case ThreatsPropagationPackage.THREAT_STATE: return createThreatState();
			case ThreatsPropagationPackage.NORMAL_STATE: return createNormalState();
			case ThreatsPropagationPackage.ERROR_STATE: return createErrorState();
			case ThreatsPropagationPackage.STUCK_AT: return createStuckAt();
			case ThreatsPropagationPackage.STUCK_AT_FIXED: return createStuckAtFixed();
			case ThreatsPropagationPackage.INVERTED: return createInverted();
			case ThreatsPropagationPackage.RAMP_DOWN: return createRampDown();
			case ThreatsPropagationPackage.NORMAL_INPUT: return createNormalInput();
			case ThreatsPropagationPackage.NORMAL_OUTPUT: return createNormalOutput();
			case ThreatsPropagationPackage.FAILURE: return createFailure();
			case ThreatsPropagationPackage.INTERNAL_FAULT: return createInternalFault();
			case ThreatsPropagationPackage.INTERNAL_PROPAGATION: return createInternalPropagation();
			case ThreatsPropagationPackage.ERROR_DETECTION: return createErrorDetection();
			case ThreatsPropagationPackage.ERROR_HANDLING: return createErrorHandling();
			case ThreatsPropagationPackage.FAULT_HANDLING: return createFaultHandling();
			case ThreatsPropagationPackage.ATTACK: return createAttack();
			case ThreatsPropagationPackage.VULNERABILITY: return createVulnerability();
			case ThreatsPropagationPackage.ATTACK_SCENARIO: return createAttackScenario();
			case ThreatsPropagationPackage.ERROR_MODEL: return createErrorModel();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ThreatsPropagationPackage.RECOVERY_KIND:
				return createRecoveryKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ThreatsPropagationPackage.RECOVERY_KIND:
				return convertRecoveryKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DegradedState createDegradedState() {
		DegradedStateImpl degradedState = new DegradedStateImpl();
		return degradedState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ThreatState createThreatState() {
		ThreatStateImpl threatState = new ThreatStateImpl();
		return threatState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NormalState createNormalState() {
		NormalStateImpl normalState = new NormalStateImpl();
		return normalState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ErrorState createErrorState() {
		ErrorStateImpl errorState = new ErrorStateImpl();
		return errorState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StuckAt createStuckAt() {
		StuckAtImpl stuckAt = new StuckAtImpl();
		return stuckAt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StuckAtFixed createStuckAtFixed() {
		StuckAtFixedImpl stuckAtFixed = new StuckAtFixedImpl();
		return stuckAtFixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Inverted createInverted() {
		InvertedImpl inverted = new InvertedImpl();
		return inverted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RampDown createRampDown() {
		RampDownImpl rampDown = new RampDownImpl();
		return rampDown;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NormalInput createNormalInput() {
		NormalInputImpl normalInput = new NormalInputImpl();
		return normalInput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NormalOutput createNormalOutput() {
		NormalOutputImpl normalOutput = new NormalOutputImpl();
		return normalOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Failure createFailure() {
		FailureImpl failure = new FailureImpl();
		return failure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InternalFault createInternalFault() {
		InternalFaultImpl internalFault = new InternalFaultImpl();
		return internalFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InternalPropagation createInternalPropagation() {
		InternalPropagationImpl internalPropagation = new InternalPropagationImpl();
		return internalPropagation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ErrorDetection createErrorDetection() {
		ErrorDetectionImpl errorDetection = new ErrorDetectionImpl();
		return errorDetection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ErrorHandling createErrorHandling() {
		ErrorHandlingImpl errorHandling = new ErrorHandlingImpl();
		return errorHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FaultHandling createFaultHandling() {
		FaultHandlingImpl faultHandling = new FaultHandlingImpl();
		return faultHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Attack createAttack() {
		AttackImpl attack = new AttackImpl();
		return attack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Vulnerability createVulnerability() {
		VulnerabilityImpl vulnerability = new VulnerabilityImpl();
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AttackScenario createAttackScenario() {
		AttackScenarioImpl attackScenario = new AttackScenarioImpl();
		return attackScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ErrorModel createErrorModel() {
		ErrorModelImpl errorModel = new ErrorModelImpl();
		return errorModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecoveryKind createRecoveryKindFromString(EDataType eDataType, String initialValue) {
		RecoveryKind result = RecoveryKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRecoveryKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ThreatsPropagationPackage getThreatsPropagationPackage() {
		return (ThreatsPropagationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ThreatsPropagationPackage getPackage() {
		return ThreatsPropagationPackage.eINSTANCE;
	}

} //ThreatsPropagationFactoryImpl

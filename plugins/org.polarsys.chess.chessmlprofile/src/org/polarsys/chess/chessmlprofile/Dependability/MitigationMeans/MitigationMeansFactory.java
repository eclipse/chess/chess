/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.MitigationMeansPackage
 * @generated
 */
public interface MitigationMeansFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MitigationMeansFactory eINSTANCE = org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.impl.MitigationMeansFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Transmission With Protocol</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transmission With Protocol</em>'.
	 * @generated
	 */
	TransmissionWithProtocol createTransmissionWithProtocol();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MitigationMeansPackage getMitigationMeansPackage();

} //MitigationMeansFactory

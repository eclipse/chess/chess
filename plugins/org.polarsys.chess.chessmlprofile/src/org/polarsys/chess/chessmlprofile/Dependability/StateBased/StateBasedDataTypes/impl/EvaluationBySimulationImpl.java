/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation By Simulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl#getConfidenceInterval <em>Confidence Interval</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl#getConfidenceLevel <em>Confidence Level</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationBySimulationImpl#getMaxBatches <em>Max Batches</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationBySimulationImpl extends EvaluationMethodImpl implements EvaluationBySimulation {
	/**
	 * The default value of the '{@link #getConfidenceInterval() <em>Confidence Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceInterval()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_INTERVAL_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getConfidenceInterval() <em>Confidence Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceInterval()
	 * @generated
	 * @ordered
	 */
	protected String confidenceInterval = CONFIDENCE_INTERVAL_EDEFAULT;
	/**
	 * The default value of the '{@link #getConfidenceLevel() <em>Confidence Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIDENCE_LEVEL_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getConfidenceLevel() <em>Confidence Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfidenceLevel()
	 * @generated
	 * @ordered
	 */
	protected String confidenceLevel = CONFIDENCE_LEVEL_EDEFAULT;
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final ConfidenceKind KIND_EDEFAULT = ConfidenceKind.RELATIVE;
	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected ConfidenceKind kind = KIND_EDEFAULT;
	/**
	 * The default value of the '{@link #getMaxBatches() <em>Max Batches</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBatches()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_BATCHES_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getMaxBatches() <em>Max Batches</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBatches()
	 * @generated
	 * @ordered
	 */
	protected int maxBatches = MAX_BATCHES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationBySimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.EVALUATION_BY_SIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getConfidenceInterval() {
		return confidenceInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConfidenceInterval(String newConfidenceInterval) {
		String oldConfidenceInterval = confidenceInterval;
		confidenceInterval = newConfidenceInterval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL, oldConfidenceInterval, confidenceInterval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConfidenceLevel(String newConfidenceLevel) {
		String oldConfidenceLevel = confidenceLevel;
		confidenceLevel = newConfidenceLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL, oldConfidenceLevel, confidenceLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConfidenceKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKind(ConfidenceKind newKind) {
		ConfidenceKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMaxBatches() {
		return maxBatches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMaxBatches(int newMaxBatches) {
		int oldMaxBatches = maxBatches;
		maxBatches = newMaxBatches;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__MAX_BATCHES, oldMaxBatches, maxBatches));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL:
				return getConfidenceInterval();
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL:
				return getConfidenceLevel();
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__KIND:
				return getKind();
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__MAX_BATCHES:
				return getMaxBatches();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL:
				setConfidenceInterval((String)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL:
				setConfidenceLevel((String)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__KIND:
				setKind((ConfidenceKind)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__MAX_BATCHES:
				setMaxBatches((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL:
				setConfidenceInterval(CONFIDENCE_INTERVAL_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL:
				setConfidenceLevel(CONFIDENCE_LEVEL_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__MAX_BATCHES:
				setMaxBatches(MAX_BATCHES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_INTERVAL:
				return CONFIDENCE_INTERVAL_EDEFAULT == null ? confidenceInterval != null : !CONFIDENCE_INTERVAL_EDEFAULT.equals(confidenceInterval);
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__CONFIDENCE_LEVEL:
				return CONFIDENCE_LEVEL_EDEFAULT == null ? confidenceLevel != null : !CONFIDENCE_LEVEL_EDEFAULT.equals(confidenceLevel);
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__KIND:
				return kind != KIND_EDEFAULT;
			case StateBasedDataTypesPackage.EVALUATION_BY_SIMULATION__MAX_BATCHES:
				return maxBatches != MAX_BATCHES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (confidenceInterval: ");
		result.append(confidenceInterval);
		result.append(", confidenceLevel: ");
		result.append(confidenceLevel);
		result.append(", kind: ");
		result.append(kind);
		result.append(", maxBatches: ");
		result.append(maxBatches);
		result.append(')');
		return result.toString();
	}

} //EvaluationBySimulationImpl

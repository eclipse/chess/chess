/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationAnalytical;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl#getSimulation <em>Simulation</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.EvaluationMethodImpl#getAnalytical <em>Analytical</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationMethodImpl extends MinimalEObjectImpl.Container implements EvaluationMethod {
	/**
	 * The cached value of the '{@link #getSimulation() <em>Simulation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulation()
	 * @generated
	 * @ordered
	 */
	protected EvaluationBySimulation simulation;
	/**
	 * The cached value of the '{@link #getAnalytical() <em>Analytical</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnalytical()
	 * @generated
	 * @ordered
	 */
	protected EvaluationAnalytical analytical;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.EVALUATION_METHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvaluationBySimulation getSimulation() {
		return simulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSimulation(EvaluationBySimulation newSimulation, NotificationChain msgs) {
		EvaluationBySimulation oldSimulation = simulation;
		simulation = newSimulation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION, oldSimulation, newSimulation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimulation(EvaluationBySimulation newSimulation) {
		if (newSimulation != simulation) {
			NotificationChain msgs = null;
			if (simulation != null)
				msgs = ((InternalEObject)simulation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION, null, msgs);
			if (newSimulation != null)
				msgs = ((InternalEObject)newSimulation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION, null, msgs);
			msgs = basicSetSimulation(newSimulation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION, newSimulation, newSimulation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvaluationAnalytical getAnalytical() {
		return analytical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnalytical(EvaluationAnalytical newAnalytical, NotificationChain msgs) {
		EvaluationAnalytical oldAnalytical = analytical;
		analytical = newAnalytical;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL, oldAnalytical, newAnalytical);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAnalytical(EvaluationAnalytical newAnalytical) {
		if (newAnalytical != analytical) {
			NotificationChain msgs = null;
			if (analytical != null)
				msgs = ((InternalEObject)analytical).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL, null, msgs);
			if (newAnalytical != null)
				msgs = ((InternalEObject)newAnalytical).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL, null, msgs);
			msgs = basicSetAnalytical(newAnalytical, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL, newAnalytical, newAnalytical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION:
				return basicSetSimulation(null, msgs);
			case StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL:
				return basicSetAnalytical(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION:
				return getSimulation();
			case StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL:
				return getAnalytical();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION:
				setSimulation((EvaluationBySimulation)newValue);
				return;
			case StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL:
				setAnalytical((EvaluationAnalytical)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION:
				setSimulation((EvaluationBySimulation)null);
				return;
			case StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL:
				setAnalytical((EvaluationAnalytical)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.EVALUATION_METHOD__SIMULATION:
				return simulation != null;
			case StateBasedDataTypesPackage.EVALUATION_METHOD__ANALYTICAL:
				return analytical != null;
		}
		return super.eIsSet(featureID);
	}

} //EvaluationMethodImpl

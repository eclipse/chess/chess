/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getSimulation <em>Simulation</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getAnalytical <em>Analytical</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationMethod()
 * @model
 * @generated
 */
public interface EvaluationMethod extends EObject {

	/**
	 * Returns the value of the '<em><b>Simulation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation</em>' containment reference.
	 * @see #setSimulation(EvaluationBySimulation)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationMethod_Simulation()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	EvaluationBySimulation getSimulation();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getSimulation <em>Simulation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation</em>' containment reference.
	 * @see #getSimulation()
	 * @generated
	 */
	void setSimulation(EvaluationBySimulation value);

	/**
	 * Returns the value of the '<em><b>Analytical</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Analytical</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analytical</em>' containment reference.
	 * @see #setAnalytical(EvaluationAnalytical)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationMethod_Analytical()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	EvaluationAnalytical getAnalytical();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod#getAnalytical <em>Analytical</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analytical</em>' containment reference.
	 * @see #getAnalytical()
	 * @generated
	 */
	void setAnalytical(EvaluationAnalytical value);
} // EvaluationMethod

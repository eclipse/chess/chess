/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FI4FA Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage#getFI4FASpecification()
 * @model
 * @generated
 */
public interface FI4FASpecification extends FPTCSpecification {
} // FI4FASpecification

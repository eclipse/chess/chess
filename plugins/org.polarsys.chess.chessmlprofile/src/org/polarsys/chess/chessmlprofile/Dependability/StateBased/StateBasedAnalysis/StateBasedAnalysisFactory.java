/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.StateBasedAnalysisPackage
 * @generated
 */
public interface StateBasedAnalysisFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StateBasedAnalysisFactory eINSTANCE = org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.impl.StateBasedAnalysisFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>State Based Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State Based Analysis</em>'.
	 * @generated
	 */
	StateBasedAnalysis createStateBasedAnalysis();

	/**
	 * Returns a new object of class '<em>SBA Initial Conditions</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SBA Initial Conditions</em>'.
	 * @generated
	 */
	SBAInitialConditions createSBAInitialConditions();

	/**
	 * Returns a new object of class '<em>SAN Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SAN Analysis</em>'.
	 * @generated
	 */
	SANAnalysis createSANAnalysis();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StateBasedAnalysisPackage getStateBasedAnalysisPackage();

} //StateBasedAnalysisFactory

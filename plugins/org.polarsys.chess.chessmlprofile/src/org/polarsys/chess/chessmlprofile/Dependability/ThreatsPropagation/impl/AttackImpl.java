/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.uml2.uml.Message;
import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.AttackType;
import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.ThreatType;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.Attack;
import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.ThreatsPropagationPackage;
import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.Vulnerability;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getSeverity <em>Severity</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getLikelyhood <em>Likelyhood</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getVulnerability <em>Vulnerability</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getThread <em>Thread</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.AttackImpl#getBase_Message <em>Base Message</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackImpl extends InternalPropagationImpl implements Attack {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final AttackType KIND_EDEFAULT = AttackType.MASQUERADE_ATTACK;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected AttackType kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final String SEVERITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected String severity = SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getLikelyhood() <em>Likelyhood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelyhood()
	 * @generated
	 * @ordered
	 */
	protected static final String LIKELYHOOD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLikelyhood() <em>Likelyhood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelyhood()
	 * @generated
	 * @ordered
	 */
	protected String likelyhood = LIKELYHOOD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVulnerability() <em>Vulnerability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVulnerability()
	 * @generated
	 * @ordered
	 */
	protected Vulnerability vulnerability;

	/**
	 * The default value of the '{@link #getThread() <em>Thread</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThread()
	 * @generated
	 * @ordered
	 */
	protected static final ThreatType THREAD_EDEFAULT = ThreatType.UNAUTHORIZED_ACCESS_OF_SERVICE;

	/**
	 * The cached value of the '{@link #getThread() <em>Thread</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThread()
	 * @generated
	 * @ordered
	 */
	protected ThreatType thread = THREAD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Message() <em>Base Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Message()
	 * @generated
	 * @ordered
	 */
	protected Message base_Message;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ThreatsPropagationPackage.Literals.ATTACK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AttackType getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKind(AttackType newKind) {
		AttackType oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSeverity() {
		return severity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSeverity(String newSeverity) {
		String oldSeverity = severity;
		severity = newSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__SEVERITY, oldSeverity, severity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLikelyhood() {
		return likelyhood;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLikelyhood(String newLikelyhood) {
		String oldLikelyhood = likelyhood;
		likelyhood = newLikelyhood;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__LIKELYHOOD, oldLikelyhood, likelyhood));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Vulnerability getVulnerability() {
		if (vulnerability != null && vulnerability.eIsProxy()) {
			InternalEObject oldVulnerability = (InternalEObject)vulnerability;
			vulnerability = (Vulnerability)eResolveProxy(oldVulnerability);
			if (vulnerability != oldVulnerability) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ThreatsPropagationPackage.ATTACK__VULNERABILITY, oldVulnerability, vulnerability));
			}
		}
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vulnerability basicGetVulnerability() {
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVulnerability(Vulnerability newVulnerability) {
		Vulnerability oldVulnerability = vulnerability;
		vulnerability = newVulnerability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__VULNERABILITY, oldVulnerability, vulnerability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ThreatType getThread() {
		return thread;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setThread(ThreatType newThread) {
		ThreatType oldThread = thread;
		thread = newThread == null ? THREAD_EDEFAULT : newThread;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__THREAD, oldThread, thread));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Message getBase_Message() {
		if (base_Message != null && base_Message.eIsProxy()) {
			InternalEObject oldBase_Message = (InternalEObject)base_Message;
			base_Message = (Message)eResolveProxy(oldBase_Message);
			if (base_Message != oldBase_Message) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ThreatsPropagationPackage.ATTACK__BASE_MESSAGE, oldBase_Message, base_Message));
			}
		}
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetBase_Message() {
		return base_Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Message(Message newBase_Message) {
		Message oldBase_Message = base_Message;
		base_Message = newBase_Message;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPropagationPackage.ATTACK__BASE_MESSAGE, oldBase_Message, base_Message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ThreatsPropagationPackage.ATTACK__KIND:
				return getKind();
			case ThreatsPropagationPackage.ATTACK__SEVERITY:
				return getSeverity();
			case ThreatsPropagationPackage.ATTACK__LIKELYHOOD:
				return getLikelyhood();
			case ThreatsPropagationPackage.ATTACK__VULNERABILITY:
				if (resolve) return getVulnerability();
				return basicGetVulnerability();
			case ThreatsPropagationPackage.ATTACK__THREAD:
				return getThread();
			case ThreatsPropagationPackage.ATTACK__BASE_MESSAGE:
				if (resolve) return getBase_Message();
				return basicGetBase_Message();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ThreatsPropagationPackage.ATTACK__KIND:
				setKind((AttackType)newValue);
				return;
			case ThreatsPropagationPackage.ATTACK__SEVERITY:
				setSeverity((String)newValue);
				return;
			case ThreatsPropagationPackage.ATTACK__LIKELYHOOD:
				setLikelyhood((String)newValue);
				return;
			case ThreatsPropagationPackage.ATTACK__VULNERABILITY:
				setVulnerability((Vulnerability)newValue);
				return;
			case ThreatsPropagationPackage.ATTACK__THREAD:
				setThread((ThreatType)newValue);
				return;
			case ThreatsPropagationPackage.ATTACK__BASE_MESSAGE:
				setBase_Message((Message)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ThreatsPropagationPackage.ATTACK__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case ThreatsPropagationPackage.ATTACK__SEVERITY:
				setSeverity(SEVERITY_EDEFAULT);
				return;
			case ThreatsPropagationPackage.ATTACK__LIKELYHOOD:
				setLikelyhood(LIKELYHOOD_EDEFAULT);
				return;
			case ThreatsPropagationPackage.ATTACK__VULNERABILITY:
				setVulnerability((Vulnerability)null);
				return;
			case ThreatsPropagationPackage.ATTACK__THREAD:
				setThread(THREAD_EDEFAULT);
				return;
			case ThreatsPropagationPackage.ATTACK__BASE_MESSAGE:
				setBase_Message((Message)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ThreatsPropagationPackage.ATTACK__KIND:
				return kind != KIND_EDEFAULT;
			case ThreatsPropagationPackage.ATTACK__SEVERITY:
				return SEVERITY_EDEFAULT == null ? severity != null : !SEVERITY_EDEFAULT.equals(severity);
			case ThreatsPropagationPackage.ATTACK__LIKELYHOOD:
				return LIKELYHOOD_EDEFAULT == null ? likelyhood != null : !LIKELYHOOD_EDEFAULT.equals(likelyhood);
			case ThreatsPropagationPackage.ATTACK__VULNERABILITY:
				return vulnerability != null;
			case ThreatsPropagationPackage.ATTACK__THREAD:
				return thread != THREAD_EDEFAULT;
			case ThreatsPropagationPackage.ATTACK__BASE_MESSAGE:
				return base_Message != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(", severity: ");
		result.append(severity);
		result.append(", likelyhood: ");
		result.append(likelyhood);
		result.append(", thread: ");
		result.append(thread);
		result.append(')');
		return result.toString();
	}

} //AttackImpl

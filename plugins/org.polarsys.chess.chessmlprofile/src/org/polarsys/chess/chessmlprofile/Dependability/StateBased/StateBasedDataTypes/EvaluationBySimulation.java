/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation By Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceInterval <em>Confidence Interval</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceLevel <em>Confidence Level</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getKind <em>Kind</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getMaxBatches <em>Max Batches</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationBySimulation()
 * @model
 * @generated
 */
public interface EvaluationBySimulation extends EvaluationMethod {

	/**
	 * Returns the value of the '<em><b>Confidence Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Interval</em>' attribute.
	 * @see #setConfidenceInterval(String)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationBySimulation_ConfidenceInterval()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getConfidenceInterval();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceInterval <em>Confidence Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Interval</em>' attribute.
	 * @see #getConfidenceInterval()
	 * @generated
	 */
	void setConfidenceInterval(String value);

	/**
	 * Returns the value of the '<em><b>Confidence Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Confidence Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confidence Level</em>' attribute.
	 * @see #setConfidenceLevel(String)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationBySimulation_ConfidenceLevel()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getConfidenceLevel();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getConfidenceLevel <em>Confidence Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confidence Level</em>' attribute.
	 * @see #getConfidenceLevel()
	 * @generated
	 */
	void setConfidenceLevel(String value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind
	 * @see #setKind(ConfidenceKind)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationBySimulation_Kind()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ConfidenceKind getKind();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.ConfidenceKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(ConfidenceKind value);

	/**
	 * Returns the value of the '<em><b>Max Batches</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Batches</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Batches</em>' attribute.
	 * @see #setMaxBatches(int)
	 * @see org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage#getEvaluationBySimulation_MaxBatches()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getMaxBatches();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationBySimulation#getMaxBatches <em>Max Batches</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Batches</em>' attribute.
	 * @see #getMaxBatches()
	 * @generated
	 */
	void setMaxBatches(int value);
} // EvaluationBySimulation

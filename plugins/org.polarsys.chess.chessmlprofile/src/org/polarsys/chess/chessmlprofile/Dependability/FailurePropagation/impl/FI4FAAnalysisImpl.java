/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaAnalysisContextImpl;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FAAnalysis;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FI4FA Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FI4FAAnalysisImpl extends GaAnalysisContextImpl implements FI4FAAnalysis {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FI4FAAnalysisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FailurePropagationPackage.Literals.FI4FA_ANALYSIS;
	}

} //FI4FAAnalysisImpl

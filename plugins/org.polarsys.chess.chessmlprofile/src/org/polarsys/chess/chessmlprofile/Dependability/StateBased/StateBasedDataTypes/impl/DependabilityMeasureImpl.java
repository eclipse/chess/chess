/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.DependabilityMeasure;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.EvaluationMethod;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependability Measure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl#getEvalProperties <em>Eval Properties</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl#getRequiredMin <em>Required Min</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.DependabilityMeasureImpl#getRequiredMax <em>Required Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DependabilityMeasureImpl extends MinimalEObjectImpl.Container implements DependabilityMeasure {
	/**
	 * The cached value of the '{@link #getEvalProperties() <em>Eval Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvalProperties()
	 * @generated
	 * @ordered
	 */
	protected EvaluationMethod evalProperties;
	/**
	 * The default value of the '{@link #getRequiredMin() <em>Required Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredMin()
	 * @generated
	 * @ordered
	 */
	protected static final String REQUIRED_MIN_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getRequiredMin() <em>Required Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredMin()
	 * @generated
	 * @ordered
	 */
	protected String requiredMin = REQUIRED_MIN_EDEFAULT;
	/**
	 * The default value of the '{@link #getRequiredMax() <em>Required Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredMax()
	 * @generated
	 * @ordered
	 */
	protected static final String REQUIRED_MAX_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getRequiredMax() <em>Required Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredMax()
	 * @generated
	 * @ordered
	 */
	protected String requiredMax = REQUIRED_MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DependabilityMeasureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.DEPENDABILITY_MEASURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EvaluationMethod getEvalProperties() {
		return evalProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvalProperties(EvaluationMethod newEvalProperties, NotificationChain msgs) {
		EvaluationMethod oldEvalProperties = evalProperties;
		evalProperties = newEvalProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES, oldEvalProperties, newEvalProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEvalProperties(EvaluationMethod newEvalProperties) {
		if (newEvalProperties != evalProperties) {
			NotificationChain msgs = null;
			if (evalProperties != null)
				msgs = ((InternalEObject)evalProperties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES, null, msgs);
			if (newEvalProperties != null)
				msgs = ((InternalEObject)newEvalProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES, null, msgs);
			msgs = basicSetEvalProperties(newEvalProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES, newEvalProperties, newEvalProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRequiredMin() {
		return requiredMin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRequiredMin(String newRequiredMin) {
		String oldRequiredMin = requiredMin;
		requiredMin = newRequiredMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MIN, oldRequiredMin, requiredMin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRequiredMax() {
		return requiredMax;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRequiredMax(String newRequiredMax) {
		String oldRequiredMax = requiredMax;
		requiredMax = newRequiredMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MAX, oldRequiredMax, requiredMax));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES:
				return basicSetEvalProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES:
				return getEvalProperties();
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MIN:
				return getRequiredMin();
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MAX:
				return getRequiredMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES:
				setEvalProperties((EvaluationMethod)newValue);
				return;
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MIN:
				setRequiredMin((String)newValue);
				return;
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MAX:
				setRequiredMax((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES:
				setEvalProperties((EvaluationMethod)null);
				return;
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MIN:
				setRequiredMin(REQUIRED_MIN_EDEFAULT);
				return;
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MAX:
				setRequiredMax(REQUIRED_MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__EVAL_PROPERTIES:
				return evalProperties != null;
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MIN:
				return REQUIRED_MIN_EDEFAULT == null ? requiredMin != null : !REQUIRED_MIN_EDEFAULT.equals(requiredMin);
			case StateBasedDataTypesPackage.DEPENDABILITY_MEASURE__REQUIRED_MAX:
				return REQUIRED_MAX_EDEFAULT == null ? requiredMax != null : !REQUIRED_MAX_EDEFAULT.equals(requiredMax);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (requiredMin: ");
		result.append(requiredMin);
		result.append(", requiredMax: ");
		result.append(requiredMax);
		result.append(')');
		return result.toString();
	}

} //DependabilityMeasureImpl

/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;
import org.eclipse.papyrus.sysml16.sysml.SysMLPackage;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.profile.standard.StandardPackage;
import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage;
import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationPackageImpl;
import org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage;

import org.polarsys.chess.chessmlprofile.ComponentModel.impl.ComponentModelPackageImpl;

import org.polarsys.chess.chessmlprofile.Core.CHESSViews.CHESSViewsPackage;

import org.polarsys.chess.chessmlprofile.Core.CHESSViews.impl.CHESSViewsPackageImpl;

import org.polarsys.chess.chessmlprofile.Core.CorePackage;

import org.polarsys.chess.chessmlprofile.Core.impl.CorePackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.DependableComponentPackage;

import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.impl.DependableComponentPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.ACIDAvoidable;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.ACIDMitigation;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FAAnalysis;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FASpecification;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FLABehavior;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTCPortSlot;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTCSpecification;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationAnalysis;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationDataTypes.FailurePropagationDataTypesPackage;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationDataTypes.impl.FailurePropagationDataTypesPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationFactory;
import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage;

import org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.MitigationMeansPackage;

import org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.impl.MitigationMeansPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.FaultTolerance.FaultTolerancePackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.FaultTolerance.impl.FaultTolerancePackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.MaintenanceMonitoring.MaintenanceMonitoringPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.MaintenanceMonitoring.impl.MaintenanceMonitoringPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.StateBasedAnalysisPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.impl.StateBasedAnalysisPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedComponents.StateBasedComponentsPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedComponents.impl.StateBasedComponentsPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.ThreatsPropagationPackage;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.ThreatsPropagationPackageImpl;

import org.polarsys.chess.chessmlprofile.ParameterizedArchitecture.ParameterizedArchitecturePackage;

import org.polarsys.chess.chessmlprofile.ParameterizedArchitecture.impl.ParameterizedArchitecturePackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.ARINCComponentModel.ARINCComponentModelPackage;

import org.polarsys.chess.chessmlprofile.Predictability.ARINCComponentModel.impl.ARINCComponentModelPackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.DeploymentConfiguration.HardwareBaseline.HardwareBaselinePackage;

import org.polarsys.chess.chessmlprofile.Predictability.DeploymentConfiguration.HardwareBaseline.impl.HardwareBaselinePackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.RTComponentModel.RTComponentModelPackage;

import org.polarsys.chess.chessmlprofile.Predictability.RTComponentModel.impl.RTComponentModelPackageImpl;
import org.polarsys.chess.chessmlprofile.Safety.SafetyPackage;
import org.polarsys.chess.chessmlprofile.Safety.impl.SafetyPackageImpl;
import org.polarsys.chess.chessmlprofile.StateMachines.StateMachinesPackage;
import org.polarsys.chess.chessmlprofile.StateMachines.impl.StateMachinesPackageImpl;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.STSPackage;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.impl.STSPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FailurePropagationPackageImpl extends EPackageImpl implements FailurePropagationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fptcSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fptcPortSlotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fptcEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flaBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass failurePropagationAnalysisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fi4FAEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fi4FASpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acidAvoidableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass acidMitigationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fi4FAAnalysisEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FailurePropagationPackageImpl() {
		super(eNS_URI, FailurePropagationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FailurePropagationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FailurePropagationPackage init() {
		if (isInited) return (FailurePropagationPackage)EPackage.Registry.INSTANCE.getEPackage(FailurePropagationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFailurePropagationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FailurePropagationPackageImpl theFailurePropagationPackage = registeredFailurePropagationPackage instanceof FailurePropagationPackageImpl ? (FailurePropagationPackageImpl)registeredFailurePropagationPackage : new FailurePropagationPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();
		StandardPackage.eINSTANCE.eClass();
		SysMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		CorePackageImpl theCorePackage = (CorePackageImpl)(registeredPackage instanceof CorePackageImpl ? registeredPackage : CorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CHESSViewsPackage.eNS_URI);
		CHESSViewsPackageImpl theCHESSViewsPackage = (CHESSViewsPackageImpl)(registeredPackage instanceof CHESSViewsPackageImpl ? registeredPackage : CHESSViewsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FailurePropagationDataTypesPackage.eNS_URI);
		FailurePropagationDataTypesPackageImpl theFailurePropagationDataTypesPackage = (FailurePropagationDataTypesPackageImpl)(registeredPackage instanceof FailurePropagationDataTypesPackageImpl ? registeredPackage : FailurePropagationDataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DependableComponentPackage.eNS_URI);
		DependableComponentPackageImpl theDependableComponentPackage = (DependableComponentPackageImpl)(registeredPackage instanceof DependableComponentPackageImpl ? registeredPackage : DependableComponentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ThreatsPropagationPackage.eNS_URI);
		ThreatsPropagationPackageImpl theThreatsPropagationPackage = (ThreatsPropagationPackageImpl)(registeredPackage instanceof ThreatsPropagationPackageImpl ? registeredPackage : ThreatsPropagationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedDataTypesPackage.eNS_URI);
		StateBasedDataTypesPackageImpl theStateBasedDataTypesPackage = (StateBasedDataTypesPackageImpl)(registeredPackage instanceof StateBasedDataTypesPackageImpl ? registeredPackage : StateBasedDataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedComponentsPackage.eNS_URI);
		StateBasedComponentsPackageImpl theStateBasedComponentsPackage = (StateBasedComponentsPackageImpl)(registeredPackage instanceof StateBasedComponentsPackageImpl ? registeredPackage : StateBasedComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FaultTolerancePackage.eNS_URI);
		FaultTolerancePackageImpl theFaultTolerancePackage = (FaultTolerancePackageImpl)(registeredPackage instanceof FaultTolerancePackageImpl ? registeredPackage : FaultTolerancePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MaintenanceMonitoringPackage.eNS_URI);
		MaintenanceMonitoringPackageImpl theMaintenanceMonitoringPackage = (MaintenanceMonitoringPackageImpl)(registeredPackage instanceof MaintenanceMonitoringPackageImpl ? registeredPackage : MaintenanceMonitoringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedAnalysisPackage.eNS_URI);
		StateBasedAnalysisPackageImpl theStateBasedAnalysisPackage = (StateBasedAnalysisPackageImpl)(registeredPackage instanceof StateBasedAnalysisPackageImpl ? registeredPackage : StateBasedAnalysisPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MitigationMeansPackage.eNS_URI);
		MitigationMeansPackageImpl theMitigationMeansPackage = (MitigationMeansPackageImpl)(registeredPackage instanceof MitigationMeansPackageImpl ? registeredPackage : MitigationMeansPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParameterizedArchitecturePackage.eNS_URI);
		ParameterizedArchitecturePackageImpl theParameterizedArchitecturePackage = (ParameterizedArchitecturePackageImpl)(registeredPackage instanceof ParameterizedArchitecturePackageImpl ? registeredPackage : ParameterizedArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HardwareBaselinePackage.eNS_URI);
		HardwareBaselinePackageImpl theHardwareBaselinePackage = (HardwareBaselinePackageImpl)(registeredPackage instanceof HardwareBaselinePackageImpl ? registeredPackage : HardwareBaselinePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RTComponentModelPackage.eNS_URI);
		RTComponentModelPackageImpl theRTComponentModelPackage = (RTComponentModelPackageImpl)(registeredPackage instanceof RTComponentModelPackageImpl ? registeredPackage : RTComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ARINCComponentModelPackage.eNS_URI);
		ARINCComponentModelPackageImpl theARINCComponentModelPackage = (ARINCComponentModelPackageImpl)(registeredPackage instanceof ARINCComponentModelPackageImpl ? registeredPackage : ARINCComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentModelPackage.eNS_URI);
		ComponentModelPackageImpl theComponentModelPackage = (ComponentModelPackageImpl)(registeredPackage instanceof ComponentModelPackageImpl ? registeredPackage : ComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(STSPackage.eNS_URI);
		STSPackageImpl theSTSPackage = (STSPackageImpl)(registeredPackage instanceof STSPackageImpl ? registeredPackage : STSPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SafetyPackage.eNS_URI);
		SafetyPackageImpl theSafetyPackage = (SafetyPackageImpl)(registeredPackage instanceof SafetyPackageImpl ? registeredPackage : SafetyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AsyncCommunicationPackage.eNS_URI);
		AsyncCommunicationPackageImpl theAsyncCommunicationPackage = (AsyncCommunicationPackageImpl)(registeredPackage instanceof AsyncCommunicationPackageImpl ? registeredPackage : AsyncCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateMachinesPackage.eNS_URI);
		StateMachinesPackageImpl theStateMachinesPackage = (StateMachinesPackageImpl)(registeredPackage instanceof StateMachinesPackageImpl ? registeredPackage : StateMachinesPackage.eINSTANCE);

		// Create package meta-data objects
		theFailurePropagationPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCHESSViewsPackage.createPackageContents();
		theFailurePropagationDataTypesPackage.createPackageContents();
		theDependableComponentPackage.createPackageContents();
		theThreatsPropagationPackage.createPackageContents();
		theStateBasedDataTypesPackage.createPackageContents();
		theStateBasedComponentsPackage.createPackageContents();
		theFaultTolerancePackage.createPackageContents();
		theMaintenanceMonitoringPackage.createPackageContents();
		theStateBasedAnalysisPackage.createPackageContents();
		theMitigationMeansPackage.createPackageContents();
		theParameterizedArchitecturePackage.createPackageContents();
		theHardwareBaselinePackage.createPackageContents();
		theRTComponentModelPackage.createPackageContents();
		theARINCComponentModelPackage.createPackageContents();
		theComponentModelPackage.createPackageContents();
		theSTSPackage.createPackageContents();
		theSafetyPackage.createPackageContents();
		theAsyncCommunicationPackage.createPackageContents();
		theStateMachinesPackage.createPackageContents();

		// Initialize created meta-data
		theFailurePropagationPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCHESSViewsPackage.initializePackageContents();
		theFailurePropagationDataTypesPackage.initializePackageContents();
		theDependableComponentPackage.initializePackageContents();
		theThreatsPropagationPackage.initializePackageContents();
		theStateBasedDataTypesPackage.initializePackageContents();
		theStateBasedComponentsPackage.initializePackageContents();
		theFaultTolerancePackage.initializePackageContents();
		theMaintenanceMonitoringPackage.initializePackageContents();
		theStateBasedAnalysisPackage.initializePackageContents();
		theMitigationMeansPackage.initializePackageContents();
		theParameterizedArchitecturePackage.initializePackageContents();
		theHardwareBaselinePackage.initializePackageContents();
		theRTComponentModelPackage.initializePackageContents();
		theARINCComponentModelPackage.initializePackageContents();
		theComponentModelPackage.initializePackageContents();
		theSTSPackage.initializePackageContents();
		theSafetyPackage.initializePackageContents();
		theAsyncCommunicationPackage.initializePackageContents();
		theStateMachinesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFailurePropagationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FailurePropagationPackage.eNS_URI, theFailurePropagationPackage);
		return theFailurePropagationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFPTCSpecification() {
		return fptcSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTCSpecification_Base_Comment() {
		return (EReference)fptcSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFPTCSpecification_Failure() {
		return (EAttribute)fptcSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTCSpecification_PartWithPort() {
		return (EReference)fptcSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTCSpecification_FailureMode() {
		return (EReference)fptcSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFPTCPortSlot() {
		return fptcPortSlotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTCPortSlot_FPTCSpecification() {
		return (EReference)fptcPortSlotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTCPortSlot_Base_Slot() {
		return (EReference)fptcPortSlotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFPTCPortSlot_Failure() {
		return (EAttribute)fptcPortSlotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFPTC() {
		return fptcEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFPTC_Fptc() {
		return (EAttribute)fptcEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTC_Base_Comment() {
		return (EReference)fptcEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFPTC_Base_Connector() {
		return (EReference)fptcEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLABehavior() {
		return flaBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLABehavior_Base_Constraint() {
		return (EReference)flaBehaviorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLABehavior_Base_Class() {
		return (EReference)flaBehaviorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLABehavior_Fptc() {
		return (EAttribute)flaBehaviorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLABehavior_Base_InstanceSpecification() {
		return (EReference)flaBehaviorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLABehavior_Base_Property() {
		return (EReference)flaBehaviorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLABehavior_Base_Connector() {
		return (EReference)flaBehaviorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFailurePropagationAnalysis() {
		return failurePropagationAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFI4FA() {
		return fi4FAEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFI4FA_Fi4fa() {
		return (EAttribute)fi4FAEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFI4FA_Base_Connector() {
		return (EReference)fi4FAEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFI4FASpecification() {
		return fi4FASpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getACIDAvoidable() {
		return acidAvoidableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDAvoidable_A() {
		return (EAttribute)acidAvoidableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDAvoidable_C() {
		return (EAttribute)acidAvoidableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACIDAvoidable_Base_Comment() {
		return (EReference)acidAvoidableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDAvoidable_I() {
		return (EAttribute)acidAvoidableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDAvoidable_D() {
		return (EAttribute)acidAvoidableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getACIDMitigation() {
		return acidMitigationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACIDMitigation_Base_Comment() {
		return (EReference)acidMitigationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDMitigation_A() {
		return (EAttribute)acidMitigationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDMitigation_C() {
		return (EAttribute)acidMitigationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDMitigation_I() {
		return (EAttribute)acidMitigationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACIDMitigation_D() {
		return (EAttribute)acidMitigationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFI4FAAnalysis() {
		return fi4FAAnalysisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FailurePropagationFactory getFailurePropagationFactory() {
		return (FailurePropagationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fptcSpecificationEClass = createEClass(FPTC_SPECIFICATION);
		createEReference(fptcSpecificationEClass, FPTC_SPECIFICATION__BASE_COMMENT);
		createEAttribute(fptcSpecificationEClass, FPTC_SPECIFICATION__FAILURE);
		createEReference(fptcSpecificationEClass, FPTC_SPECIFICATION__PART_WITH_PORT);
		createEReference(fptcSpecificationEClass, FPTC_SPECIFICATION__FAILURE_MODE);

		fptcPortSlotEClass = createEClass(FPTC_PORT_SLOT);
		createEReference(fptcPortSlotEClass, FPTC_PORT_SLOT__FPTC_SPECIFICATION);
		createEReference(fptcPortSlotEClass, FPTC_PORT_SLOT__BASE_SLOT);
		createEAttribute(fptcPortSlotEClass, FPTC_PORT_SLOT__FAILURE);

		fptcEClass = createEClass(FPTC);
		createEAttribute(fptcEClass, FPTC__FPTC);
		createEReference(fptcEClass, FPTC__BASE_COMMENT);
		createEReference(fptcEClass, FPTC__BASE_CONNECTOR);

		flaBehaviorEClass = createEClass(FLA_BEHAVIOR);
		createEReference(flaBehaviorEClass, FLA_BEHAVIOR__BASE_CONSTRAINT);
		createEReference(flaBehaviorEClass, FLA_BEHAVIOR__BASE_CLASS);
		createEAttribute(flaBehaviorEClass, FLA_BEHAVIOR__FPTC);
		createEReference(flaBehaviorEClass, FLA_BEHAVIOR__BASE_INSTANCE_SPECIFICATION);
		createEReference(flaBehaviorEClass, FLA_BEHAVIOR__BASE_PROPERTY);
		createEReference(flaBehaviorEClass, FLA_BEHAVIOR__BASE_CONNECTOR);

		failurePropagationAnalysisEClass = createEClass(FAILURE_PROPAGATION_ANALYSIS);

		fi4FAEClass = createEClass(FI4FA);
		createEAttribute(fi4FAEClass, FI4FA__FI4FA);
		createEReference(fi4FAEClass, FI4FA__BASE_CONNECTOR);

		fi4FASpecificationEClass = createEClass(FI4FA_SPECIFICATION);

		acidAvoidableEClass = createEClass(ACID_AVOIDABLE);
		createEAttribute(acidAvoidableEClass, ACID_AVOIDABLE__A);
		createEAttribute(acidAvoidableEClass, ACID_AVOIDABLE__C);
		createEReference(acidAvoidableEClass, ACID_AVOIDABLE__BASE_COMMENT);
		createEAttribute(acidAvoidableEClass, ACID_AVOIDABLE__I);
		createEAttribute(acidAvoidableEClass, ACID_AVOIDABLE__D);

		acidMitigationEClass = createEClass(ACID_MITIGATION);
		createEReference(acidMitigationEClass, ACID_MITIGATION__BASE_COMMENT);
		createEAttribute(acidMitigationEClass, ACID_MITIGATION__A);
		createEAttribute(acidMitigationEClass, ACID_MITIGATION__C);
		createEAttribute(acidMitigationEClass, ACID_MITIGATION__I);
		createEAttribute(acidMitigationEClass, ACID_MITIGATION__D);

		fi4FAAnalysisEClass = createEClass(FI4FA_ANALYSIS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FailurePropagationDataTypesPackage theFailurePropagationDataTypesPackage = (FailurePropagationDataTypesPackage)EPackage.Registry.INSTANCE.getEPackage(FailurePropagationDataTypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		DependableComponentPackage theDependableComponentPackage = (DependableComponentPackage)EPackage.Registry.INSTANCE.getEPackage(DependableComponentPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		GQAMPackage theGQAMPackage = (GQAMPackage)EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theFailurePropagationDataTypesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fptcEClass.getESuperTypes().add(theDependableComponentPackage.getDependableComponent());
		failurePropagationAnalysisEClass.getESuperTypes().add(theGQAMPackage.getGaAnalysisContext());
		fi4FAEClass.getESuperTypes().add(theDependableComponentPackage.getDependableComponent());
		fi4FASpecificationEClass.getESuperTypes().add(this.getFPTCSpecification());
		fi4FAAnalysisEClass.getESuperTypes().add(theGQAMPackage.getGaAnalysisContext());

		// Initialize classes, features, and operations; add parameters
		initEClass(fptcSpecificationEClass, FPTCSpecification.class, "FPTCSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFPTCSpecification_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, FPTCSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFPTCSpecification_Failure(), theFailurePropagationDataTypesPackage.getFailureType(), "failure", null, 0, -1, FPTCSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFPTCSpecification_PartWithPort(), theUMLPackage.getProperty(), null, "partWithPort", null, 1, 1, FPTCSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFPTCSpecification_FailureMode(), theDependableComponentPackage.getFailureMode(), null, "failureMode", null, 0, -1, FPTCSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(fptcPortSlotEClass, FPTCPortSlot.class, "FPTCPortSlot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFPTCPortSlot_FPTCSpecification(), this.getFPTCSpecification(), null, "FPTCSpecification", null, 1, 1, FPTCPortSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFPTCPortSlot_Base_Slot(), theUMLPackage.getSlot(), null, "base_Slot", null, 1, 1, FPTCPortSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFPTCPortSlot_Failure(), theFailurePropagationDataTypesPackage.getFailureType(), "failure", null, 0, -1, FPTCPortSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(fptcEClass, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTC.class, "FPTC", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFPTC_Fptc(), theTypesPackage.getString(), "fptc", null, 1, 1, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFPTC_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFPTC_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 1, 1, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FPTC.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(flaBehaviorEClass, FLABehavior.class, "FLABehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFLABehavior_Base_Constraint(), theUMLPackage.getConstraint(), null, "base_Constraint", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFLABehavior_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFLABehavior_Fptc(), theTypesPackage.getString(), "fptc", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFLABehavior_Base_InstanceSpecification(), theUMLPackage.getInstanceSpecification(), null, "base_InstanceSpecification", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFLABehavior_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFLABehavior_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 1, 1, FLABehavior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(failurePropagationAnalysisEClass, FailurePropagationAnalysis.class, "FailurePropagationAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fi4FAEClass, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FA.class, "FI4FA", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFI4FA_Fi4fa(), theTypesPackage.getString(), "fi4fa", null, 1, 1, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getFI4FA_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 1, 1, org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FI4FA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(fi4FASpecificationEClass, FI4FASpecification.class, "FI4FASpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(acidAvoidableEClass, ACIDAvoidable.class, "ACIDAvoidable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getACIDAvoidable_A(), theFailurePropagationDataTypesPackage.getA_avoidable(), "a", null, 0, 1, ACIDAvoidable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDAvoidable_C(), theFailurePropagationDataTypesPackage.getC_avoidable(), "c", null, 0, 1, ACIDAvoidable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getACIDAvoidable_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, ACIDAvoidable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDAvoidable_I(), theFailurePropagationDataTypesPackage.getI_avoidable(), "i", null, 0, 1, ACIDAvoidable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDAvoidable_D(), theFailurePropagationDataTypesPackage.getD_avoidable(), "d", null, 0, 1, ACIDAvoidable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(acidMitigationEClass, ACIDMitigation.class, "ACIDMitigation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getACIDMitigation_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, ACIDMitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDMitigation_A(), theFailurePropagationDataTypesPackage.getA_mitigation(), "a", null, 0, 1, ACIDMitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDMitigation_C(), theFailurePropagationDataTypesPackage.getC_mitigation(), "c", null, 0, 1, ACIDMitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDMitigation_I(), theFailurePropagationDataTypesPackage.getI_mitigation(), "i", null, 0, 1, ACIDMitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getACIDMitigation_D(), theFailurePropagationDataTypesPackage.getD_mitigation(), "d", null, 0, 1, ACIDMitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(fi4FAAnalysisEClass, FI4FAAnalysis.class, "FI4FAAnalysis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (acidMitigationEClass,
		   source,
		   new String[] {
			   "originalName", "ACIDMitigation "
		   });
	}

} //FailurePropagationPackageImpl

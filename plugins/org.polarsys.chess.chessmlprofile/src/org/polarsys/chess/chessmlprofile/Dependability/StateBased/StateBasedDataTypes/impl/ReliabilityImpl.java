/**
 */
package org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.Reliability;
import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reliability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.ReliabilityImpl#getInstantOfTime <em>Instant Of Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReliabilityImpl extends DependabilityMeasureImpl implements Reliability {
	/**
	 * The default value of the '{@link #getInstantOfTime() <em>Instant Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantOfTime()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANT_OF_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getInstantOfTime() <em>Instant Of Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantOfTime()
	 * @generated
	 * @ordered
	 */
	protected String instantOfTime = INSTANT_OF_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReliabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StateBasedDataTypesPackage.Literals.RELIABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInstantOfTime() {
		return instantOfTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstantOfTime(String newInstantOfTime) {
		String oldInstantOfTime = instantOfTime;
		instantOfTime = newInstantOfTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StateBasedDataTypesPackage.RELIABILITY__INSTANT_OF_TIME, oldInstantOfTime, instantOfTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StateBasedDataTypesPackage.RELIABILITY__INSTANT_OF_TIME:
				return getInstantOfTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StateBasedDataTypesPackage.RELIABILITY__INSTANT_OF_TIME:
				setInstantOfTime((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.RELIABILITY__INSTANT_OF_TIME:
				setInstantOfTime(INSTANT_OF_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StateBasedDataTypesPackage.RELIABILITY__INSTANT_OF_TIME:
				return INSTANT_OF_TIME_EDEFAULT == null ? instantOfTime != null : !INSTANT_OF_TIME_EDEFAULT.equals(instantOfTime);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (instantOfTime: ");
		result.append(instantOfTime);
		result.append(')');
		return result.toString();
	}

} //ReliabilityImpl

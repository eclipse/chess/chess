/**
 */
package org.polarsys.chess.chessmlprofile.Safety.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.polarsys.chess.chessmlprofile.Safety.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SafetyFactoryImpl extends EFactoryImpl implements SafetyFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SafetyFactory init() {
		try {
			SafetyFactory theSafetyFactory = (SafetyFactory)EPackage.Registry.INSTANCE.getEFactory(SafetyPackage.eNS_URI);
			if (theSafetyFactory != null) {
				return theSafetyFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SafetyFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SafetyPackage.CRITICALITY_INTERFACE_SPECIFICATION: return createCriticalityInterfaceSpecification();
			case SafetyPackage.CRITICALITY_LEVEL: return createCriticalityLevel();
			case SafetyPackage.CRITICALITY_SPECIFICATION: return createCriticalitySpecification();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CriticalityInterfaceSpecification createCriticalityInterfaceSpecification() {
		CriticalityInterfaceSpecificationImpl criticalityInterfaceSpecification = new CriticalityInterfaceSpecificationImpl();
		return criticalityInterfaceSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CriticalityLevel createCriticalityLevel() {
		CriticalityLevelImpl criticalityLevel = new CriticalityLevelImpl();
		return criticalityLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CriticalitySpecification createCriticalitySpecification() {
		CriticalitySpecificationImpl criticalitySpecification = new CriticalitySpecificationImpl();
		return criticalitySpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SafetyPackage getSafetyPackage() {
		return (SafetyPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SafetyPackage getPackage() {
		return SafetyPackage.eINSTANCE;
	}

} //SafetyFactoryImpl

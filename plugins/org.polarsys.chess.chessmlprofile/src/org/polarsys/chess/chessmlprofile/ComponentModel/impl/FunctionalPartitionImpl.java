/**
 */
package org.polarsys.chess.chessmlprofile.ComponentModel.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.uml2.uml.Component;

import org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage;
import org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functional Partition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.impl.FunctionalPartitionImpl#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.impl.FunctionalPartitionImpl#getBase_Component <em>Base Component</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.impl.FunctionalPartitionImpl#getMAF <em>MAF</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.impl.FunctionalPartitionImpl#getMIF <em>MIF</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.impl.FunctionalPartitionImpl#getSchedulingTable <em>Scheduling Table</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionalPartitionImpl extends MinimalEObjectImpl.Container implements FunctionalPartition {
	/**
	 * The default value of the '{@link #getUtilization() <em>Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUtilization()
	 * @generated
	 * @ordered
	 */
	protected static final double UTILIZATION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getUtilization() <em>Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUtilization()
	 * @generated
	 * @ordered
	 */
	protected double utilization = UTILIZATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Component() <em>Base Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Component()
	 * @generated
	 * @ordered
	 */
	protected Component base_Component;

	/**
	 * The default value of the '{@link #getMAF() <em>MAF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAF()
	 * @generated
	 * @ordered
	 */
	protected static final int MAF_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMAF() <em>MAF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAF()
	 * @generated
	 * @ordered
	 */
	protected int maf = MAF_EDEFAULT;

	/**
	 * The default value of the '{@link #getMIF() <em>MIF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMIF()
	 * @generated
	 * @ordered
	 */
	protected static final int MIF_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMIF() <em>MIF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMIF()
	 * @generated
	 * @ordered
	 */
	protected int mif = MIF_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchedulingTable() <em>Scheduling Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingTable()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEDULING_TABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSchedulingTable() <em>Scheduling Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingTable()
	 * @generated
	 * @ordered
	 */
	protected String schedulingTable = SCHEDULING_TABLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalPartitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentModelPackage.Literals.FUNCTIONAL_PARTITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getUtilization() {
		return utilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUtilization(double newUtilization) {
		double oldUtilization = utilization;
		utilization = newUtilization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentModelPackage.FUNCTIONAL_PARTITION__UTILIZATION, oldUtilization, utilization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Component getBase_Component() {
		if (base_Component != null && base_Component.eIsProxy()) {
			InternalEObject oldBase_Component = (InternalEObject)base_Component;
			base_Component = (Component)eResolveProxy(oldBase_Component);
			if (base_Component != oldBase_Component) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT, oldBase_Component, base_Component));
			}
		}
		return base_Component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetBase_Component() {
		return base_Component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Component(Component newBase_Component) {
		Component oldBase_Component = base_Component;
		base_Component = newBase_Component;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT, oldBase_Component, base_Component));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMAF() {
		return maf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMAF(int newMAF) {
		int oldMAF = maf;
		maf = newMAF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentModelPackage.FUNCTIONAL_PARTITION__MAF, oldMAF, maf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMIF() {
		return mif;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMIF(int newMIF) {
		int oldMIF = mif;
		mif = newMIF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentModelPackage.FUNCTIONAL_PARTITION__MIF, oldMIF, mif));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSchedulingTable() {
		return schedulingTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSchedulingTable(String newSchedulingTable) {
		String oldSchedulingTable = schedulingTable;
		schedulingTable = newSchedulingTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentModelPackage.FUNCTIONAL_PARTITION__SCHEDULING_TABLE, oldSchedulingTable, schedulingTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComponentModelPackage.FUNCTIONAL_PARTITION__UTILIZATION:
				return getUtilization();
			case ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT:
				if (resolve) return getBase_Component();
				return basicGetBase_Component();
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MAF:
				return getMAF();
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MIF:
				return getMIF();
			case ComponentModelPackage.FUNCTIONAL_PARTITION__SCHEDULING_TABLE:
				return getSchedulingTable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComponentModelPackage.FUNCTIONAL_PARTITION__UTILIZATION:
				setUtilization((Double)newValue);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT:
				setBase_Component((Component)newValue);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MAF:
				setMAF((Integer)newValue);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MIF:
				setMIF((Integer)newValue);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__SCHEDULING_TABLE:
				setSchedulingTable((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComponentModelPackage.FUNCTIONAL_PARTITION__UTILIZATION:
				setUtilization(UTILIZATION_EDEFAULT);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT:
				setBase_Component((Component)null);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MAF:
				setMAF(MAF_EDEFAULT);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MIF:
				setMIF(MIF_EDEFAULT);
				return;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__SCHEDULING_TABLE:
				setSchedulingTable(SCHEDULING_TABLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComponentModelPackage.FUNCTIONAL_PARTITION__UTILIZATION:
				return utilization != UTILIZATION_EDEFAULT;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__BASE_COMPONENT:
				return base_Component != null;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MAF:
				return maf != MAF_EDEFAULT;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__MIF:
				return mif != MIF_EDEFAULT;
			case ComponentModelPackage.FUNCTIONAL_PARTITION__SCHEDULING_TABLE:
				return SCHEDULING_TABLE_EDEFAULT == null ? schedulingTable != null : !SCHEDULING_TABLE_EDEFAULT.equals(schedulingTable);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (utilization: ");
		result.append(utilization);
		result.append(", MAF: ");
		result.append(maf);
		result.append(", MIF: ");
		result.append(mif);
		result.append(", SchedulingTable: ");
		result.append(schedulingTable);
		result.append(')');
		return result.toString();
	}

} //FunctionalPartitionImpl

/**
 */
package org.polarsys.chess.chessmlprofile.ComponentModel;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Component;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Functional Partition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Avioncs IMA concept
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getBase_Component <em>Base Component</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getMAF <em>MAF</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getMIF <em>MIF</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getSchedulingTable <em>Scheduling Table</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition()
 * @model
 * @generated
 */
public interface FunctionalPartition extends EObject {
	/**
	 * Returns the value of the '<em><b>Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Utilization</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Utilization</em>' attribute.
	 * @see #setUtilization(double)
	 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition_Utilization()
	 * @model dataType="org.eclipse.uml2.types.Real" ordered="false"
	 * @generated
	 */
	double getUtilization();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getUtilization <em>Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Utilization</em>' attribute.
	 * @see #getUtilization()
	 * @generated
	 */
	void setUtilization(double value);

	/**
	 * Returns the value of the '<em><b>Base Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Component</em>' reference.
	 * @see #setBase_Component(Component)
	 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition_Base_Component()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Component getBase_Component();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getBase_Component <em>Base Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Component</em>' reference.
	 * @see #getBase_Component()
	 * @generated
	 */
	void setBase_Component(Component value);

	/**
	 * Returns the value of the '<em><b>MAF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MAF</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MAF</em>' attribute.
	 * @see #setMAF(int)
	 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition_MAF()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getMAF();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getMAF <em>MAF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MAF</em>' attribute.
	 * @see #getMAF()
	 * @generated
	 */
	void setMAF(int value);

	/**
	 * Returns the value of the '<em><b>MIF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MIF</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MIF</em>' attribute.
	 * @see #setMIF(int)
	 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition_MIF()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getMIF();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getMIF <em>MIF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MIF</em>' attribute.
	 * @see #getMIF()
	 * @generated
	 */
	void setMIF(int value);

	/**
	 * Returns the value of the '<em><b>Scheduling Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling Table</em>' attribute.
	 * @see #setSchedulingTable(String)
	 * @see org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage#getFunctionalPartition_SchedulingTable()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getSchedulingTable();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.ComponentModel.FunctionalPartition#getSchedulingTable <em>Scheduling Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduling Table</em>' attribute.
	 * @see #getSchedulingTable()
	 * @generated
	 */
	void setSchedulingTable(String value);

} // FunctionalPartition

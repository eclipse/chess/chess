/**
 */
package org.polarsys.chess.chessmlprofile.Core;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Slot;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identif Slot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Core.IdentifSlot#getId <em>Id</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.Core.IdentifSlot#getBase_slot <em>Base slot</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.chessmlprofile.Core.CorePackage#getIdentifSlot()
 * @model
 * @generated
 */
public interface IdentifSlot extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see org.polarsys.chess.chessmlprofile.Core.CorePackage#getIdentifSlot_Id()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Core.IdentifSlot#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Base slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base slot</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base slot</em>' reference.
	 * @see #setBase_slot(Slot)
	 * @see org.polarsys.chess.chessmlprofile.Core.CorePackage#getIdentifSlot_Base_slot()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Slot getBase_slot();

	/**
	 * Sets the value of the '{@link org.polarsys.chess.chessmlprofile.Core.IdentifSlot#getBase_slot <em>Base slot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base slot</em>' reference.
	 * @see #getBase_slot()
	 * @generated
	 */
	void setBase_slot(Slot value);

} // IdentifSlot

/**
 */
package org.polarsys.chess.chessmlprofile.Core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dummy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.Core.CorePackage#getDummy()
 * @model
 * @generated
 */
public interface Dummy extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void dummyOperation();

} // Dummy

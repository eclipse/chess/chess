/**
 */
package org.polarsys.chess.chessmlprofile.SystemModel.STS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Pressure</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.chessmlprofile.SystemModel.STS.STSPackage#getTimePressure()
 * @model
 * @generated
 */
public interface TimePressure extends OrganizationUnit {
} // TimePressure

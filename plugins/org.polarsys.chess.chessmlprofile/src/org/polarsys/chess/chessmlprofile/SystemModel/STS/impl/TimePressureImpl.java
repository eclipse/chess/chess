/**
 */
package org.polarsys.chess.chessmlprofile.SystemModel.STS.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.chess.chessmlprofile.SystemModel.STS.STSPackage;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.TimePressure;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Pressure</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimePressureImpl extends OrganizationUnitImpl implements TimePressure {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimePressureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return STSPackage.Literals.TIME_PRESSURE;
	}

} //TimePressureImpl

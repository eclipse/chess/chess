/**
 */
package org.polarsys.chess.chessmlprofile.SystemModel.STS.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;
import org.eclipse.papyrus.sysml16.sysml.SysMLPackage;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.profile.standard.StandardPackage;
import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage;
import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationPackageImpl;
import org.polarsys.chess.chessmlprofile.ComponentModel.ComponentModelPackage;

import org.polarsys.chess.chessmlprofile.ComponentModel.impl.ComponentModelPackageImpl;

import org.polarsys.chess.chessmlprofile.Core.CHESSViews.CHESSViewsPackage;

import org.polarsys.chess.chessmlprofile.Core.CHESSViews.impl.CHESSViewsPackageImpl;

import org.polarsys.chess.chessmlprofile.Core.CorePackage;

import org.polarsys.chess.chessmlprofile.Core.impl.CorePackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.DependableComponentPackage;

import org.polarsys.chess.chessmlprofile.Dependability.DependableComponent.impl.DependableComponentPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationDataTypes.FailurePropagationDataTypesPackage;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationDataTypes.impl.FailurePropagationDataTypesPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.FailurePropagationPackage;

import org.polarsys.chess.chessmlprofile.Dependability.FailurePropagation.impl.FailurePropagationPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.MitigationMeansPackage;

import org.polarsys.chess.chessmlprofile.Dependability.MitigationMeans.impl.MitigationMeansPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.FaultTolerance.FaultTolerancePackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.FaultTolerance.impl.FaultTolerancePackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.MaintenanceMonitoring.MaintenanceMonitoringPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.MaintenanceMonitoring.impl.MaintenanceMonitoringPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.StateBasedAnalysisPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedAnalysis.impl.StateBasedAnalysisPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedComponents.StateBasedComponentsPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedComponents.impl.StateBasedComponentsPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.StateBasedDataTypesPackage;

import org.polarsys.chess.chessmlprofile.Dependability.StateBased.StateBasedDataTypes.impl.StateBasedDataTypesPackageImpl;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.ThreatsPropagationPackage;

import org.polarsys.chess.chessmlprofile.Dependability.ThreatsPropagation.impl.ThreatsPropagationPackageImpl;

import org.polarsys.chess.chessmlprofile.ParameterizedArchitecture.ParameterizedArchitecturePackage;

import org.polarsys.chess.chessmlprofile.ParameterizedArchitecture.impl.ParameterizedArchitecturePackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.ARINCComponentModel.ARINCComponentModelPackage;

import org.polarsys.chess.chessmlprofile.Predictability.ARINCComponentModel.impl.ARINCComponentModelPackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.DeploymentConfiguration.HardwareBaseline.HardwareBaselinePackage;

import org.polarsys.chess.chessmlprofile.Predictability.DeploymentConfiguration.HardwareBaseline.impl.HardwareBaselinePackageImpl;

import org.polarsys.chess.chessmlprofile.Predictability.RTComponentModel.RTComponentModelPackage;

import org.polarsys.chess.chessmlprofile.Predictability.RTComponentModel.impl.RTComponentModelPackageImpl;
import org.polarsys.chess.chessmlprofile.Safety.SafetyPackage;
import org.polarsys.chess.chessmlprofile.Safety.impl.SafetyPackageImpl;
import org.polarsys.chess.chessmlprofile.StateMachines.StateMachinesPackage;
import org.polarsys.chess.chessmlprofile.StateMachines.impl.StateMachinesPackageImpl;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HACommunication;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HAFeedback;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HAIntent;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HAKnowledgeDecision;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HAResponse;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HASelection;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HATimeManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HSAttention;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HSKnowledgePerception;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HSPerception;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HSSensory;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.Human;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HumanActuatorUnit;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.HumanSensorUnit;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OUClimateManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OUMissionManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OUOversightManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OUProcessManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OUResourceManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OURulesRegulationManagement;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.Organization;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.OrganizationUnit;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.STSFactory;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.STSPackage;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.Technological;
import org.polarsys.chess.chessmlprofile.SystemModel.STS.TimePressure;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class STSPackageImpl extends EPackageImpl implements STSPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass humanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass technologicalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timePressureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouMissionManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouRulesRegulationManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouClimateManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouOversightManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouProcessManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ouResourceManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hsAttentionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass humanSensorUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hsPerceptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hsKnowledgePerceptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hsSensoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haFeedbackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass humanActuatorUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haIntentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haCommunicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haTimeManagementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass haKnowledgeDecisionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.polarsys.chess.chessmlprofile.SystemModel.STS.STSPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private STSPackageImpl() {
		super(eNS_URI, STSFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link STSPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static STSPackage init() {
		if (isInited) return (STSPackage)EPackage.Registry.INSTANCE.getEPackage(STSPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSTSPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		STSPackageImpl theSTSPackage = registeredSTSPackage instanceof STSPackageImpl ? (STSPackageImpl)registeredSTSPackage : new STSPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();
		StandardPackage.eINSTANCE.eClass();
		SysMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		CorePackageImpl theCorePackage = (CorePackageImpl)(registeredPackage instanceof CorePackageImpl ? registeredPackage : CorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CHESSViewsPackage.eNS_URI);
		CHESSViewsPackageImpl theCHESSViewsPackage = (CHESSViewsPackageImpl)(registeredPackage instanceof CHESSViewsPackageImpl ? registeredPackage : CHESSViewsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FailurePropagationPackage.eNS_URI);
		FailurePropagationPackageImpl theFailurePropagationPackage = (FailurePropagationPackageImpl)(registeredPackage instanceof FailurePropagationPackageImpl ? registeredPackage : FailurePropagationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FailurePropagationDataTypesPackage.eNS_URI);
		FailurePropagationDataTypesPackageImpl theFailurePropagationDataTypesPackage = (FailurePropagationDataTypesPackageImpl)(registeredPackage instanceof FailurePropagationDataTypesPackageImpl ? registeredPackage : FailurePropagationDataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DependableComponentPackage.eNS_URI);
		DependableComponentPackageImpl theDependableComponentPackage = (DependableComponentPackageImpl)(registeredPackage instanceof DependableComponentPackageImpl ? registeredPackage : DependableComponentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ThreatsPropagationPackage.eNS_URI);
		ThreatsPropagationPackageImpl theThreatsPropagationPackage = (ThreatsPropagationPackageImpl)(registeredPackage instanceof ThreatsPropagationPackageImpl ? registeredPackage : ThreatsPropagationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedDataTypesPackage.eNS_URI);
		StateBasedDataTypesPackageImpl theStateBasedDataTypesPackage = (StateBasedDataTypesPackageImpl)(registeredPackage instanceof StateBasedDataTypesPackageImpl ? registeredPackage : StateBasedDataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedComponentsPackage.eNS_URI);
		StateBasedComponentsPackageImpl theStateBasedComponentsPackage = (StateBasedComponentsPackageImpl)(registeredPackage instanceof StateBasedComponentsPackageImpl ? registeredPackage : StateBasedComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FaultTolerancePackage.eNS_URI);
		FaultTolerancePackageImpl theFaultTolerancePackage = (FaultTolerancePackageImpl)(registeredPackage instanceof FaultTolerancePackageImpl ? registeredPackage : FaultTolerancePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MaintenanceMonitoringPackage.eNS_URI);
		MaintenanceMonitoringPackageImpl theMaintenanceMonitoringPackage = (MaintenanceMonitoringPackageImpl)(registeredPackage instanceof MaintenanceMonitoringPackageImpl ? registeredPackage : MaintenanceMonitoringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateBasedAnalysisPackage.eNS_URI);
		StateBasedAnalysisPackageImpl theStateBasedAnalysisPackage = (StateBasedAnalysisPackageImpl)(registeredPackage instanceof StateBasedAnalysisPackageImpl ? registeredPackage : StateBasedAnalysisPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MitigationMeansPackage.eNS_URI);
		MitigationMeansPackageImpl theMitigationMeansPackage = (MitigationMeansPackageImpl)(registeredPackage instanceof MitigationMeansPackageImpl ? registeredPackage : MitigationMeansPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParameterizedArchitecturePackage.eNS_URI);
		ParameterizedArchitecturePackageImpl theParameterizedArchitecturePackage = (ParameterizedArchitecturePackageImpl)(registeredPackage instanceof ParameterizedArchitecturePackageImpl ? registeredPackage : ParameterizedArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HardwareBaselinePackage.eNS_URI);
		HardwareBaselinePackageImpl theHardwareBaselinePackage = (HardwareBaselinePackageImpl)(registeredPackage instanceof HardwareBaselinePackageImpl ? registeredPackage : HardwareBaselinePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RTComponentModelPackage.eNS_URI);
		RTComponentModelPackageImpl theRTComponentModelPackage = (RTComponentModelPackageImpl)(registeredPackage instanceof RTComponentModelPackageImpl ? registeredPackage : RTComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ARINCComponentModelPackage.eNS_URI);
		ARINCComponentModelPackageImpl theARINCComponentModelPackage = (ARINCComponentModelPackageImpl)(registeredPackage instanceof ARINCComponentModelPackageImpl ? registeredPackage : ARINCComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentModelPackage.eNS_URI);
		ComponentModelPackageImpl theComponentModelPackage = (ComponentModelPackageImpl)(registeredPackage instanceof ComponentModelPackageImpl ? registeredPackage : ComponentModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SafetyPackage.eNS_URI);
		SafetyPackageImpl theSafetyPackage = (SafetyPackageImpl)(registeredPackage instanceof SafetyPackageImpl ? registeredPackage : SafetyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AsyncCommunicationPackage.eNS_URI);
		AsyncCommunicationPackageImpl theAsyncCommunicationPackage = (AsyncCommunicationPackageImpl)(registeredPackage instanceof AsyncCommunicationPackageImpl ? registeredPackage : AsyncCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StateMachinesPackage.eNS_URI);
		StateMachinesPackageImpl theStateMachinesPackage = (StateMachinesPackageImpl)(registeredPackage instanceof StateMachinesPackageImpl ? registeredPackage : StateMachinesPackage.eINSTANCE);

		// Create package meta-data objects
		theSTSPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCHESSViewsPackage.createPackageContents();
		theFailurePropagationPackage.createPackageContents();
		theFailurePropagationDataTypesPackage.createPackageContents();
		theDependableComponentPackage.createPackageContents();
		theThreatsPropagationPackage.createPackageContents();
		theStateBasedDataTypesPackage.createPackageContents();
		theStateBasedComponentsPackage.createPackageContents();
		theFaultTolerancePackage.createPackageContents();
		theMaintenanceMonitoringPackage.createPackageContents();
		theStateBasedAnalysisPackage.createPackageContents();
		theMitigationMeansPackage.createPackageContents();
		theParameterizedArchitecturePackage.createPackageContents();
		theHardwareBaselinePackage.createPackageContents();
		theRTComponentModelPackage.createPackageContents();
		theARINCComponentModelPackage.createPackageContents();
		theComponentModelPackage.createPackageContents();
		theSafetyPackage.createPackageContents();
		theAsyncCommunicationPackage.createPackageContents();
		theStateMachinesPackage.createPackageContents();

		// Initialize created meta-data
		theSTSPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCHESSViewsPackage.initializePackageContents();
		theFailurePropagationPackage.initializePackageContents();
		theFailurePropagationDataTypesPackage.initializePackageContents();
		theDependableComponentPackage.initializePackageContents();
		theThreatsPropagationPackage.initializePackageContents();
		theStateBasedDataTypesPackage.initializePackageContents();
		theStateBasedComponentsPackage.initializePackageContents();
		theFaultTolerancePackage.initializePackageContents();
		theMaintenanceMonitoringPackage.initializePackageContents();
		theStateBasedAnalysisPackage.initializePackageContents();
		theMitigationMeansPackage.initializePackageContents();
		theParameterizedArchitecturePackage.initializePackageContents();
		theHardwareBaselinePackage.initializePackageContents();
		theRTComponentModelPackage.initializePackageContents();
		theARINCComponentModelPackage.initializePackageContents();
		theComponentModelPackage.initializePackageContents();
		theSafetyPackage.initializePackageContents();
		theAsyncCommunicationPackage.initializePackageContents();
		theStateMachinesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSTSPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(STSPackage.eNS_URI, theSTSPackage);
		return theSTSPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHuman() {
		return humanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHuman_Base_Class() {
		return (EReference)humanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOrganization() {
		return organizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOrganization_Base_Class() {
		return (EReference)organizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOrganizationUnit() {
		return organizationUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOrganizationUnit_Base_Class() {
		return (EReference)organizationUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTechnological() {
		return technologicalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTechnological_Base_Class() {
		return (EReference)technologicalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimePressure() {
		return timePressureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUMissionManagement() {
		return ouMissionManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOURulesRegulationManagement() {
		return ouRulesRegulationManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUClimateManagement() {
		return ouClimateManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUOversightManagement() {
		return ouOversightManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUProcessManagement() {
		return ouProcessManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUResourceManagement() {
		return ouResourceManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHSAttention() {
		return hsAttentionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHumanSensorUnit() {
		return humanSensorUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHumanSensorUnit_Base_Class() {
		return (EReference)humanSensorUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHSPerception() {
		return hsPerceptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHSKnowledgePerception() {
		return hsKnowledgePerceptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHSSensory() {
		return hsSensoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHAFeedback() {
		return haFeedbackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHumanActuatorUnit() {
		return humanActuatorUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHumanActuatorUnit_Base_Class() {
		return (EReference)humanActuatorUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHAIntent() {
		return haIntentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHACommunication() {
		return haCommunicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHATimeManagement() {
		return haTimeManagementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHASelection() {
		return haSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHAResponse() {
		return haResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHAKnowledgeDecision() {
		return haKnowledgeDecisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STSFactory getSTSFactory() {
		return (STSFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		humanEClass = createEClass(HUMAN);
		createEReference(humanEClass, HUMAN__BASE_CLASS);

		organizationEClass = createEClass(ORGANIZATION);
		createEReference(organizationEClass, ORGANIZATION__BASE_CLASS);

		organizationUnitEClass = createEClass(ORGANIZATION_UNIT);
		createEReference(organizationUnitEClass, ORGANIZATION_UNIT__BASE_CLASS);

		technologicalEClass = createEClass(TECHNOLOGICAL);
		createEReference(technologicalEClass, TECHNOLOGICAL__BASE_CLASS);

		timePressureEClass = createEClass(TIME_PRESSURE);

		ouMissionManagementEClass = createEClass(OU_MISSION_MANAGEMENT);

		ouRulesRegulationManagementEClass = createEClass(OU_RULES_REGULATION_MANAGEMENT);

		ouClimateManagementEClass = createEClass(OU_CLIMATE_MANAGEMENT);

		ouOversightManagementEClass = createEClass(OU_OVERSIGHT_MANAGEMENT);

		ouProcessManagementEClass = createEClass(OU_PROCESS_MANAGEMENT);

		ouResourceManagementEClass = createEClass(OU_RESOURCE_MANAGEMENT);

		hsAttentionEClass = createEClass(HS_ATTENTION);

		humanSensorUnitEClass = createEClass(HUMAN_SENSOR_UNIT);
		createEReference(humanSensorUnitEClass, HUMAN_SENSOR_UNIT__BASE_CLASS);

		hsPerceptionEClass = createEClass(HS_PERCEPTION);

		hsKnowledgePerceptionEClass = createEClass(HS_KNOWLEDGE_PERCEPTION);

		hsSensoryEClass = createEClass(HS_SENSORY);

		haFeedbackEClass = createEClass(HA_FEEDBACK);

		humanActuatorUnitEClass = createEClass(HUMAN_ACTUATOR_UNIT);
		createEReference(humanActuatorUnitEClass, HUMAN_ACTUATOR_UNIT__BASE_CLASS);

		haIntentEClass = createEClass(HA_INTENT);

		haCommunicationEClass = createEClass(HA_COMMUNICATION);

		haTimeManagementEClass = createEClass(HA_TIME_MANAGEMENT);

		haSelectionEClass = createEClass(HA_SELECTION);

		haResponseEClass = createEClass(HA_RESPONSE);

		haKnowledgeDecisionEClass = createEClass(HA_KNOWLEDGE_DECISION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timePressureEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouMissionManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouRulesRegulationManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouClimateManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouOversightManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouProcessManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		ouResourceManagementEClass.getESuperTypes().add(this.getOrganizationUnit());
		hsAttentionEClass.getESuperTypes().add(this.getHumanSensorUnit());
		hsPerceptionEClass.getESuperTypes().add(this.getHumanSensorUnit());
		hsKnowledgePerceptionEClass.getESuperTypes().add(this.getHumanSensorUnit());
		hsSensoryEClass.getESuperTypes().add(this.getHumanSensorUnit());
		haFeedbackEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haIntentEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haCommunicationEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haTimeManagementEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haSelectionEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haResponseEClass.getESuperTypes().add(this.getHumanActuatorUnit());
		haKnowledgeDecisionEClass.getESuperTypes().add(this.getHumanActuatorUnit());

		// Initialize classes, features, and operations; add parameters
		initEClass(humanEClass, Human.class, "Human", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHuman_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, Human.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(organizationEClass, Organization.class, "Organization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganization_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(organizationUnitEClass, OrganizationUnit.class, "OrganizationUnit", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganizationUnit_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, OrganizationUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(technologicalEClass, Technological.class, "Technological", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTechnological_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, Technological.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(timePressureEClass, TimePressure.class, "TimePressure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouMissionManagementEClass, OUMissionManagement.class, "OUMissionManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouRulesRegulationManagementEClass, OURulesRegulationManagement.class, "OURulesRegulationManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouClimateManagementEClass, OUClimateManagement.class, "OUClimateManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouOversightManagementEClass, OUOversightManagement.class, "OUOversightManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouProcessManagementEClass, OUProcessManagement.class, "OUProcessManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ouResourceManagementEClass, OUResourceManagement.class, "OUResourceManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hsAttentionEClass, HSAttention.class, "HSAttention", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(humanSensorUnitEClass, HumanSensorUnit.class, "HumanSensorUnit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHumanSensorUnit_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, HumanSensorUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(hsPerceptionEClass, HSPerception.class, "HSPerception", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hsKnowledgePerceptionEClass, HSKnowledgePerception.class, "HSKnowledgePerception", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hsSensoryEClass, HSSensory.class, "HSSensory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haFeedbackEClass, HAFeedback.class, "HAFeedback", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(humanActuatorUnitEClass, HumanActuatorUnit.class, "HumanActuatorUnit", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHumanActuatorUnit_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, HumanActuatorUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(haIntentEClass, HAIntent.class, "HAIntent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haCommunicationEClass, HACommunication.class, "HACommunication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haTimeManagementEClass, HATimeManagement.class, "HATimeManagement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haSelectionEClass, HASelection.class, "HASelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haResponseEClass, HAResponse.class, "HAResponse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(haKnowledgeDecisionEClass, HAKnowledgeDecision.class, "HAKnowledgeDecision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //STSPackageImpl

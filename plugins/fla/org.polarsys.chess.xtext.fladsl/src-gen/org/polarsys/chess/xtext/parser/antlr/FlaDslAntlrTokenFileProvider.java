/*
 * generated by Xtext 2.25.0
 */
package org.polarsys.chess.xtext.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class FlaDslAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("org/polarsys/chess/xtext/parser/antlr/internal/InternalFlaDsl.tokens");
	}
}

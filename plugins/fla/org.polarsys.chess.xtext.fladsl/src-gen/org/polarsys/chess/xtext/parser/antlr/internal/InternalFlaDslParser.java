package org.polarsys.chess.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.polarsys.chess.xtext.services.FlaDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFlaDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FLA:'", "'->'", "';'", "','", "'undefined'", "'.'", "'{'", "'}'", "'noFailure'", "'wildcard'", "'early'", "'late'", "'commission'", "'omission'", "'valueSubtle'", "'valueCoarse'", "'incompletion'", "'none'", "'unspecified'", "'inconsistency'", "'interference'", "'impermanence'", "'all_or_nothing'", "'all_or_compensation'", "'full_consistency'", "'range_violation_allowed'", "'serializable'", "'portable_level'", "'no_loss'", "'partial_loss_allowed'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFlaDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFlaDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFlaDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFlaDsl.g"; }



     	private FlaDslGrammarAccess grammarAccess;

        public InternalFlaDslParser(TokenStream input, FlaDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Behaviour";
       	}

       	@Override
       	protected FlaDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleBehaviour"
    // InternalFlaDsl.g:65:1: entryRuleBehaviour returns [EObject current=null] : iv_ruleBehaviour= ruleBehaviour EOF ;
    public final EObject entryRuleBehaviour() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBehaviour = null;


        try {
            // InternalFlaDsl.g:65:50: (iv_ruleBehaviour= ruleBehaviour EOF )
            // InternalFlaDsl.g:66:2: iv_ruleBehaviour= ruleBehaviour EOF
            {
             newCompositeNode(grammarAccess.getBehaviourRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBehaviour=ruleBehaviour();

            state._fsp--;

             current =iv_ruleBehaviour; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBehaviour"


    // $ANTLR start "ruleBehaviour"
    // InternalFlaDsl.g:72:1: ruleBehaviour returns [EObject current=null] : ( (lv_rules_0_0= ruleExpression ) )+ ;
    public final EObject ruleBehaviour() throws RecognitionException {
        EObject current = null;

        EObject lv_rules_0_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:78:2: ( ( (lv_rules_0_0= ruleExpression ) )+ )
            // InternalFlaDsl.g:79:2: ( (lv_rules_0_0= ruleExpression ) )+
            {
            // InternalFlaDsl.g:79:2: ( (lv_rules_0_0= ruleExpression ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalFlaDsl.g:80:3: (lv_rules_0_0= ruleExpression )
            	    {
            	    // InternalFlaDsl.g:80:3: (lv_rules_0_0= ruleExpression )
            	    // InternalFlaDsl.g:81:4: lv_rules_0_0= ruleExpression
            	    {

            	    				newCompositeNode(grammarAccess.getBehaviourAccess().getRulesExpressionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_rules_0_0=ruleExpression();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getBehaviourRule());
            	    				}
            	    				add(
            	    					current,
            	    					"rules",
            	    					lv_rules_0_0,
            	    					"org.polarsys.chess.xtext.FlaDsl.Expression");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBehaviour"


    // $ANTLR start "entryRuleExpression"
    // InternalFlaDsl.g:101:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalFlaDsl.g:101:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalFlaDsl.g:102:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalFlaDsl.g:108:1: ruleExpression returns [EObject current=null] : (otherlv_0= 'FLA:' ( (lv_lhs_1_0= ruleLhs ) ) otherlv_2= '->' ( (lv_rhs_3_0= ruleRhs ) ) otherlv_4= ';' ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_lhs_1_0 = null;

        EObject lv_rhs_3_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:114:2: ( (otherlv_0= 'FLA:' ( (lv_lhs_1_0= ruleLhs ) ) otherlv_2= '->' ( (lv_rhs_3_0= ruleRhs ) ) otherlv_4= ';' ) )
            // InternalFlaDsl.g:115:2: (otherlv_0= 'FLA:' ( (lv_lhs_1_0= ruleLhs ) ) otherlv_2= '->' ( (lv_rhs_3_0= ruleRhs ) ) otherlv_4= ';' )
            {
            // InternalFlaDsl.g:115:2: (otherlv_0= 'FLA:' ( (lv_lhs_1_0= ruleLhs ) ) otherlv_2= '->' ( (lv_rhs_3_0= ruleRhs ) ) otherlv_4= ';' )
            // InternalFlaDsl.g:116:3: otherlv_0= 'FLA:' ( (lv_lhs_1_0= ruleLhs ) ) otherlv_2= '->' ( (lv_rhs_3_0= ruleRhs ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getExpressionAccess().getFLAKeyword_0());
            		
            // InternalFlaDsl.g:120:3: ( (lv_lhs_1_0= ruleLhs ) )
            // InternalFlaDsl.g:121:4: (lv_lhs_1_0= ruleLhs )
            {
            // InternalFlaDsl.g:121:4: (lv_lhs_1_0= ruleLhs )
            // InternalFlaDsl.g:122:5: lv_lhs_1_0= ruleLhs
            {

            					newCompositeNode(grammarAccess.getExpressionAccess().getLhsLhsParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_5);
            lv_lhs_1_0=ruleLhs();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExpressionRule());
            					}
            					set(
            						current,
            						"lhs",
            						lv_lhs_1_0,
            						"org.polarsys.chess.xtext.FlaDsl.Lhs");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getExpressionAccess().getHyphenMinusGreaterThanSignKeyword_2());
            		
            // InternalFlaDsl.g:143:3: ( (lv_rhs_3_0= ruleRhs ) )
            // InternalFlaDsl.g:144:4: (lv_rhs_3_0= ruleRhs )
            {
            // InternalFlaDsl.g:144:4: (lv_rhs_3_0= ruleRhs )
            // InternalFlaDsl.g:145:5: lv_rhs_3_0= ruleRhs
            {

            					newCompositeNode(grammarAccess.getExpressionAccess().getRhsRhsParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_rhs_3_0=ruleRhs();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExpressionRule());
            					}
            					set(
            						current,
            						"rhs",
            						lv_rhs_3_0,
            						"org.polarsys.chess.xtext.FlaDsl.Rhs");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getExpressionAccess().getSemicolonKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleLhs"
    // InternalFlaDsl.g:170:1: entryRuleLhs returns [EObject current=null] : iv_ruleLhs= ruleLhs EOF ;
    public final EObject entryRuleLhs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLhs = null;


        try {
            // InternalFlaDsl.g:170:44: (iv_ruleLhs= ruleLhs EOF )
            // InternalFlaDsl.g:171:2: iv_ruleLhs= ruleLhs EOF
            {
             newCompositeNode(grammarAccess.getLhsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLhs=ruleLhs();

            state._fsp--;

             current =iv_ruleLhs; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLhs"


    // $ANTLR start "ruleLhs"
    // InternalFlaDsl.g:177:1: ruleLhs returns [EObject current=null] : ( ( (lv_failures_0_0= ruleInputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )* ) ;
    public final EObject ruleLhs() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_failures_0_0 = null;

        EObject lv_failures_2_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:183:2: ( ( ( (lv_failures_0_0= ruleInputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )* ) )
            // InternalFlaDsl.g:184:2: ( ( (lv_failures_0_0= ruleInputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )* )
            {
            // InternalFlaDsl.g:184:2: ( ( (lv_failures_0_0= ruleInputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )* )
            // InternalFlaDsl.g:185:3: ( (lv_failures_0_0= ruleInputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )*
            {
            // InternalFlaDsl.g:185:3: ( (lv_failures_0_0= ruleInputExpression ) )
            // InternalFlaDsl.g:186:4: (lv_failures_0_0= ruleInputExpression )
            {
            // InternalFlaDsl.g:186:4: (lv_failures_0_0= ruleInputExpression )
            // InternalFlaDsl.g:187:5: lv_failures_0_0= ruleInputExpression
            {

            					newCompositeNode(grammarAccess.getLhsAccess().getFailuresInputExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_7);
            lv_failures_0_0=ruleInputExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLhsRule());
            					}
            					add(
            						current,
            						"failures",
            						lv_failures_0_0,
            						"org.polarsys.chess.xtext.FlaDsl.InputExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFlaDsl.g:204:3: (otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalFlaDsl.g:205:4: otherlv_1= ',' ( (lv_failures_2_0= ruleInputExpression ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FOLLOW_4); 

            	    				newLeafNode(otherlv_1, grammarAccess.getLhsAccess().getCommaKeyword_1_0());
            	    			
            	    // InternalFlaDsl.g:209:4: ( (lv_failures_2_0= ruleInputExpression ) )
            	    // InternalFlaDsl.g:210:5: (lv_failures_2_0= ruleInputExpression )
            	    {
            	    // InternalFlaDsl.g:210:5: (lv_failures_2_0= ruleInputExpression )
            	    // InternalFlaDsl.g:211:6: lv_failures_2_0= ruleInputExpression
            	    {

            	    						newCompositeNode(grammarAccess.getLhsAccess().getFailuresInputExpressionParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_failures_2_0=ruleInputExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLhsRule());
            	    						}
            	    						add(
            	    							current,
            	    							"failures",
            	    							lv_failures_2_0,
            	    							"org.polarsys.chess.xtext.FlaDsl.InputExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLhs"


    // $ANTLR start "entryRuleRhs"
    // InternalFlaDsl.g:233:1: entryRuleRhs returns [EObject current=null] : iv_ruleRhs= ruleRhs EOF ;
    public final EObject entryRuleRhs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRhs = null;


        try {
            // InternalFlaDsl.g:233:44: (iv_ruleRhs= ruleRhs EOF )
            // InternalFlaDsl.g:234:2: iv_ruleRhs= ruleRhs EOF
            {
             newCompositeNode(grammarAccess.getRhsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRhs=ruleRhs();

            state._fsp--;

             current =iv_ruleRhs; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRhs"


    // $ANTLR start "ruleRhs"
    // InternalFlaDsl.g:240:1: ruleRhs returns [EObject current=null] : ( ( (lv_failures_0_0= ruleOutputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )* ) ;
    public final EObject ruleRhs() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_failures_0_0 = null;

        EObject lv_failures_2_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:246:2: ( ( ( (lv_failures_0_0= ruleOutputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )* ) )
            // InternalFlaDsl.g:247:2: ( ( (lv_failures_0_0= ruleOutputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )* )
            {
            // InternalFlaDsl.g:247:2: ( ( (lv_failures_0_0= ruleOutputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )* )
            // InternalFlaDsl.g:248:3: ( (lv_failures_0_0= ruleOutputExpression ) ) (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )*
            {
            // InternalFlaDsl.g:248:3: ( (lv_failures_0_0= ruleOutputExpression ) )
            // InternalFlaDsl.g:249:4: (lv_failures_0_0= ruleOutputExpression )
            {
            // InternalFlaDsl.g:249:4: (lv_failures_0_0= ruleOutputExpression )
            // InternalFlaDsl.g:250:5: lv_failures_0_0= ruleOutputExpression
            {

            					newCompositeNode(grammarAccess.getRhsAccess().getFailuresOutputExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_7);
            lv_failures_0_0=ruleOutputExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRhsRule());
            					}
            					add(
            						current,
            						"failures",
            						lv_failures_0_0,
            						"org.polarsys.chess.xtext.FlaDsl.OutputExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFlaDsl.g:267:3: (otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalFlaDsl.g:268:4: otherlv_1= ',' ( (lv_failures_2_0= ruleOutputExpression ) )
            	    {
            	    otherlv_1=(Token)match(input,14,FOLLOW_4); 

            	    				newLeafNode(otherlv_1, grammarAccess.getRhsAccess().getCommaKeyword_1_0());
            	    			
            	    // InternalFlaDsl.g:272:4: ( (lv_failures_2_0= ruleOutputExpression ) )
            	    // InternalFlaDsl.g:273:5: (lv_failures_2_0= ruleOutputExpression )
            	    {
            	    // InternalFlaDsl.g:273:5: (lv_failures_2_0= ruleOutputExpression )
            	    // InternalFlaDsl.g:274:6: lv_failures_2_0= ruleOutputExpression
            	    {

            	    						newCompositeNode(grammarAccess.getRhsAccess().getFailuresOutputExpressionParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_7);
            	    lv_failures_2_0=ruleOutputExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getRhsRule());
            	    						}
            	    						add(
            	    							current,
            	    							"failures",
            	    							lv_failures_2_0,
            	    							"org.polarsys.chess.xtext.FlaDsl.OutputExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRhs"


    // $ANTLR start "entryRuleInputExpression"
    // InternalFlaDsl.g:296:1: entryRuleInputExpression returns [EObject current=null] : iv_ruleInputExpression= ruleInputExpression EOF ;
    public final EObject entryRuleInputExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputExpression = null;


        try {
            // InternalFlaDsl.g:296:56: (iv_ruleInputExpression= ruleInputExpression EOF )
            // InternalFlaDsl.g:297:2: iv_ruleInputExpression= ruleInputExpression EOF
            {
             newCompositeNode(grammarAccess.getInputExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInputExpression=ruleInputExpression();

            state._fsp--;

             current =iv_ruleInputExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputExpression"


    // $ANTLR start "ruleInputExpression"
    // InternalFlaDsl.g:303:1: ruleInputExpression returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleInFailureExpr ) ) ) ;
    public final EObject ruleInputExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_failureExpr_3_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:309:2: ( ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleInFailureExpr ) ) ) )
            // InternalFlaDsl.g:310:2: ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleInFailureExpr ) ) )
            {
            // InternalFlaDsl.g:310:2: ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleInFailureExpr ) ) )
            // InternalFlaDsl.g:311:3: ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleInFailureExpr ) )
            {
            // InternalFlaDsl.g:311:3: ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==15) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalFlaDsl.g:312:4: ( (otherlv_0= RULE_ID ) )
                    {
                    // InternalFlaDsl.g:312:4: ( (otherlv_0= RULE_ID ) )
                    // InternalFlaDsl.g:313:5: (otherlv_0= RULE_ID )
                    {
                    // InternalFlaDsl.g:313:5: (otherlv_0= RULE_ID )
                    // InternalFlaDsl.g:314:6: otherlv_0= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getInputExpressionRule());
                    						}
                    					
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_8); 

                    						newLeafNode(otherlv_0, grammarAccess.getInputExpressionAccess().getRefPortCrossReference_0_0_0());
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:326:4: otherlv_1= 'undefined'
                    {
                    otherlv_1=(Token)match(input,15,FOLLOW_8); 

                    				newLeafNode(otherlv_1, grammarAccess.getInputExpressionAccess().getUndefinedKeyword_0_1());
                    			

                    }
                    break;

            }

            otherlv_2=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getInputExpressionAccess().getFullStopKeyword_1());
            		
            // InternalFlaDsl.g:335:3: ( (lv_failureExpr_3_0= ruleInFailureExpr ) )
            // InternalFlaDsl.g:336:4: (lv_failureExpr_3_0= ruleInFailureExpr )
            {
            // InternalFlaDsl.g:336:4: (lv_failureExpr_3_0= ruleInFailureExpr )
            // InternalFlaDsl.g:337:5: lv_failureExpr_3_0= ruleInFailureExpr
            {

            					newCompositeNode(grammarAccess.getInputExpressionAccess().getFailureExprInFailureExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_failureExpr_3_0=ruleInFailureExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInputExpressionRule());
            					}
            					set(
            						current,
            						"failureExpr",
            						lv_failureExpr_3_0,
            						"org.polarsys.chess.xtext.FlaDsl.InFailureExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputExpression"


    // $ANTLR start "entryRuleOutputExpression"
    // InternalFlaDsl.g:358:1: entryRuleOutputExpression returns [EObject current=null] : iv_ruleOutputExpression= ruleOutputExpression EOF ;
    public final EObject entryRuleOutputExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputExpression = null;


        try {
            // InternalFlaDsl.g:358:57: (iv_ruleOutputExpression= ruleOutputExpression EOF )
            // InternalFlaDsl.g:359:2: iv_ruleOutputExpression= ruleOutputExpression EOF
            {
             newCompositeNode(grammarAccess.getOutputExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputExpression=ruleOutputExpression();

            state._fsp--;

             current =iv_ruleOutputExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputExpression"


    // $ANTLR start "ruleOutputExpression"
    // InternalFlaDsl.g:365:1: ruleOutputExpression returns [EObject current=null] : ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleOutFailureExpr ) ) ) ;
    public final EObject ruleOutputExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_failureExpr_3_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:371:2: ( ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleOutFailureExpr ) ) ) )
            // InternalFlaDsl.g:372:2: ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleOutFailureExpr ) ) )
            {
            // InternalFlaDsl.g:372:2: ( ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleOutFailureExpr ) ) )
            // InternalFlaDsl.g:373:3: ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' ) otherlv_2= '.' ( (lv_failureExpr_3_0= ruleOutFailureExpr ) )
            {
            // InternalFlaDsl.g:373:3: ( ( (otherlv_0= RULE_ID ) ) | otherlv_1= 'undefined' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            else if ( (LA5_0==15) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalFlaDsl.g:374:4: ( (otherlv_0= RULE_ID ) )
                    {
                    // InternalFlaDsl.g:374:4: ( (otherlv_0= RULE_ID ) )
                    // InternalFlaDsl.g:375:5: (otherlv_0= RULE_ID )
                    {
                    // InternalFlaDsl.g:375:5: (otherlv_0= RULE_ID )
                    // InternalFlaDsl.g:376:6: otherlv_0= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOutputExpressionRule());
                    						}
                    					
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_8); 

                    						newLeafNode(otherlv_0, grammarAccess.getOutputExpressionAccess().getRefPortCrossReference_0_0_0());
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:388:4: otherlv_1= 'undefined'
                    {
                    otherlv_1=(Token)match(input,15,FOLLOW_8); 

                    				newLeafNode(otherlv_1, grammarAccess.getOutputExpressionAccess().getUndefinedKeyword_0_1());
                    			

                    }
                    break;

            }

            otherlv_2=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getOutputExpressionAccess().getFullStopKeyword_1());
            		
            // InternalFlaDsl.g:397:3: ( (lv_failureExpr_3_0= ruleOutFailureExpr ) )
            // InternalFlaDsl.g:398:4: (lv_failureExpr_3_0= ruleOutFailureExpr )
            {
            // InternalFlaDsl.g:398:4: (lv_failureExpr_3_0= ruleOutFailureExpr )
            // InternalFlaDsl.g:399:5: lv_failureExpr_3_0= ruleOutFailureExpr
            {

            					newCompositeNode(grammarAccess.getOutputExpressionAccess().getFailureExprOutFailureExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_failureExpr_3_0=ruleOutFailureExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOutputExpressionRule());
            					}
            					set(
            						current,
            						"failureExpr",
            						lv_failureExpr_3_0,
            						"org.polarsys.chess.xtext.FlaDsl.OutFailureExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputExpression"


    // $ANTLR start "entryRuleInFailureExpr"
    // InternalFlaDsl.g:420:1: entryRuleInFailureExpr returns [EObject current=null] : iv_ruleInFailureExpr= ruleInFailureExpr EOF ;
    public final EObject entryRuleInFailureExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInFailureExpr = null;


        try {
            // InternalFlaDsl.g:420:54: (iv_ruleInFailureExpr= ruleInFailureExpr EOF )
            // InternalFlaDsl.g:421:2: iv_ruleInFailureExpr= ruleInFailureExpr EOF
            {
             newCompositeNode(grammarAccess.getInFailureExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInFailureExpr=ruleInFailureExpr();

            state._fsp--;

             current =iv_ruleInFailureExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInFailureExpr"


    // $ANTLR start "ruleInFailureExpr"
    // InternalFlaDsl.g:427:1: ruleInFailureExpr returns [EObject current=null] : ( ( (lv_failures_0_0= ruleWildcardDefinition ) ) | ( (lv_failures_1_0= ruleNoFailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) ) ;
    public final EObject ruleInFailureExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_failures_0_0 = null;

        EObject lv_failures_1_0 = null;

        EObject lv_failures_2_0 = null;

        EObject lv_failures_4_0 = null;

        EObject lv_failures_6_0 = null;

        EObject lv_failures_8_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:433:2: ( ( ( (lv_failures_0_0= ruleWildcardDefinition ) ) | ( (lv_failures_1_0= ruleNoFailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) ) )
            // InternalFlaDsl.g:434:2: ( ( (lv_failures_0_0= ruleWildcardDefinition ) ) | ( (lv_failures_1_0= ruleNoFailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) )
            {
            // InternalFlaDsl.g:434:2: ( ( (lv_failures_0_0= ruleWildcardDefinition ) ) | ( (lv_failures_1_0= ruleNoFailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) )
            int alt7=5;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt7=1;
                }
                break;
            case 19:
                {
                alt7=2;
                }
                break;
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                {
                alt7=3;
                }
                break;
            case 17:
                {
                alt7=4;
                }
                break;
            case RULE_ID:
                {
                alt7=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalFlaDsl.g:435:3: ( (lv_failures_0_0= ruleWildcardDefinition ) )
                    {
                    // InternalFlaDsl.g:435:3: ( (lv_failures_0_0= ruleWildcardDefinition ) )
                    // InternalFlaDsl.g:436:4: (lv_failures_0_0= ruleWildcardDefinition )
                    {
                    // InternalFlaDsl.g:436:4: (lv_failures_0_0= ruleWildcardDefinition )
                    // InternalFlaDsl.g:437:5: lv_failures_0_0= ruleWildcardDefinition
                    {

                    					newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresWildcardDefinitionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_0_0=ruleWildcardDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_0_0,
                    						"org.polarsys.chess.xtext.FlaDsl.WildcardDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:455:3: ( (lv_failures_1_0= ruleNoFailureDefinition ) )
                    {
                    // InternalFlaDsl.g:455:3: ( (lv_failures_1_0= ruleNoFailureDefinition ) )
                    // InternalFlaDsl.g:456:4: (lv_failures_1_0= ruleNoFailureDefinition )
                    {
                    // InternalFlaDsl.g:456:4: (lv_failures_1_0= ruleNoFailureDefinition )
                    // InternalFlaDsl.g:457:5: lv_failures_1_0= ruleNoFailureDefinition
                    {

                    					newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresNoFailureDefinitionParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_1_0=ruleNoFailureDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_1_0,
                    						"org.polarsys.chess.xtext.FlaDsl.NoFailureDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:475:3: ( (lv_failures_2_0= ruleFailureDefinition ) )
                    {
                    // InternalFlaDsl.g:475:3: ( (lv_failures_2_0= ruleFailureDefinition ) )
                    // InternalFlaDsl.g:476:4: (lv_failures_2_0= ruleFailureDefinition )
                    {
                    // InternalFlaDsl.g:476:4: (lv_failures_2_0= ruleFailureDefinition )
                    // InternalFlaDsl.g:477:5: lv_failures_2_0= ruleFailureDefinition
                    {

                    					newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_2_0=ruleFailureDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_2_0,
                    						"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:495:3: (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' )
                    {
                    // InternalFlaDsl.g:495:3: (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' )
                    // InternalFlaDsl.g:496:4: otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}'
                    {
                    otherlv_3=(Token)match(input,17,FOLLOW_10); 

                    				newLeafNode(otherlv_3, grammarAccess.getInFailureExprAccess().getLeftCurlyBracketKeyword_3_0());
                    			
                    // InternalFlaDsl.g:500:4: ( (lv_failures_4_0= ruleFailureDefinition ) )
                    // InternalFlaDsl.g:501:5: (lv_failures_4_0= ruleFailureDefinition )
                    {
                    // InternalFlaDsl.g:501:5: (lv_failures_4_0= ruleFailureDefinition )
                    // InternalFlaDsl.g:502:6: lv_failures_4_0= ruleFailureDefinition
                    {

                    						newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_failures_4_0=ruleFailureDefinition();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    						}
                    						add(
                    							current,
                    							"failures",
                    							lv_failures_4_0,
                    							"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalFlaDsl.g:519:4: (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==14) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalFlaDsl.g:520:5: otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) )
                    	    {
                    	    otherlv_5=(Token)match(input,14,FOLLOW_10); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getInFailureExprAccess().getCommaKeyword_3_2_0());
                    	    				
                    	    // InternalFlaDsl.g:524:5: ( (lv_failures_6_0= ruleFailureDefinition ) )
                    	    // InternalFlaDsl.g:525:6: (lv_failures_6_0= ruleFailureDefinition )
                    	    {
                    	    // InternalFlaDsl.g:525:6: (lv_failures_6_0= ruleFailureDefinition )
                    	    // InternalFlaDsl.g:526:7: lv_failures_6_0= ruleFailureDefinition
                    	    {

                    	    							newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_3_2_1_0());
                    	    						
                    	    pushFollow(FOLLOW_12);
                    	    lv_failures_6_0=ruleFailureDefinition();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"failures",
                    	    								lv_failures_6_0,
                    	    								"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);

                    otherlv_7=(Token)match(input,18,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getInFailureExprAccess().getRightCurlyBracketKeyword_3_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalFlaDsl.g:550:3: ( (lv_failures_8_0= ruleVariableDefinition ) )
                    {
                    // InternalFlaDsl.g:550:3: ( (lv_failures_8_0= ruleVariableDefinition ) )
                    // InternalFlaDsl.g:551:4: (lv_failures_8_0= ruleVariableDefinition )
                    {
                    // InternalFlaDsl.g:551:4: (lv_failures_8_0= ruleVariableDefinition )
                    // InternalFlaDsl.g:552:5: lv_failures_8_0= ruleVariableDefinition
                    {

                    					newCompositeNode(grammarAccess.getInFailureExprAccess().getFailuresVariableDefinitionParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_8_0=ruleVariableDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getInFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_8_0,
                    						"org.polarsys.chess.xtext.FlaDsl.VariableDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInFailureExpr"


    // $ANTLR start "entryRuleOutFailureExpr"
    // InternalFlaDsl.g:573:1: entryRuleOutFailureExpr returns [EObject current=null] : iv_ruleOutFailureExpr= ruleOutFailureExpr EOF ;
    public final EObject entryRuleOutFailureExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutFailureExpr = null;


        try {
            // InternalFlaDsl.g:573:55: (iv_ruleOutFailureExpr= ruleOutFailureExpr EOF )
            // InternalFlaDsl.g:574:2: iv_ruleOutFailureExpr= ruleOutFailureExpr EOF
            {
             newCompositeNode(grammarAccess.getOutFailureExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutFailureExpr=ruleOutFailureExpr();

            state._fsp--;

             current =iv_ruleOutFailureExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutFailureExpr"


    // $ANTLR start "ruleOutFailureExpr"
    // InternalFlaDsl.g:580:1: ruleOutFailureExpr returns [EObject current=null] : ( ( (lv_failures_0_0= ruleNoFailureDefinition ) ) | ( (lv_failures_1_0= ruleComplexNofailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) ) ;
    public final EObject ruleOutFailureExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_failures_0_0 = null;

        EObject lv_failures_1_0 = null;

        EObject lv_failures_2_0 = null;

        EObject lv_failures_4_0 = null;

        EObject lv_failures_6_0 = null;

        EObject lv_failures_8_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:586:2: ( ( ( (lv_failures_0_0= ruleNoFailureDefinition ) ) | ( (lv_failures_1_0= ruleComplexNofailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) ) )
            // InternalFlaDsl.g:587:2: ( ( (lv_failures_0_0= ruleNoFailureDefinition ) ) | ( (lv_failures_1_0= ruleComplexNofailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) )
            {
            // InternalFlaDsl.g:587:2: ( ( (lv_failures_0_0= ruleNoFailureDefinition ) ) | ( (lv_failures_1_0= ruleComplexNofailureDefinition ) ) | ( (lv_failures_2_0= ruleFailureDefinition ) ) | (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' ) | ( (lv_failures_8_0= ruleVariableDefinition ) ) )
            int alt9=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==EOF||(LA9_1>=13 && LA9_1<=14)) ) {
                    alt9=1;
                }
                else if ( (LA9_1==16) ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
                }
                break;
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                {
                alt9=3;
                }
                break;
            case 17:
                {
                alt9=4;
                }
                break;
            case RULE_ID:
                {
                alt9=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalFlaDsl.g:588:3: ( (lv_failures_0_0= ruleNoFailureDefinition ) )
                    {
                    // InternalFlaDsl.g:588:3: ( (lv_failures_0_0= ruleNoFailureDefinition ) )
                    // InternalFlaDsl.g:589:4: (lv_failures_0_0= ruleNoFailureDefinition )
                    {
                    // InternalFlaDsl.g:589:4: (lv_failures_0_0= ruleNoFailureDefinition )
                    // InternalFlaDsl.g:590:5: lv_failures_0_0= ruleNoFailureDefinition
                    {

                    					newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresNoFailureDefinitionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_0_0=ruleNoFailureDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_0_0,
                    						"org.polarsys.chess.xtext.FlaDsl.NoFailureDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:608:3: ( (lv_failures_1_0= ruleComplexNofailureDefinition ) )
                    {
                    // InternalFlaDsl.g:608:3: ( (lv_failures_1_0= ruleComplexNofailureDefinition ) )
                    // InternalFlaDsl.g:609:4: (lv_failures_1_0= ruleComplexNofailureDefinition )
                    {
                    // InternalFlaDsl.g:609:4: (lv_failures_1_0= ruleComplexNofailureDefinition )
                    // InternalFlaDsl.g:610:5: lv_failures_1_0= ruleComplexNofailureDefinition
                    {

                    					newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresComplexNofailureDefinitionParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_1_0=ruleComplexNofailureDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_1_0,
                    						"org.polarsys.chess.xtext.FlaDsl.ComplexNofailureDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:628:3: ( (lv_failures_2_0= ruleFailureDefinition ) )
                    {
                    // InternalFlaDsl.g:628:3: ( (lv_failures_2_0= ruleFailureDefinition ) )
                    // InternalFlaDsl.g:629:4: (lv_failures_2_0= ruleFailureDefinition )
                    {
                    // InternalFlaDsl.g:629:4: (lv_failures_2_0= ruleFailureDefinition )
                    // InternalFlaDsl.g:630:5: lv_failures_2_0= ruleFailureDefinition
                    {

                    					newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_2_0=ruleFailureDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_2_0,
                    						"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:648:3: (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' )
                    {
                    // InternalFlaDsl.g:648:3: (otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}' )
                    // InternalFlaDsl.g:649:4: otherlv_3= '{' ( (lv_failures_4_0= ruleFailureDefinition ) ) (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+ otherlv_7= '}'
                    {
                    otherlv_3=(Token)match(input,17,FOLLOW_10); 

                    				newLeafNode(otherlv_3, grammarAccess.getOutFailureExprAccess().getLeftCurlyBracketKeyword_3_0());
                    			
                    // InternalFlaDsl.g:653:4: ( (lv_failures_4_0= ruleFailureDefinition ) )
                    // InternalFlaDsl.g:654:5: (lv_failures_4_0= ruleFailureDefinition )
                    {
                    // InternalFlaDsl.g:654:5: (lv_failures_4_0= ruleFailureDefinition )
                    // InternalFlaDsl.g:655:6: lv_failures_4_0= ruleFailureDefinition
                    {

                    						newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_failures_4_0=ruleFailureDefinition();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    						}
                    						add(
                    							current,
                    							"failures",
                    							lv_failures_4_0,
                    							"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalFlaDsl.g:672:4: (otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) ) )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==14) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalFlaDsl.g:673:5: otherlv_5= ',' ( (lv_failures_6_0= ruleFailureDefinition ) )
                    	    {
                    	    otherlv_5=(Token)match(input,14,FOLLOW_10); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getOutFailureExprAccess().getCommaKeyword_3_2_0());
                    	    				
                    	    // InternalFlaDsl.g:677:5: ( (lv_failures_6_0= ruleFailureDefinition ) )
                    	    // InternalFlaDsl.g:678:6: (lv_failures_6_0= ruleFailureDefinition )
                    	    {
                    	    // InternalFlaDsl.g:678:6: (lv_failures_6_0= ruleFailureDefinition )
                    	    // InternalFlaDsl.g:679:7: lv_failures_6_0= ruleFailureDefinition
                    	    {

                    	    							newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresFailureDefinitionParserRuleCall_3_2_1_0());
                    	    						
                    	    pushFollow(FOLLOW_12);
                    	    lv_failures_6_0=ruleFailureDefinition();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"failures",
                    	    								lv_failures_6_0,
                    	    								"org.polarsys.chess.xtext.FlaDsl.FailureDefinition");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);

                    otherlv_7=(Token)match(input,18,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getOutFailureExprAccess().getRightCurlyBracketKeyword_3_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalFlaDsl.g:703:3: ( (lv_failures_8_0= ruleVariableDefinition ) )
                    {
                    // InternalFlaDsl.g:703:3: ( (lv_failures_8_0= ruleVariableDefinition ) )
                    // InternalFlaDsl.g:704:4: (lv_failures_8_0= ruleVariableDefinition )
                    {
                    // InternalFlaDsl.g:704:4: (lv_failures_8_0= ruleVariableDefinition )
                    // InternalFlaDsl.g:705:5: lv_failures_8_0= ruleVariableDefinition
                    {

                    					newCompositeNode(grammarAccess.getOutFailureExprAccess().getFailuresVariableDefinitionParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_failures_8_0=ruleVariableDefinition();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getOutFailureExprRule());
                    					}
                    					add(
                    						current,
                    						"failures",
                    						lv_failures_8_0,
                    						"org.polarsys.chess.xtext.FlaDsl.VariableDefinition");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutFailureExpr"


    // $ANTLR start "entryRuleFailureDefinition"
    // InternalFlaDsl.g:726:1: entryRuleFailureDefinition returns [EObject current=null] : iv_ruleFailureDefinition= ruleFailureDefinition EOF ;
    public final EObject entryRuleFailureDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFailureDefinition = null;


        try {
            // InternalFlaDsl.g:726:58: (iv_ruleFailureDefinition= ruleFailureDefinition EOF )
            // InternalFlaDsl.g:727:2: iv_ruleFailureDefinition= ruleFailureDefinition EOF
            {
             newCompositeNode(grammarAccess.getFailureDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFailureDefinition=ruleFailureDefinition();

            state._fsp--;

             current =iv_ruleFailureDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFailureDefinition"


    // $ANTLR start "ruleFailureDefinition"
    // InternalFlaDsl.g:733:1: ruleFailureDefinition returns [EObject current=null] : ( () ( (lv_type_1_0= ruleActualFailureType ) ) (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )? ) ;
    public final EObject ruleFailureDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Enumerator lv_type_1_0 = null;

        EObject lv_acidAvoidable_3_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:739:2: ( ( () ( (lv_type_1_0= ruleActualFailureType ) ) (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )? ) )
            // InternalFlaDsl.g:740:2: ( () ( (lv_type_1_0= ruleActualFailureType ) ) (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )? )
            {
            // InternalFlaDsl.g:740:2: ( () ( (lv_type_1_0= ruleActualFailureType ) ) (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )? )
            // InternalFlaDsl.g:741:3: () ( (lv_type_1_0= ruleActualFailureType ) ) (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )?
            {
            // InternalFlaDsl.g:741:3: ()
            // InternalFlaDsl.g:742:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFailureDefinitionAccess().getFailureDefinitionAction_0(),
            					current);
            			

            }

            // InternalFlaDsl.g:748:3: ( (lv_type_1_0= ruleActualFailureType ) )
            // InternalFlaDsl.g:749:4: (lv_type_1_0= ruleActualFailureType )
            {
            // InternalFlaDsl.g:749:4: (lv_type_1_0= ruleActualFailureType )
            // InternalFlaDsl.g:750:5: lv_type_1_0= ruleActualFailureType
            {

            					newCompositeNode(grammarAccess.getFailureDefinitionAccess().getTypeActualFailureTypeEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_13);
            lv_type_1_0=ruleActualFailureType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFailureDefinitionRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_1_0,
            						"org.polarsys.chess.xtext.FlaDsl.ActualFailureType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFlaDsl.g:767:3: (otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==16) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalFlaDsl.g:768:4: otherlv_2= '.' ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) )
                    {
                    otherlv_2=(Token)match(input,16,FOLLOW_14); 

                    				newLeafNode(otherlv_2, grammarAccess.getFailureDefinitionAccess().getFullStopKeyword_2_0());
                    			
                    // InternalFlaDsl.g:772:4: ( (lv_acidAvoidable_3_0= ruleACIDavoidable ) )
                    // InternalFlaDsl.g:773:5: (lv_acidAvoidable_3_0= ruleACIDavoidable )
                    {
                    // InternalFlaDsl.g:773:5: (lv_acidAvoidable_3_0= ruleACIDavoidable )
                    // InternalFlaDsl.g:774:6: lv_acidAvoidable_3_0= ruleACIDavoidable
                    {

                    						newCompositeNode(grammarAccess.getFailureDefinitionAccess().getAcidAvoidableACIDavoidableParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_acidAvoidable_3_0=ruleACIDavoidable();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFailureDefinitionRule());
                    						}
                    						set(
                    							current,
                    							"acidAvoidable",
                    							lv_acidAvoidable_3_0,
                    							"org.polarsys.chess.xtext.FlaDsl.ACIDavoidable");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFailureDefinition"


    // $ANTLR start "entryRuleNoFailureDefinition"
    // InternalFlaDsl.g:796:1: entryRuleNoFailureDefinition returns [EObject current=null] : iv_ruleNoFailureDefinition= ruleNoFailureDefinition EOF ;
    public final EObject entryRuleNoFailureDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNoFailureDefinition = null;


        try {
            // InternalFlaDsl.g:796:60: (iv_ruleNoFailureDefinition= ruleNoFailureDefinition EOF )
            // InternalFlaDsl.g:797:2: iv_ruleNoFailureDefinition= ruleNoFailureDefinition EOF
            {
             newCompositeNode(grammarAccess.getNoFailureDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNoFailureDefinition=ruleNoFailureDefinition();

            state._fsp--;

             current =iv_ruleNoFailureDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNoFailureDefinition"


    // $ANTLR start "ruleNoFailureDefinition"
    // InternalFlaDsl.g:803:1: ruleNoFailureDefinition returns [EObject current=null] : ( () otherlv_1= 'noFailure' ) ;
    public final EObject ruleNoFailureDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:809:2: ( ( () otherlv_1= 'noFailure' ) )
            // InternalFlaDsl.g:810:2: ( () otherlv_1= 'noFailure' )
            {
            // InternalFlaDsl.g:810:2: ( () otherlv_1= 'noFailure' )
            // InternalFlaDsl.g:811:3: () otherlv_1= 'noFailure'
            {
            // InternalFlaDsl.g:811:3: ()
            // InternalFlaDsl.g:812:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNoFailureDefinitionAccess().getNoFailureDefinitionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getNoFailureDefinitionAccess().getNoFailureKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNoFailureDefinition"


    // $ANTLR start "entryRuleComplexNofailureDefinition"
    // InternalFlaDsl.g:826:1: entryRuleComplexNofailureDefinition returns [EObject current=null] : iv_ruleComplexNofailureDefinition= ruleComplexNofailureDefinition EOF ;
    public final EObject entryRuleComplexNofailureDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplexNofailureDefinition = null;


        try {
            // InternalFlaDsl.g:826:67: (iv_ruleComplexNofailureDefinition= ruleComplexNofailureDefinition EOF )
            // InternalFlaDsl.g:827:2: iv_ruleComplexNofailureDefinition= ruleComplexNofailureDefinition EOF
            {
             newCompositeNode(grammarAccess.getComplexNofailureDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComplexNofailureDefinition=ruleComplexNofailureDefinition();

            state._fsp--;

             current =iv_ruleComplexNofailureDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplexNofailureDefinition"


    // $ANTLR start "ruleComplexNofailureDefinition"
    // InternalFlaDsl.g:833:1: ruleComplexNofailureDefinition returns [EObject current=null] : ( () otherlv_1= 'noFailure' otherlv_2= '.' ( (lv_acidMitigation_3_0= ruleACIDMitigation ) ) ) ;
    public final EObject ruleComplexNofailureDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_acidMitigation_3_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:839:2: ( ( () otherlv_1= 'noFailure' otherlv_2= '.' ( (lv_acidMitigation_3_0= ruleACIDMitigation ) ) ) )
            // InternalFlaDsl.g:840:2: ( () otherlv_1= 'noFailure' otherlv_2= '.' ( (lv_acidMitigation_3_0= ruleACIDMitigation ) ) )
            {
            // InternalFlaDsl.g:840:2: ( () otherlv_1= 'noFailure' otherlv_2= '.' ( (lv_acidMitigation_3_0= ruleACIDMitigation ) ) )
            // InternalFlaDsl.g:841:3: () otherlv_1= 'noFailure' otherlv_2= '.' ( (lv_acidMitigation_3_0= ruleACIDMitigation ) )
            {
            // InternalFlaDsl.g:841:3: ()
            // InternalFlaDsl.g:842:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getComplexNofailureDefinitionAccess().getNoFailureDefinitionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getComplexNofailureDefinitionAccess().getNoFailureKeyword_1());
            		
            otherlv_2=(Token)match(input,16,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getComplexNofailureDefinitionAccess().getFullStopKeyword_2());
            		
            // InternalFlaDsl.g:856:3: ( (lv_acidMitigation_3_0= ruleACIDMitigation ) )
            // InternalFlaDsl.g:857:4: (lv_acidMitigation_3_0= ruleACIDMitigation )
            {
            // InternalFlaDsl.g:857:4: (lv_acidMitigation_3_0= ruleACIDMitigation )
            // InternalFlaDsl.g:858:5: lv_acidMitigation_3_0= ruleACIDMitigation
            {

            					newCompositeNode(grammarAccess.getComplexNofailureDefinitionAccess().getAcidMitigationACIDMitigationParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_acidMitigation_3_0=ruleACIDMitigation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getComplexNofailureDefinitionRule());
            					}
            					set(
            						current,
            						"acidMitigation",
            						lv_acidMitigation_3_0,
            						"org.polarsys.chess.xtext.FlaDsl.ACIDMitigation");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplexNofailureDefinition"


    // $ANTLR start "entryRuleWildcardDefinition"
    // InternalFlaDsl.g:879:1: entryRuleWildcardDefinition returns [EObject current=null] : iv_ruleWildcardDefinition= ruleWildcardDefinition EOF ;
    public final EObject entryRuleWildcardDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWildcardDefinition = null;


        try {
            // InternalFlaDsl.g:879:59: (iv_ruleWildcardDefinition= ruleWildcardDefinition EOF )
            // InternalFlaDsl.g:880:2: iv_ruleWildcardDefinition= ruleWildcardDefinition EOF
            {
             newCompositeNode(grammarAccess.getWildcardDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWildcardDefinition=ruleWildcardDefinition();

            state._fsp--;

             current =iv_ruleWildcardDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWildcardDefinition"


    // $ANTLR start "ruleWildcardDefinition"
    // InternalFlaDsl.g:886:1: ruleWildcardDefinition returns [EObject current=null] : ( () otherlv_1= 'wildcard' ) ;
    public final EObject ruleWildcardDefinition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:892:2: ( ( () otherlv_1= 'wildcard' ) )
            // InternalFlaDsl.g:893:2: ( () otherlv_1= 'wildcard' )
            {
            // InternalFlaDsl.g:893:2: ( () otherlv_1= 'wildcard' )
            // InternalFlaDsl.g:894:3: () otherlv_1= 'wildcard'
            {
            // InternalFlaDsl.g:894:3: ()
            // InternalFlaDsl.g:895:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWildcardDefinitionAccess().getWildcardDefinitionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getWildcardDefinitionAccess().getWildcardKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWildcardDefinition"


    // $ANTLR start "entryRuleVariableDefinition"
    // InternalFlaDsl.g:909:1: entryRuleVariableDefinition returns [EObject current=null] : iv_ruleVariableDefinition= ruleVariableDefinition EOF ;
    public final EObject entryRuleVariableDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableDefinition = null;


        try {
            // InternalFlaDsl.g:909:59: (iv_ruleVariableDefinition= ruleVariableDefinition EOF )
            // InternalFlaDsl.g:910:2: iv_ruleVariableDefinition= ruleVariableDefinition EOF
            {
             newCompositeNode(grammarAccess.getVariableDefinitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariableDefinition=ruleVariableDefinition();

            state._fsp--;

             current =iv_ruleVariableDefinition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableDefinition"


    // $ANTLR start "ruleVariableDefinition"
    // InternalFlaDsl.g:916:1: ruleVariableDefinition returns [EObject current=null] : ( () ( (lv_variableName_1_0= RULE_ID ) ) ) ;
    public final EObject ruleVariableDefinition() throws RecognitionException {
        EObject current = null;

        Token lv_variableName_1_0=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:922:2: ( ( () ( (lv_variableName_1_0= RULE_ID ) ) ) )
            // InternalFlaDsl.g:923:2: ( () ( (lv_variableName_1_0= RULE_ID ) ) )
            {
            // InternalFlaDsl.g:923:2: ( () ( (lv_variableName_1_0= RULE_ID ) ) )
            // InternalFlaDsl.g:924:3: () ( (lv_variableName_1_0= RULE_ID ) )
            {
            // InternalFlaDsl.g:924:3: ()
            // InternalFlaDsl.g:925:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getVariableDefinitionAccess().getVariableDefinitionAction_0(),
            					current);
            			

            }

            // InternalFlaDsl.g:931:3: ( (lv_variableName_1_0= RULE_ID ) )
            // InternalFlaDsl.g:932:4: (lv_variableName_1_0= RULE_ID )
            {
            // InternalFlaDsl.g:932:4: (lv_variableName_1_0= RULE_ID )
            // InternalFlaDsl.g:933:5: lv_variableName_1_0= RULE_ID
            {
            lv_variableName_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_variableName_1_0, grammarAccess.getVariableDefinitionAccess().getVariableNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVariableDefinitionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"variableName",
            						lv_variableName_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableDefinition"


    // $ANTLR start "entryRuleACIDavoidable"
    // InternalFlaDsl.g:953:1: entryRuleACIDavoidable returns [EObject current=null] : iv_ruleACIDavoidable= ruleACIDavoidable EOF ;
    public final EObject entryRuleACIDavoidable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleACIDavoidable = null;


        try {
            // InternalFlaDsl.g:953:54: (iv_ruleACIDavoidable= ruleACIDavoidable EOF )
            // InternalFlaDsl.g:954:2: iv_ruleACIDavoidable= ruleACIDavoidable EOF
            {
             newCompositeNode(grammarAccess.getACIDavoidableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleACIDavoidable=ruleACIDavoidable();

            state._fsp--;

             current =iv_ruleACIDavoidable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleACIDavoidable"


    // $ANTLR start "ruleACIDavoidable"
    // InternalFlaDsl.g:960:1: ruleACIDavoidable returns [EObject current=null] : ( ( (lv_a_0_0= ruleAavoidable ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCavoidable ) ) otherlv_3= '.' ( (lv_i_4_0= ruleIavoidable ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDavoidable ) ) ) ;
    public final EObject ruleACIDavoidable() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Enumerator lv_a_0_0 = null;

        Enumerator lv_c_2_0 = null;

        Enumerator lv_i_4_0 = null;

        Enumerator lv_d_6_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:966:2: ( ( ( (lv_a_0_0= ruleAavoidable ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCavoidable ) ) otherlv_3= '.' ( (lv_i_4_0= ruleIavoidable ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDavoidable ) ) ) )
            // InternalFlaDsl.g:967:2: ( ( (lv_a_0_0= ruleAavoidable ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCavoidable ) ) otherlv_3= '.' ( (lv_i_4_0= ruleIavoidable ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDavoidable ) ) )
            {
            // InternalFlaDsl.g:967:2: ( ( (lv_a_0_0= ruleAavoidable ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCavoidable ) ) otherlv_3= '.' ( (lv_i_4_0= ruleIavoidable ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDavoidable ) ) )
            // InternalFlaDsl.g:968:3: ( (lv_a_0_0= ruleAavoidable ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCavoidable ) ) otherlv_3= '.' ( (lv_i_4_0= ruleIavoidable ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDavoidable ) )
            {
            // InternalFlaDsl.g:968:3: ( (lv_a_0_0= ruleAavoidable ) )
            // InternalFlaDsl.g:969:4: (lv_a_0_0= ruleAavoidable )
            {
            // InternalFlaDsl.g:969:4: (lv_a_0_0= ruleAavoidable )
            // InternalFlaDsl.g:970:5: lv_a_0_0= ruleAavoidable
            {

            					newCompositeNode(grammarAccess.getACIDavoidableAccess().getAAavoidableEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_8);
            lv_a_0_0=ruleAavoidable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDavoidableRule());
            					}
            					set(
            						current,
            						"a",
            						lv_a_0_0,
            						"org.polarsys.chess.xtext.FlaDsl.Aavoidable");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,16,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getACIDavoidableAccess().getFullStopKeyword_1());
            		
            // InternalFlaDsl.g:991:3: ( (lv_c_2_0= ruleCavoidable ) )
            // InternalFlaDsl.g:992:4: (lv_c_2_0= ruleCavoidable )
            {
            // InternalFlaDsl.g:992:4: (lv_c_2_0= ruleCavoidable )
            // InternalFlaDsl.g:993:5: lv_c_2_0= ruleCavoidable
            {

            					newCompositeNode(grammarAccess.getACIDavoidableAccess().getCCavoidableEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_c_2_0=ruleCavoidable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDavoidableRule());
            					}
            					set(
            						current,
            						"c",
            						lv_c_2_0,
            						"org.polarsys.chess.xtext.FlaDsl.Cavoidable");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getACIDavoidableAccess().getFullStopKeyword_3());
            		
            // InternalFlaDsl.g:1014:3: ( (lv_i_4_0= ruleIavoidable ) )
            // InternalFlaDsl.g:1015:4: (lv_i_4_0= ruleIavoidable )
            {
            // InternalFlaDsl.g:1015:4: (lv_i_4_0= ruleIavoidable )
            // InternalFlaDsl.g:1016:5: lv_i_4_0= ruleIavoidable
            {

            					newCompositeNode(grammarAccess.getACIDavoidableAccess().getIIavoidableEnumRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_i_4_0=ruleIavoidable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDavoidableRule());
            					}
            					set(
            						current,
            						"i",
            						lv_i_4_0,
            						"org.polarsys.chess.xtext.FlaDsl.Iavoidable");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,16,FOLLOW_18); 

            			newLeafNode(otherlv_5, grammarAccess.getACIDavoidableAccess().getFullStopKeyword_5());
            		
            // InternalFlaDsl.g:1037:3: ( (lv_d_6_0= ruleDavoidable ) )
            // InternalFlaDsl.g:1038:4: (lv_d_6_0= ruleDavoidable )
            {
            // InternalFlaDsl.g:1038:4: (lv_d_6_0= ruleDavoidable )
            // InternalFlaDsl.g:1039:5: lv_d_6_0= ruleDavoidable
            {

            					newCompositeNode(grammarAccess.getACIDavoidableAccess().getDDavoidableEnumRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_d_6_0=ruleDavoidable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDavoidableRule());
            					}
            					set(
            						current,
            						"d",
            						lv_d_6_0,
            						"org.polarsys.chess.xtext.FlaDsl.Davoidable");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleACIDavoidable"


    // $ANTLR start "entryRuleACIDMitigation"
    // InternalFlaDsl.g:1060:1: entryRuleACIDMitigation returns [EObject current=null] : iv_ruleACIDMitigation= ruleACIDMitigation EOF ;
    public final EObject entryRuleACIDMitigation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleACIDMitigation = null;


        try {
            // InternalFlaDsl.g:1060:55: (iv_ruleACIDMitigation= ruleACIDMitigation EOF )
            // InternalFlaDsl.g:1061:2: iv_ruleACIDMitigation= ruleACIDMitigation EOF
            {
             newCompositeNode(grammarAccess.getACIDMitigationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleACIDMitigation=ruleACIDMitigation();

            state._fsp--;

             current =iv_ruleACIDMitigation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleACIDMitigation"


    // $ANTLR start "ruleACIDMitigation"
    // InternalFlaDsl.g:1067:1: ruleACIDMitigation returns [EObject current=null] : ( ( (lv_a_0_0= ruleAmitigation ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCmitigation ) ) otherlv_3= '.' ( (lv_i_4_0= ruleImitigation ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDmitigation ) ) ) ;
    public final EObject ruleACIDMitigation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Enumerator lv_a_0_0 = null;

        Enumerator lv_c_2_0 = null;

        Enumerator lv_i_4_0 = null;

        Enumerator lv_d_6_0 = null;



        	enterRule();

        try {
            // InternalFlaDsl.g:1073:2: ( ( ( (lv_a_0_0= ruleAmitigation ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCmitigation ) ) otherlv_3= '.' ( (lv_i_4_0= ruleImitigation ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDmitigation ) ) ) )
            // InternalFlaDsl.g:1074:2: ( ( (lv_a_0_0= ruleAmitigation ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCmitigation ) ) otherlv_3= '.' ( (lv_i_4_0= ruleImitigation ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDmitigation ) ) )
            {
            // InternalFlaDsl.g:1074:2: ( ( (lv_a_0_0= ruleAmitigation ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCmitigation ) ) otherlv_3= '.' ( (lv_i_4_0= ruleImitigation ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDmitigation ) ) )
            // InternalFlaDsl.g:1075:3: ( (lv_a_0_0= ruleAmitigation ) ) otherlv_1= '.' ( (lv_c_2_0= ruleCmitigation ) ) otherlv_3= '.' ( (lv_i_4_0= ruleImitigation ) ) otherlv_5= '.' ( (lv_d_6_0= ruleDmitigation ) )
            {
            // InternalFlaDsl.g:1075:3: ( (lv_a_0_0= ruleAmitigation ) )
            // InternalFlaDsl.g:1076:4: (lv_a_0_0= ruleAmitigation )
            {
            // InternalFlaDsl.g:1076:4: (lv_a_0_0= ruleAmitigation )
            // InternalFlaDsl.g:1077:5: lv_a_0_0= ruleAmitigation
            {

            					newCompositeNode(grammarAccess.getACIDMitigationAccess().getAAmitigationEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_8);
            lv_a_0_0=ruleAmitigation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDMitigationRule());
            					}
            					set(
            						current,
            						"a",
            						lv_a_0_0,
            						"org.polarsys.chess.xtext.FlaDsl.Amitigation");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,16,FOLLOW_19); 

            			newLeafNode(otherlv_1, grammarAccess.getACIDMitigationAccess().getFullStopKeyword_1());
            		
            // InternalFlaDsl.g:1098:3: ( (lv_c_2_0= ruleCmitigation ) )
            // InternalFlaDsl.g:1099:4: (lv_c_2_0= ruleCmitigation )
            {
            // InternalFlaDsl.g:1099:4: (lv_c_2_0= ruleCmitigation )
            // InternalFlaDsl.g:1100:5: lv_c_2_0= ruleCmitigation
            {

            					newCompositeNode(grammarAccess.getACIDMitigationAccess().getCCmitigationEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_c_2_0=ruleCmitigation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDMitigationRule());
            					}
            					set(
            						current,
            						"c",
            						lv_c_2_0,
            						"org.polarsys.chess.xtext.FlaDsl.Cmitigation");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getACIDMitigationAccess().getFullStopKeyword_3());
            		
            // InternalFlaDsl.g:1121:3: ( (lv_i_4_0= ruleImitigation ) )
            // InternalFlaDsl.g:1122:4: (lv_i_4_0= ruleImitigation )
            {
            // InternalFlaDsl.g:1122:4: (lv_i_4_0= ruleImitigation )
            // InternalFlaDsl.g:1123:5: lv_i_4_0= ruleImitigation
            {

            					newCompositeNode(grammarAccess.getACIDMitigationAccess().getIImitigationEnumRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_i_4_0=ruleImitigation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDMitigationRule());
            					}
            					set(
            						current,
            						"i",
            						lv_i_4_0,
            						"org.polarsys.chess.xtext.FlaDsl.Imitigation");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,16,FOLLOW_21); 

            			newLeafNode(otherlv_5, grammarAccess.getACIDMitigationAccess().getFullStopKeyword_5());
            		
            // InternalFlaDsl.g:1144:3: ( (lv_d_6_0= ruleDmitigation ) )
            // InternalFlaDsl.g:1145:4: (lv_d_6_0= ruleDmitigation )
            {
            // InternalFlaDsl.g:1145:4: (lv_d_6_0= ruleDmitigation )
            // InternalFlaDsl.g:1146:5: lv_d_6_0= ruleDmitigation
            {

            					newCompositeNode(grammarAccess.getACIDMitigationAccess().getDDmitigationEnumRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_d_6_0=ruleDmitigation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getACIDMitigationRule());
            					}
            					set(
            						current,
            						"d",
            						lv_d_6_0,
            						"org.polarsys.chess.xtext.FlaDsl.Dmitigation");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleACIDMitigation"


    // $ANTLR start "ruleActualFailureType"
    // InternalFlaDsl.g:1167:1: ruleActualFailureType returns [Enumerator current=null] : ( (enumLiteral_0= 'early' ) | (enumLiteral_1= 'late' ) | (enumLiteral_2= 'commission' ) | (enumLiteral_3= 'omission' ) | (enumLiteral_4= 'valueSubtle' ) | (enumLiteral_5= 'valueCoarse' ) ) ;
    public final Enumerator ruleActualFailureType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1173:2: ( ( (enumLiteral_0= 'early' ) | (enumLiteral_1= 'late' ) | (enumLiteral_2= 'commission' ) | (enumLiteral_3= 'omission' ) | (enumLiteral_4= 'valueSubtle' ) | (enumLiteral_5= 'valueCoarse' ) ) )
            // InternalFlaDsl.g:1174:2: ( (enumLiteral_0= 'early' ) | (enumLiteral_1= 'late' ) | (enumLiteral_2= 'commission' ) | (enumLiteral_3= 'omission' ) | (enumLiteral_4= 'valueSubtle' ) | (enumLiteral_5= 'valueCoarse' ) )
            {
            // InternalFlaDsl.g:1174:2: ( (enumLiteral_0= 'early' ) | (enumLiteral_1= 'late' ) | (enumLiteral_2= 'commission' ) | (enumLiteral_3= 'omission' ) | (enumLiteral_4= 'valueSubtle' ) | (enumLiteral_5= 'valueCoarse' ) )
            int alt11=6;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt11=1;
                }
                break;
            case 22:
                {
                alt11=2;
                }
                break;
            case 23:
                {
                alt11=3;
                }
                break;
            case 24:
                {
                alt11=4;
                }
                break;
            case 25:
                {
                alt11=5;
                }
                break;
            case 26:
                {
                alt11=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalFlaDsl.g:1175:3: (enumLiteral_0= 'early' )
                    {
                    // InternalFlaDsl.g:1175:3: (enumLiteral_0= 'early' )
                    // InternalFlaDsl.g:1176:4: enumLiteral_0= 'early'
                    {
                    enumLiteral_0=(Token)match(input,21,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getEARLYEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getActualFailureTypeAccess().getEARLYEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1183:3: (enumLiteral_1= 'late' )
                    {
                    // InternalFlaDsl.g:1183:3: (enumLiteral_1= 'late' )
                    // InternalFlaDsl.g:1184:4: enumLiteral_1= 'late'
                    {
                    enumLiteral_1=(Token)match(input,22,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getLATEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getActualFailureTypeAccess().getLATEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1191:3: (enumLiteral_2= 'commission' )
                    {
                    // InternalFlaDsl.g:1191:3: (enumLiteral_2= 'commission' )
                    // InternalFlaDsl.g:1192:4: enumLiteral_2= 'commission'
                    {
                    enumLiteral_2=(Token)match(input,23,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getCOMMISSIONEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getActualFailureTypeAccess().getCOMMISSIONEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:1199:3: (enumLiteral_3= 'omission' )
                    {
                    // InternalFlaDsl.g:1199:3: (enumLiteral_3= 'omission' )
                    // InternalFlaDsl.g:1200:4: enumLiteral_3= 'omission'
                    {
                    enumLiteral_3=(Token)match(input,24,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getOMISSIONEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getActualFailureTypeAccess().getOMISSIONEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalFlaDsl.g:1207:3: (enumLiteral_4= 'valueSubtle' )
                    {
                    // InternalFlaDsl.g:1207:3: (enumLiteral_4= 'valueSubtle' )
                    // InternalFlaDsl.g:1208:4: enumLiteral_4= 'valueSubtle'
                    {
                    enumLiteral_4=(Token)match(input,25,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getVALUE_SUBTLEEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getActualFailureTypeAccess().getVALUE_SUBTLEEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalFlaDsl.g:1215:3: (enumLiteral_5= 'valueCoarse' )
                    {
                    // InternalFlaDsl.g:1215:3: (enumLiteral_5= 'valueCoarse' )
                    // InternalFlaDsl.g:1216:4: enumLiteral_5= 'valueCoarse'
                    {
                    enumLiteral_5=(Token)match(input,26,FOLLOW_2); 

                    				current = grammarAccess.getActualFailureTypeAccess().getVALUE_COARSEEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getActualFailureTypeAccess().getVALUE_COARSEEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActualFailureType"


    // $ANTLR start "ruleAavoidable"
    // InternalFlaDsl.g:1226:1: ruleAavoidable returns [Enumerator current=null] : ( (enumLiteral_0= 'incompletion' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) ;
    public final Enumerator ruleAavoidable() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1232:2: ( ( (enumLiteral_0= 'incompletion' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) )
            // InternalFlaDsl.g:1233:2: ( (enumLiteral_0= 'incompletion' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1233:2: ( (enumLiteral_0= 'incompletion' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt12=1;
                }
                break;
            case 28:
                {
                alt12=2;
                }
                break;
            case 29:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalFlaDsl.g:1234:3: (enumLiteral_0= 'incompletion' )
                    {
                    // InternalFlaDsl.g:1234:3: (enumLiteral_0= 'incompletion' )
                    // InternalFlaDsl.g:1235:4: enumLiteral_0= 'incompletion'
                    {
                    enumLiteral_0=(Token)match(input,27,FOLLOW_2); 

                    				current = grammarAccess.getAavoidableAccess().getINCOMPLETIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAavoidableAccess().getINCOMPLETIONEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1242:3: (enumLiteral_1= 'none' )
                    {
                    // InternalFlaDsl.g:1242:3: (enumLiteral_1= 'none' )
                    // InternalFlaDsl.g:1243:4: enumLiteral_1= 'none'
                    {
                    enumLiteral_1=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getAavoidableAccess().getNONEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAavoidableAccess().getNONEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1250:3: (enumLiteral_2= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1250:3: (enumLiteral_2= 'unspecified' )
                    // InternalFlaDsl.g:1251:4: enumLiteral_2= 'unspecified'
                    {
                    enumLiteral_2=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getAavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getAavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAavoidable"


    // $ANTLR start "ruleCavoidable"
    // InternalFlaDsl.g:1261:1: ruleCavoidable returns [Enumerator current=null] : ( (enumLiteral_0= 'inconsistency' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) ;
    public final Enumerator ruleCavoidable() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1267:2: ( ( (enumLiteral_0= 'inconsistency' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) )
            // InternalFlaDsl.g:1268:2: ( (enumLiteral_0= 'inconsistency' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1268:2: ( (enumLiteral_0= 'inconsistency' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            int alt13=3;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt13=1;
                }
                break;
            case 28:
                {
                alt13=2;
                }
                break;
            case 29:
                {
                alt13=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalFlaDsl.g:1269:3: (enumLiteral_0= 'inconsistency' )
                    {
                    // InternalFlaDsl.g:1269:3: (enumLiteral_0= 'inconsistency' )
                    // InternalFlaDsl.g:1270:4: enumLiteral_0= 'inconsistency'
                    {
                    enumLiteral_0=(Token)match(input,30,FOLLOW_2); 

                    				current = grammarAccess.getCavoidableAccess().getINCONSISTENCYEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getCavoidableAccess().getINCONSISTENCYEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1277:3: (enumLiteral_1= 'none' )
                    {
                    // InternalFlaDsl.g:1277:3: (enumLiteral_1= 'none' )
                    // InternalFlaDsl.g:1278:4: enumLiteral_1= 'none'
                    {
                    enumLiteral_1=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getCavoidableAccess().getNONEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getCavoidableAccess().getNONEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1285:3: (enumLiteral_2= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1285:3: (enumLiteral_2= 'unspecified' )
                    // InternalFlaDsl.g:1286:4: enumLiteral_2= 'unspecified'
                    {
                    enumLiteral_2=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getCavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getCavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCavoidable"


    // $ANTLR start "ruleIavoidable"
    // InternalFlaDsl.g:1296:1: ruleIavoidable returns [Enumerator current=null] : ( (enumLiteral_0= 'interference' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) ;
    public final Enumerator ruleIavoidable() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1302:2: ( ( (enumLiteral_0= 'interference' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) )
            // InternalFlaDsl.g:1303:2: ( (enumLiteral_0= 'interference' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1303:2: ( (enumLiteral_0= 'interference' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            int alt14=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt14=1;
                }
                break;
            case 28:
                {
                alt14=2;
                }
                break;
            case 29:
                {
                alt14=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalFlaDsl.g:1304:3: (enumLiteral_0= 'interference' )
                    {
                    // InternalFlaDsl.g:1304:3: (enumLiteral_0= 'interference' )
                    // InternalFlaDsl.g:1305:4: enumLiteral_0= 'interference'
                    {
                    enumLiteral_0=(Token)match(input,31,FOLLOW_2); 

                    				current = grammarAccess.getIavoidableAccess().getINTERFERENCEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getIavoidableAccess().getINTERFERENCEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1312:3: (enumLiteral_1= 'none' )
                    {
                    // InternalFlaDsl.g:1312:3: (enumLiteral_1= 'none' )
                    // InternalFlaDsl.g:1313:4: enumLiteral_1= 'none'
                    {
                    enumLiteral_1=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getIavoidableAccess().getNONEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getIavoidableAccess().getNONEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1320:3: (enumLiteral_2= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1320:3: (enumLiteral_2= 'unspecified' )
                    // InternalFlaDsl.g:1321:4: enumLiteral_2= 'unspecified'
                    {
                    enumLiteral_2=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getIavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getIavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIavoidable"


    // $ANTLR start "ruleDavoidable"
    // InternalFlaDsl.g:1331:1: ruleDavoidable returns [Enumerator current=null] : ( (enumLiteral_0= 'impermanence' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) ;
    public final Enumerator ruleDavoidable() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1337:2: ( ( (enumLiteral_0= 'impermanence' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) ) )
            // InternalFlaDsl.g:1338:2: ( (enumLiteral_0= 'impermanence' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1338:2: ( (enumLiteral_0= 'impermanence' ) | (enumLiteral_1= 'none' ) | (enumLiteral_2= 'unspecified' ) )
            int alt15=3;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt15=1;
                }
                break;
            case 28:
                {
                alt15=2;
                }
                break;
            case 29:
                {
                alt15=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalFlaDsl.g:1339:3: (enumLiteral_0= 'impermanence' )
                    {
                    // InternalFlaDsl.g:1339:3: (enumLiteral_0= 'impermanence' )
                    // InternalFlaDsl.g:1340:4: enumLiteral_0= 'impermanence'
                    {
                    enumLiteral_0=(Token)match(input,32,FOLLOW_2); 

                    				current = grammarAccess.getDavoidableAccess().getIMPERMANENCEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDavoidableAccess().getIMPERMANENCEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1347:3: (enumLiteral_1= 'none' )
                    {
                    // InternalFlaDsl.g:1347:3: (enumLiteral_1= 'none' )
                    // InternalFlaDsl.g:1348:4: enumLiteral_1= 'none'
                    {
                    enumLiteral_1=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getDavoidableAccess().getNONEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDavoidableAccess().getNONEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1355:3: (enumLiteral_2= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1355:3: (enumLiteral_2= 'unspecified' )
                    // InternalFlaDsl.g:1356:4: enumLiteral_2= 'unspecified'
                    {
                    enumLiteral_2=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getDavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDavoidableAccess().getUNSPECIFIEDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDavoidable"


    // $ANTLR start "ruleAmitigation"
    // InternalFlaDsl.g:1366:1: ruleAmitigation returns [Enumerator current=null] : ( (enumLiteral_0= 'all_or_nothing' ) | (enumLiteral_1= 'all_or_compensation' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) ;
    public final Enumerator ruleAmitigation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1372:2: ( ( (enumLiteral_0= 'all_or_nothing' ) | (enumLiteral_1= 'all_or_compensation' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) )
            // InternalFlaDsl.g:1373:2: ( (enumLiteral_0= 'all_or_nothing' ) | (enumLiteral_1= 'all_or_compensation' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1373:2: ( (enumLiteral_0= 'all_or_nothing' ) | (enumLiteral_1= 'all_or_compensation' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            int alt16=4;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt16=1;
                }
                break;
            case 34:
                {
                alt16=2;
                }
                break;
            case 28:
                {
                alt16=3;
                }
                break;
            case 29:
                {
                alt16=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalFlaDsl.g:1374:3: (enumLiteral_0= 'all_or_nothing' )
                    {
                    // InternalFlaDsl.g:1374:3: (enumLiteral_0= 'all_or_nothing' )
                    // InternalFlaDsl.g:1375:4: enumLiteral_0= 'all_or_nothing'
                    {
                    enumLiteral_0=(Token)match(input,33,FOLLOW_2); 

                    				current = grammarAccess.getAmitigationAccess().getALL_OR_NOTHINGEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAmitigationAccess().getALL_OR_NOTHINGEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1382:3: (enumLiteral_1= 'all_or_compensation' )
                    {
                    // InternalFlaDsl.g:1382:3: (enumLiteral_1= 'all_or_compensation' )
                    // InternalFlaDsl.g:1383:4: enumLiteral_1= 'all_or_compensation'
                    {
                    enumLiteral_1=(Token)match(input,34,FOLLOW_2); 

                    				current = grammarAccess.getAmitigationAccess().getALL_OR_COMPENSATIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAmitigationAccess().getALL_OR_COMPENSATIONEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1390:3: (enumLiteral_2= 'none' )
                    {
                    // InternalFlaDsl.g:1390:3: (enumLiteral_2= 'none' )
                    // InternalFlaDsl.g:1391:4: enumLiteral_2= 'none'
                    {
                    enumLiteral_2=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getAmitigationAccess().getNONEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getAmitigationAccess().getNONEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:1398:3: (enumLiteral_3= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1398:3: (enumLiteral_3= 'unspecified' )
                    // InternalFlaDsl.g:1399:4: enumLiteral_3= 'unspecified'
                    {
                    enumLiteral_3=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getAmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getAmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAmitigation"


    // $ANTLR start "ruleCmitigation"
    // InternalFlaDsl.g:1409:1: ruleCmitigation returns [Enumerator current=null] : ( (enumLiteral_0= 'full_consistency' ) | (enumLiteral_1= 'range_violation_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) ;
    public final Enumerator ruleCmitigation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1415:2: ( ( (enumLiteral_0= 'full_consistency' ) | (enumLiteral_1= 'range_violation_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) )
            // InternalFlaDsl.g:1416:2: ( (enumLiteral_0= 'full_consistency' ) | (enumLiteral_1= 'range_violation_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1416:2: ( (enumLiteral_0= 'full_consistency' ) | (enumLiteral_1= 'range_violation_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            int alt17=4;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt17=1;
                }
                break;
            case 36:
                {
                alt17=2;
                }
                break;
            case 28:
                {
                alt17=3;
                }
                break;
            case 29:
                {
                alt17=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalFlaDsl.g:1417:3: (enumLiteral_0= 'full_consistency' )
                    {
                    // InternalFlaDsl.g:1417:3: (enumLiteral_0= 'full_consistency' )
                    // InternalFlaDsl.g:1418:4: enumLiteral_0= 'full_consistency'
                    {
                    enumLiteral_0=(Token)match(input,35,FOLLOW_2); 

                    				current = grammarAccess.getCmitigationAccess().getFULL_CONSISTENCYEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getCmitigationAccess().getFULL_CONSISTENCYEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1425:3: (enumLiteral_1= 'range_violation_allowed' )
                    {
                    // InternalFlaDsl.g:1425:3: (enumLiteral_1= 'range_violation_allowed' )
                    // InternalFlaDsl.g:1426:4: enumLiteral_1= 'range_violation_allowed'
                    {
                    enumLiteral_1=(Token)match(input,36,FOLLOW_2); 

                    				current = grammarAccess.getCmitigationAccess().getRANGE_VIOLATION_ALLOWEDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getCmitigationAccess().getRANGE_VIOLATION_ALLOWEDEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1433:3: (enumLiteral_2= 'none' )
                    {
                    // InternalFlaDsl.g:1433:3: (enumLiteral_2= 'none' )
                    // InternalFlaDsl.g:1434:4: enumLiteral_2= 'none'
                    {
                    enumLiteral_2=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getCmitigationAccess().getNONEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getCmitigationAccess().getNONEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:1441:3: (enumLiteral_3= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1441:3: (enumLiteral_3= 'unspecified' )
                    // InternalFlaDsl.g:1442:4: enumLiteral_3= 'unspecified'
                    {
                    enumLiteral_3=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getCmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getCmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCmitigation"


    // $ANTLR start "ruleImitigation"
    // InternalFlaDsl.g:1452:1: ruleImitigation returns [Enumerator current=null] : ( (enumLiteral_0= 'serializable' ) | (enumLiteral_1= 'portable_level' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) ;
    public final Enumerator ruleImitigation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1458:2: ( ( (enumLiteral_0= 'serializable' ) | (enumLiteral_1= 'portable_level' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) )
            // InternalFlaDsl.g:1459:2: ( (enumLiteral_0= 'serializable' ) | (enumLiteral_1= 'portable_level' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1459:2: ( (enumLiteral_0= 'serializable' ) | (enumLiteral_1= 'portable_level' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            int alt18=4;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt18=1;
                }
                break;
            case 38:
                {
                alt18=2;
                }
                break;
            case 28:
                {
                alt18=3;
                }
                break;
            case 29:
                {
                alt18=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalFlaDsl.g:1460:3: (enumLiteral_0= 'serializable' )
                    {
                    // InternalFlaDsl.g:1460:3: (enumLiteral_0= 'serializable' )
                    // InternalFlaDsl.g:1461:4: enumLiteral_0= 'serializable'
                    {
                    enumLiteral_0=(Token)match(input,37,FOLLOW_2); 

                    				current = grammarAccess.getImitigationAccess().getSERIALIZABLEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getImitigationAccess().getSERIALIZABLEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1468:3: (enumLiteral_1= 'portable_level' )
                    {
                    // InternalFlaDsl.g:1468:3: (enumLiteral_1= 'portable_level' )
                    // InternalFlaDsl.g:1469:4: enumLiteral_1= 'portable_level'
                    {
                    enumLiteral_1=(Token)match(input,38,FOLLOW_2); 

                    				current = grammarAccess.getImitigationAccess().getPORTABLE_LEVELEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getImitigationAccess().getPORTABLE_LEVELEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1476:3: (enumLiteral_2= 'none' )
                    {
                    // InternalFlaDsl.g:1476:3: (enumLiteral_2= 'none' )
                    // InternalFlaDsl.g:1477:4: enumLiteral_2= 'none'
                    {
                    enumLiteral_2=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getImitigationAccess().getNONEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getImitigationAccess().getNONEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:1484:3: (enumLiteral_3= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1484:3: (enumLiteral_3= 'unspecified' )
                    // InternalFlaDsl.g:1485:4: enumLiteral_3= 'unspecified'
                    {
                    enumLiteral_3=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getImitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getImitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImitigation"


    // $ANTLR start "ruleDmitigation"
    // InternalFlaDsl.g:1495:1: ruleDmitigation returns [Enumerator current=null] : ( (enumLiteral_0= 'no_loss' ) | (enumLiteral_1= 'partial_loss_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) ;
    public final Enumerator ruleDmitigation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFlaDsl.g:1501:2: ( ( (enumLiteral_0= 'no_loss' ) | (enumLiteral_1= 'partial_loss_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) ) )
            // InternalFlaDsl.g:1502:2: ( (enumLiteral_0= 'no_loss' ) | (enumLiteral_1= 'partial_loss_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            {
            // InternalFlaDsl.g:1502:2: ( (enumLiteral_0= 'no_loss' ) | (enumLiteral_1= 'partial_loss_allowed' ) | (enumLiteral_2= 'none' ) | (enumLiteral_3= 'unspecified' ) )
            int alt19=4;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt19=1;
                }
                break;
            case 40:
                {
                alt19=2;
                }
                break;
            case 28:
                {
                alt19=3;
                }
                break;
            case 29:
                {
                alt19=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalFlaDsl.g:1503:3: (enumLiteral_0= 'no_loss' )
                    {
                    // InternalFlaDsl.g:1503:3: (enumLiteral_0= 'no_loss' )
                    // InternalFlaDsl.g:1504:4: enumLiteral_0= 'no_loss'
                    {
                    enumLiteral_0=(Token)match(input,39,FOLLOW_2); 

                    				current = grammarAccess.getDmitigationAccess().getNO_LOSSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDmitigationAccess().getNO_LOSSEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlaDsl.g:1511:3: (enumLiteral_1= 'partial_loss_allowed' )
                    {
                    // InternalFlaDsl.g:1511:3: (enumLiteral_1= 'partial_loss_allowed' )
                    // InternalFlaDsl.g:1512:4: enumLiteral_1= 'partial_loss_allowed'
                    {
                    enumLiteral_1=(Token)match(input,40,FOLLOW_2); 

                    				current = grammarAccess.getDmitigationAccess().getPARTIAL_LOSS_ALLOWEDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDmitigationAccess().getPARTIAL_LOSS_ALLOWEDEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlaDsl.g:1519:3: (enumLiteral_2= 'none' )
                    {
                    // InternalFlaDsl.g:1519:3: (enumLiteral_2= 'none' )
                    // InternalFlaDsl.g:1520:4: enumLiteral_2= 'none'
                    {
                    enumLiteral_2=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getDmitigationAccess().getNONEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDmitigationAccess().getNONEEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlaDsl.g:1527:3: (enumLiteral_3= 'unspecified' )
                    {
                    // InternalFlaDsl.g:1527:3: (enumLiteral_3= 'unspecified' )
                    // InternalFlaDsl.g:1528:4: enumLiteral_3= 'unspecified'
                    {
                    enumLiteral_3=(Token)match(input,29,FOLLOW_2); 

                    				current = grammarAccess.getDmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDmitigationAccess().getUNSPECIFIEDEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDmitigation"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000007FA0010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000007E00000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000044000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000038000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000630000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000070000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000B0000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000130000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000001830000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000006030000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000018030000000L});

}
<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="flamm" nsURI="http://www.polarsys.org/chess/fla/flamm" nsPrefix="flamm">
  <eClassifiers xsi:type="ecore:EClass" name="NamedElement">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="id" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="FlaBehaviour" abstract="true" interface="true">
    <eOperations name="propagateFailures"/>
    <eOperations name="initialize">
      <eParameters name="initNoFailure" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    </eOperations>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Component" abstract="true" eSuperTypes="#//NamedElement #//FlaBehaviour">
    <eStructuralFeatures xsi:type="ecore:EReference" name="inputPorts" upperBound="-1"
        eType="#//Port" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="outputPorts" upperBound="-1"
        eType="#//Port" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parent" eType="#//CompositeComponent"
        eOpposite="#//CompositeComponent/components"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="CompositeComponent" eSuperTypes="#//Component">
    <eStructuralFeatures xsi:type="ecore:EReference" name="components" upperBound="-1"
        eType="#//Component" containment="true" eOpposite="#//Component/parent"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="connections" upperBound="-1"
        eType="#//Connection" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="SimpleComponent" eSuperTypes="#//Component">
    <eStructuralFeatures xsi:type="ecore:EReference" name="rules" upperBound="-1"
        eType="#//Rule" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Port" eSuperTypes="#//NamedElement">
    <eOperations name="propagateFailures"/>
    <eOperations name="initialize"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="connectedPorts" upperBound="-1"
        eType="#//Port"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="failures" ordered="false"
        upperBound="-1" eType="#//Failure" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="newFailures" ordered="false"
        upperBound="-1" eType="#//Failure" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="owner" lowerBound="1" eType="#//Component"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="incomingConnections" lowerBound="1"
        eType="#//Connection" eOpposite="#//Connection/to"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="outgoingConnections" lowerBound="1"
        eType="#//Connection" eOpposite="#//Connection/from"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Connection" eSuperTypes="#//FlaBehaviour">
    <eStructuralFeatures xsi:type="ecore:EReference" name="from" lowerBound="1" eType="#//Port"
        eOpposite="#//Port/outgoingConnections"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="to" lowerBound="1" eType="#//Port"
        eOpposite="#//Port/incomingConnections"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="rules" upperBound="-1"
        eType="#//Rule"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Rule">
    <eStructuralFeatures xsi:type="ecore:EReference" name="inputExpression" upperBound="-1"
        eType="#//Expression" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="outputExpression" upperBound="-1"
        eType="#//Expression" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="specificity" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EInt" changeable="false"
        transient="true" defaultValueLiteral="-1"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Expression">
    <eStructuralFeatures xsi:type="ecore:EReference" name="port" lowerBound="1" eType="#//Port"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="failures" upperBound="-1"
        eType="#//Failure" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Failure">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="type" lowerBound="1" eType="#//FailureTypes/FailureType"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="id" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="acidAvoidable" eType="#//ACIDavoidable"
        containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="acidMitigation" eType="#//ACIDmitigation"
        containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="previousFailures" upperBound="-1"
        eType="#//Failure"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="specialization" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="vulnerability" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="attack" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ACIDavoidable">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="a" eType="#//FailureTypes/A_avoidable"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="c" eType="#//FailureTypes/C_avoidable"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="i" eType="#//FailureTypes/I_avoidable"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="d" eType="#//FailureTypes/D_avoidable"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ACIDmitigation">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="a" eType="#//FailureTypes/A_mitigation"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="c" eType="#//FailureTypes/C_mitigation"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="i" eType="#//FailureTypes/I_mitigation"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="d" eType="#//FailureTypes/D_mitigation"/>
  </eClassifiers>
  <eSubpackages name="FailureTypes" nsURI="http://www.polarsys.org/chess/fla/flamm/FailureTypes"
      nsPrefix="FailureTypes">
    <eClassifiers xsi:type="ecore:EEnum" name="FailureType">
      <eLiterals name="noFailure"/>
      <eLiterals name="failure" value="1"/>
      <eLiterals name="variable" value="2"/>
      <eLiterals name="wildcard" value="3"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="A_avoidable">
      <eLiterals name="unspecified"/>
      <eLiterals name="incompletion" value="1"/>
      <eLiterals name="none" value="2"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="C_avoidable">
      <eLiterals name="unspecified"/>
      <eLiterals name="inconsistency" value="1"/>
      <eLiterals name="none" value="2"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="I_avoidable">
      <eLiterals name="unspecified"/>
      <eLiterals name="interference" value="1"/>
      <eLiterals name="none" value="2"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="D_avoidable">
      <eLiterals name="unspecified"/>
      <eLiterals name="impermanence" value="1"/>
      <eLiterals name="none" value="2"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="A_mitigation">
      <eLiterals name="unspecified"/>
      <eLiterals name="all_or_nothing" value="1"/>
      <eLiterals name="all_or_compensation" value="2"/>
      <eLiterals name="none" value="3"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="C_mitigation">
      <eLiterals name="unspecified"/>
      <eLiterals name="full_consistency" value="1"/>
      <eLiterals name="range_violation_allowed" value="2"/>
      <eLiterals name="none" value="3"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="I_mitigation">
      <eLiterals name="unspecified"/>
      <eLiterals name="portable_level" value="1"/>
      <eLiterals name="serializable" value="2"/>
      <eLiterals name="none" value="3"/>
    </eClassifiers>
    <eClassifiers xsi:type="ecore:EEnum" name="D_mitigation">
      <eLiterals name="unspecified"/>
      <eLiterals name="no_loss" value="1"/>
      <eLiterals name="partial_loss_allowed" value="2"/>
      <eLiterals name="none" value="3"/>
    </eClassifiers>
  </eSubpackages>
</ecore:EPackage>
